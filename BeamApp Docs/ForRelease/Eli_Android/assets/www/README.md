###BEAM APP###
Main directory where application code build happens.
This folder consists of
    - Index.html
    - JS (Vendor - Framework Scripts & Application Scripts)
    - CSS (Vendor - Framework Styles & Application Styles)
    - Fonts
    - Images