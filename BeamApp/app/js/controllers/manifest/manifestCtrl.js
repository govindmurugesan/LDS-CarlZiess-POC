(function () {
  'use strict';

  angular
    .module('beam')
    .controller('ManifestCtrl', ManifestCtrl);

  ManifestCtrl.$inject = ['$scope', '$stateParams', '$state', '$ionicHistory', 'manifestService', 'deviceService', '$rootScope', 'notificationService', '$ionicPopup', 'packageStatusService', 'scanFunctionService', '$q', '$filter', '$timeout', '$log','rootScopeService','jsonStoreService'];

  function ManifestCtrl($scope, $stateParams, $state, $ionicHistory, manifestService, deviceService, $rootScope, notificationService, $ionicPopup, packageStatusService, scanFunctionService, $q, $filter, $timeout, $log,rootScopeService,jsonStoreService) {
    var vm = this;
    vm.choice = 'optimized';

    vm.deliveryScanRouting = deliveryScanRouting;
    vm.filterBy = "Optimized View";
    vm.locationRouting = locationRouting;
    vm.loadScanRouting = loadScanRouting;
    vm.onReorder = onReorder;
    vm.pieceDetails = pieceDetails;
    vm.showPopup = showPopup;
    vm.seqViewRouting = seqViewRouting;
    vm.pickUpScanRouting = pickUpScanRouting;
    vm.navigation = navigation;
    vm.filterOptions = '';
    vm.orderBy = 'defaultOrderIndex';
    var _manifestView = [];
    vm.groupedManifestView = [];
    vm.showCustomFilter = false;
    var _customViewData = [];
    var promises = [];
    var watcher;
    vm.manifestAvailable = true;

    $scope.$on('$ionicView.loaded', function () {
      //Here your view content is fully loaded !!
      $timeout(function () {
        notificationService.hideLoad();
      }, 100);
    });

    $scope.$on('$ionicView.beforeEnter', function () {
      var deferred = $q.defer();
      promises.push(loadPackages());
      promises.push(loadManifest());
      $q.all(promises).then(function (data) {
        _manifestView = [];
        promises = [];

        $rootScope.manifestData.forEach(function (manifestPiece, index) {
          if ($rootScope.packageCollection.length > 0) {
            var packageStatus = $.grep(data[0], function (packState) {
              return packState.json.Barcode == manifestPiece.Barcode;
            });
            if (packageStatus && packageStatus.length > 0) {
              addManifestWithPackageStatus(manifestPiece, packageStatus, index);
            }
            else {
              addManifest(manifestPiece, index);
            }
          } else {
            addManifest(manifestPiece, index);
          }

        }, this);

      });
    }, function (err) {
      $log.warn('Error getting manifest data', err);
    });


    $scope.$on('$ionicView.afterEnter', function () {

      watcher = $scope.$watch(function () {
        return _manifestView;
      }, function (res) {
        if (res.length > 0) {
          loadGroupedView();
        }else{
          notificationService.hideLoad();
        }
      });
      var loadedItems = jsonStoreService.getLoadedItems();
      var unassignLoadedItems = jsonStoreService.getUnassignLoadedItems();
      loadedItems.findAll().then(function (data) {
        if(data.length > 0)
          rootScopeService.saveScannedLoadScanItems(data);
      }, function (err) {
          $log.warn("Failed to save scanned items",err);
      });
      unassignLoadedItems.findAll().then(function (unassigndata) {
        if(unassigndata.length > 0)
          rootScopeService.unassignLoadItems(unassigndata);
      }, function (err) {
          $log.warn("Failed to save scanned items",err);
      });
    });

    $scope.$on('$ionicView.leave', function () {
      watcher();
    });

    $scope.$on("$ionicView.enter", function () {
      notificationService.showLoad('Updating Manifest');
    });

    function loadPackages() {
      var deferred = $q.defer();
      packageStatusService.getPackageInfo().then(
        function (data) {
          $rootScope.packageCollection = data;
          deferred.resolve(data);
        },
        function (err) {
          $log.error('Error getting manifest data', err);
          deferred.reject(err);
        }
      );
      return deferred.promise;
    }

    function loadManifest() {
      var deferred = $q.defer();
      manifestService.getManifestInfo().then(
        function (data) {
          var manifests = [];
          angular.forEach(data, function (piece, index) {
            manifests.push(piece.json);
          });
          if (manifests.length > 0) {
            vm.manifestAvailable = true;
            $rootScope.manifestData = manifests;

          } else {
            vm.manifestAvailable = false;
            $rootScope.manifestData = manifests;
          }
          deferred.resolve(data);
        },
        function (err) {
          $log.error('Error getting manifest data', err);
          deferred.reject(err);
        }
      );
      return deferred.promise;
    }

    function showPopup() {
      var popup = $ionicPopup.show({
        'templateUrl': 'filterOptionsPopup.html',
        title: 'Filters',
        scope: $scope,
        'buttons': [
          {
            'text': 'Cancel',
            'type': 'button-positive',
            'onTap': function (event) {
              event.preventDefault();
              popup.close();
            }
          },
          {
            'text': 'Ok',
            'type': 'button-positive',
            'onTap': function (event) {
              vm.filterOptions = '';
              vm.groupedManifestView = [];
              vm.manifestAvailable = true;
              popup.close();
              event.preventDefault();
              $timeout(function () {
                notificationService.showLoad('Updating Manifest');
                popup.close();
                applyFilter(vm.choice);
              }, 100);
             
            }
          }
        ]
      });

      function applyFilter(result) {       
        vm.manifestAvailable = true;
        popup.close();
        if (!result)
          return;
        if (result === 'optimized') {
          vm.filterBy = "Optimized View";
          vm.orderBy = 'defaultOrderIndex';
          vm.filterOptions = '';
          vm.showCustomFilter = false;
        }
        else if (result === 'custom') {
          loadCustomData();
          vm.showCustomFilter = true;
          vm.orderBy = '';
          vm.filterBy = "Custom View";
        }
        else if (result === 'alphabetical') {
          vm.showCustomFilter = false;
          vm.orderBy = "Contact";
          vm.filterBy = "Alphabetical";
          vm.filterOptions = '';
        }
        else if (result === 'piece_count') {
          vm.showCustomFilter = false;
          vm.orderBy = "-groupLength";
          vm.filterBy = "Piece Count";
          vm.filterOptions = '';
        }
        else if (result === 'delivered') {
          vm.showCustomFilter = false;
          vm.orderBy = '';
          vm.filterBy = "Delivered";
          vm.filterOptions = { 'Delivered': 'true' };
        }
        else if (result === 'scheduled') {
          vm.showCustomFilter = false;
          vm.filterBy = "Scheduled";
          vm.filterOptions = '';
          vm.orderBy = 'UTCExpectedDeliveryBy';

        }
        else if (result === 'not_assigned') {
          vm.showCustomFilter = false;
          vm.orderBy = '';
          vm.filterBy = "Not Assigned";
          vm.filterOptions = { 'Assigned': 'false' }

        }
        else if (result === 'attempted') {
          vm.showCustomFilter = false;
          vm.orderBy = '';
          vm.filterBy = "Attempted";
          vm.filterOptions =  { 'Attempted': 'true', 'Delivered': 'false' }
        }
        else if (result === 'pickups') {
          vm.showCustomFilter = false;
          vm.orderBy = '';
          vm.filterBy = "Pickups";
          vm.filterOptions = { 'Type': 'pickup' };
        }
        loadGroupedView();
      }
    }

    function onReorder(fromIndex, toIndex) {

      var moved = vm.customlist.splice(fromIndex, 1);
      vm.customlist.splice(toIndex, 0, moved[0]);
    }

    function pieceDetails(stopIdentifier, type) {
      $ionicHistory.nextViewOptions({
        disableBack: true
      });
      $state.go('app.detail', { 'stopIdentifier': stopIdentifier, 'type': type });
    }

    function loadScanRouting(stopIdentifier, instruction) {
      scanFunctionService.clearRootScope();
      rootScopeService.clearLocalStorage();
      if (instruction != "") {
        notificationService.alert('','', instruction, function () {
          $ionicHistory.nextViewOptions({
            disableBack: true
          });
          $state.go('app.scan.stop', { 'stopIdentifier': stopIdentifier });
        });
      } else {
        $ionicHistory.nextViewOptions({
          disableBack: true
        });
        $state.go('app.scan.stop', { 'stopIdentifier': stopIdentifier });
      }

    }

    function pickUpScanRouting(stopIdentifier, instruction) {
      scanFunctionService.clearRootScope();
      rootScopeService.clearLocalStorage();
      if (instruction != "") {
        notificationService.alert('','', instruction, function () {
          $ionicHistory.nextViewOptions({
            disableBack: true
          });
          $state.go('app.scan.stop', { 'stopIdentifier': stopIdentifier });
        });
      } else {
        $ionicHistory.nextViewOptions({
          disableBack: true
        });
        $state.go('app.scan.stop', { 'stopIdentifier': stopIdentifier });
      }
    }

    function seqViewRouting() {
      $ionicHistory.nextViewOptions({
        disableBack: true
      });
      $state.go('app.seqmanifest');
    }

    function locationRouting() {
      deviceService.getLocation().then(function (data) {
        var currentAddress = data.lat + ',' + data.long;
        if (ionic.Platform.isIOS())
          window.open("http://maps.apple.com/?q=" + currentAddress, '_system');
        else
          window.open("http://maps.google.com/?q=" + currentAddress, '_system');
      }, function (err) {
        $log.info("Error in getting location detail", err);
      });
    }

    function deliveryScanRouting(stopIdentifier, instruction, type) {
      scanFunctionService.clearRootScope();
      rootScopeService.clearLocalStorage();
      if (instruction != "") {
        $ionicHistory.nextViewOptions({
          disableBack: true
        });
        $state.go('app.detail', { 'stopIdentifier': stopIdentifier, 'type': type });
      } else {
        $ionicHistory.nextViewOptions({
          disableBack: true
        });
        $state.go('app.scan.stop', { 'stopIdentifier': stopIdentifier });
      }
    }

    function navigation(data) {
      if (ionic.Platform.isIOS())
        window.open("http://maps.apple.com/?q=" + data, '_system');
      else
        window.open("http://maps.google.com/?q=" + data, '_system');
    }

    function addManifest(manifestPiece, defaultOrderIndex) {
      var manifest = {};
      if (manifestPiece.Origin !== undefined && manifestPiece.Destination === undefined) {
        manifest = {
          'Type': 'pickup',
          'Barcode': manifestPiece.Barcode,
          'SignatureRequirement': manifestPiece.SignatureRequirement,
          'Address': manifestPiece.Origin.Address,
          'Address2': manifestPiece.Origin.Address2,
          'AddressQuality': manifestPiece.Origin.AddressQuality,
          'City': manifestPiece.Origin.City,
          'Contact': manifestPiece.Origin.Contact,
          'Country': manifestPiece.Origin.Country,
          'DeliveryRoute': manifestPiece.Origin.DeliveryRoute,
          'DeliveryRouteSequence': manifestPiece.Origin.DeliveryRouteSequence,
          'FacilityCode': manifestPiece.Origin.FacilityCode,
          'Instruction': manifestPiece.Origin.Instruction,
          'Latitude': manifestPiece.Origin.Latitude,
          'LocationType': manifestPiece.Origin.LocationType,
          'Longitude': manifestPiece.Origin.Longitude,
          'Organization': manifestPiece.Origin.Organization,
          'Phone': manifestPiece.Origin.Phone,
          'PhoneExtension': manifestPiece.Origin.PhoneExtension,
          'PostalCode': manifestPiece.Origin.PostalCode,
          'State': manifestPiece.Origin.State,
          'StopIdentifier': manifestPiece.Origin.StopIdentifier,
          'UTCExpectedDeliveryBy': manifestPiece.Origin.UTCExpectedDeliveryBy,
          'Assigned': '',
          'Attempted': '',
          'Delivered': '',
          'Loaded': '',
          'PickUp': '',
          'StopIdentifierForGrouping': "Origin" + manifestPiece.Origin.StopIdentifier,
          'show': true,
          'showSignature': false,
          'groupLength': "1"

        };
        manifest.defaultOrderIndex = defaultOrderIndex;
        _manifestView.push(manifest);
      }
      if (manifestPiece.Origin === undefined && manifestPiece.Destination !== undefined) {
        manifest = {
          'Type': 'delivery',
          'Barcode': manifestPiece.Barcode,
          'SignatureRequirement': manifestPiece.SignatureRequirement,
          'Address': manifestPiece.Destination.Address,
          'Address2': manifestPiece.Destination.Address2,
          'AddressQuality': manifestPiece.Destination.AddressQuality,
          'City': manifestPiece.Destination.City,
          'Contact': manifestPiece.Destination.Contact,
          'Country': manifestPiece.Destination.Country,
          'DeliveryRoute': manifestPiece.Destination.DeliveryRoute,
          'DeliveryRouteSequence': manifestPiece.Destination.DeliveryRouteSequence,
          'FacilityCode': manifestPiece.Destination.FacilityCode,
          'Instruction': manifestPiece.Destination.Instruction,
          'Latitude': manifestPiece.Destination.Latitude,
          'LocationType': manifestPiece.Destination.LocationType,
          'Longitude': manifestPiece.Destination.Longitude,
          'Organization': manifestPiece.Destination.Organization,
          'Phone': manifestPiece.Destination.Phone,
          'PhoneExtension': manifestPiece.Destination.PhoneExtension,
          'PostalCode': manifestPiece.Destination.PostalCode,
          'State': manifestPiece.Destination.State,
          'StopIdentifier': manifestPiece.Destination.StopIdentifier,
          'UTCExpectedDeliveryBy': manifestPiece.Destination.UTCExpectedDeliveryBy,
          'Assigned': '',
          'Attempted': '',
          'Delivered': '',
          'Loaded': '',
          'PickUp': '',
          'StopIdentifierForGrouping': "Destination" + manifestPiece.Destination.StopIdentifier,
          'show': true,
          'showSignature': false,
          'groupLength': "1"

        };
        manifest.defaultOrderIndex = defaultOrderIndex;
        _manifestView.push(manifest);
      }

      if (manifestPiece.Origin !== undefined && manifestPiece.Destination !== undefined) {
        manifest = {
          'Type': 'delivery',
          'Barcode': manifestPiece.Barcode,
          'SignatureRequirement': manifestPiece.SignatureRequirement,
          'Address': manifestPiece.Destination.Address,
          'Address2': manifestPiece.Destination.Address2,
          'AddressQuality': manifestPiece.Destination.AddressQuality,
          'City': manifestPiece.Destination.City,
          'Contact': manifestPiece.Destination.Contact,
          'Country': manifestPiece.Destination.Country,
          'DeliveryRoute': manifestPiece.Destination.DeliveryRoute,
          'DeliveryRouteSequence': manifestPiece.Destination.DeliveryRouteSequence,
          'FacilityCode': manifestPiece.Destination.FacilityCode,
          'Instruction': manifestPiece.Destination.Instruction,
          'Latitude': manifestPiece.Destination.Latitude,
          'LocationType': manifestPiece.Destination.LocationType,
          'Longitude': manifestPiece.Destination.Longitude,
          'Organization': manifestPiece.Destination.Organization,
          'Phone': manifestPiece.Destination.Phone,
          'PhoneExtension': manifestPiece.Destination.PhoneExtension,
          'PostalCode': manifestPiece.Destination.PostalCode,
          'State': manifestPiece.Destination.State,
          'StopIdentifier': manifestPiece.Destination.StopIdentifier,
          'UTCExpectedDeliveryBy': manifestPiece.Destination.UTCExpectedDeliveryBy,
          'Assigned': '',
          'Attempted': '',
          'Delivered': '',
          'Loaded': '',
          'PickUp': '',
          'StopIdentifierForGrouping': "Destination" + manifestPiece.Destination.StopIdentifier,
          'show': true,
          'showSignature': false,
          'groupLength': "1"
        };
        manifest.defaultOrderIndex = defaultOrderIndex;
        _manifestView.push(manifest);
        manifest = {
          'Type': 'pickup',
          'Barcode': manifestPiece.Barcode,
          'SignatureRequirement': manifestPiece.SignatureRequirement,
          'Address': manifestPiece.Origin.Address,
          'Address2': manifestPiece.Origin.Address2,
          'AddressQuality': manifestPiece.Origin.AddressQuality,
          'City': manifestPiece.Origin.City,
          'Contact': manifestPiece.Origin.Contact,
          'Country': manifestPiece.Origin.Country,
          'DeliveryRoute': manifestPiece.Origin.DeliveryRoute,
          'DeliveryRouteSequence': manifestPiece.Origin.DeliveryRouteSequence,
          'FacilityCode': manifestPiece.Origin.FacilityCode,
          'Instruction': manifestPiece.Origin.Instruction,
          'Latitude': manifestPiece.Origin.Latitude,
          'LocationType': manifestPiece.Origin.LocationType,
          'Longitude': manifestPiece.Origin.Longitude,
          'Organization': manifestPiece.Origin.Organization,
          'Phone': manifestPiece.Origin.Phone,
          'PhoneExtension': manifestPiece.Origin.PhoneExtension,
          'PostalCode': manifestPiece.Origin.PostalCode,
          'State': manifestPiece.Origin.State,
          'StopIdentifier': manifestPiece.Origin.StopIdentifier,
          'UTCExpectedDeliveryBy': manifestPiece.Origin.UTCExpectedDeliveryBy,
          'Assigned': '',
          'Attempted': '',
          'Delivered': '',
          'Loaded': '',
          'PickUp': '',
          'StopIdentifierForGrouping': "Origin" + manifestPiece.Origin.StopIdentifier,
          'show': true,
          'showSignature': false,
          'groupLength': "1"

        };
        manifest.defaultOrderIndex = defaultOrderIndex;
        _manifestView.push(manifest);
      }
      return;
    }

    function addManifestWithPackageStatus(manifestPiece, packageStatus, defaultOrderIndex) {
      var manifest = {};
      if (manifestPiece.Origin !== undefined && manifestPiece.Destination === undefined) {
        manifest = {
          'Type': 'pickup',
          'Barcode': manifestPiece.Barcode,
          'SignatureRequirement': manifestPiece.SignatureRequirement,
          'Address': manifestPiece.Origin.Address,
          'Address2': manifestPiece.Origin.Address2,
          'AddressQuality': manifestPiece.Origin.AddressQuality,
          'City': manifestPiece.Origin.City,
          'Contact': manifestPiece.Origin.Contact,
          'Country': manifestPiece.Origin.Country,
          'DeliveryRoute': manifestPiece.Origin.DeliveryRoute,
          'DeliveryRouteSequence': manifestPiece.Origin.DeliveryRouteSequence,
          'FacilityCode': manifestPiece.Origin.FacilityCode,
          'Instruction': manifestPiece.Origin.Instruction,
          'Latitude': manifestPiece.Origin.Latitude,
          'LocationType': manifestPiece.Origin.LocationType,
          'Longitude': manifestPiece.Origin.Longitude,
          'Organization': manifestPiece.Origin.Organization,
          'Phone': manifestPiece.Origin.Phone,
          'PhoneExtension': manifestPiece.Origin.PhoneExtension,
          'PostalCode': manifestPiece.Origin.PostalCode,
          'State': manifestPiece.Origin.State,
          'StopIdentifier': manifestPiece.Origin.StopIdentifier,
          'UTCExpectedDeliveryBy': manifestPiece.Origin.UTCExpectedDeliveryBy,
          'Assigned': packageStatus[0].json.Assigned,
          'Attempted': packageStatus[0].json.Attempted,
          'Delivered': packageStatus[0].json.Delivered,
          'Loaded': packageStatus[0].json.Loaded,
          'PickUp': packageStatus[0].json.PickUp,
          'StopIdentifierForGrouping': "Origin" + manifestPiece.Origin.StopIdentifier,
          'show': true,
          'showSignature': false,
          'groupLength': "1"

        };
        manifest.defaultOrderIndex = defaultOrderIndex;
        _manifestView.push(manifest);
      }
      if (manifestPiece.Origin === undefined && manifestPiece.Destination !== undefined) {
        manifest = {
          'Type': 'delivery',
          'Barcode': manifestPiece.Barcode,
          'SignatureRequirement': manifestPiece.SignatureRequirement,
          'Address': manifestPiece.Destination.Address,
          'Address2': manifestPiece.Destination.Address2,
          'AddressQuality': manifestPiece.Destination.AddressQuality,
          'City': manifestPiece.Destination.City,
          'Contact': manifestPiece.Destination.Contact,
          'Country': manifestPiece.Destination.Country,
          'DeliveryRoute': manifestPiece.Destination.DeliveryRoute,
          'DeliveryRouteSequence': manifestPiece.Destination.DeliveryRouteSequence,
          'FacilityCode': manifestPiece.Destination.FacilityCode,
          'Instruction': manifestPiece.Destination.Instruction,
          'Latitude': manifestPiece.Destination.Latitude,
          'LocationType': manifestPiece.Destination.LocationType,
          'Longitude': manifestPiece.Destination.Longitude,
          'Organization': manifestPiece.Destination.Organization,
          'Phone': manifestPiece.Destination.Phone,
          'PhoneExtension': manifestPiece.Destination.PhoneExtension,
          'PostalCode': manifestPiece.Destination.PostalCode,
          'State': manifestPiece.Destination.State,
          'StopIdentifier': manifestPiece.Destination.StopIdentifier,
          'UTCExpectedDeliveryBy': manifestPiece.Destination.UTCExpectedDeliveryBy,
          'Assigned': packageStatus[0].json.Assigned,
          'Attempted': packageStatus[0].json.Attempted,
          'Delivered': packageStatus[0].json.Delivered,
          'Loaded': packageStatus[0].json.Loaded,
          'PickUp': packageStatus[0].json.PickUp,
          'StopIdentifierForGrouping': "Destination" + manifestPiece.Destination.StopIdentifier,
          'show': true,
          'showSignature': false,
          'groupLength': "1"

        };
        manifest.defaultOrderIndex = defaultOrderIndex;
        _manifestView.push(manifest);
      }

      if (manifestPiece.Origin != undefined && manifestPiece.Destination != undefined) {
        manifest = {
          'Type': 'delivery',
          'Barcode': manifestPiece.Barcode,
          'SignatureRequirement': manifestPiece.SignatureRequirement,
          'Address': manifestPiece.Destination.Address,
          'Address2': manifestPiece.Destination.Address2,
          'AddressQuality': manifestPiece.Destination.AddressQuality,
          'City': manifestPiece.Destination.City,
          'Contact': manifestPiece.Destination.Contact,
          'Country': manifestPiece.Destination.Country,
          'DeliveryRoute': manifestPiece.Destination.DeliveryRoute,
          'DeliveryRouteSequence': manifestPiece.Destination.DeliveryRouteSequence,
          'FacilityCode': manifestPiece.Destination.FacilityCode,
          'Instruction': manifestPiece.Destination.Instruction,
          'Latitude': manifestPiece.Destination.Latitude,
          'LocationType': manifestPiece.Destination.LocationType,
          'Longitude': manifestPiece.Destination.Longitude,
          'Organization': manifestPiece.Destination.Organization,
          'Phone': manifestPiece.Destination.Phone,
          'PhoneExtension': manifestPiece.Destination.PhoneExtension,
          'PostalCode': manifestPiece.Destination.PostalCode,
          'State': manifestPiece.Destination.State,
          'StopIdentifier': manifestPiece.Destination.StopIdentifier,
          'UTCExpectedDeliveryBy': manifestPiece.Destination.UTCExpectedDeliveryBy,
          'Assigned': packageStatus[0].json.Assigned,
          'Attempted': packageStatus[0].json.Attempted,
          'Delivered': packageStatus[0].json.Delivered,
          'Loaded': packageStatus[0].json.Loaded,
          'PickUp': packageStatus[0].json.PickUp,
          'StopIdentifierForGrouping': "Destination" + manifestPiece.Destination.StopIdentifier,
          'show': true,
          'showSignature': false,
          'groupLength': "1"
        };
        manifest.defaultOrderIndex = defaultOrderIndex;
        _manifestView.push(manifest);
        manifest = {
          'Type': 'pickup',
          'Barcode': manifestPiece.Barcode,
          'SignatureRequirement': manifestPiece.SignatureRequirement,
          'Address': manifestPiece.Origin.Address,
          'Address2': manifestPiece.Origin.Address2,
          'AddressQuality': manifestPiece.Origin.AddressQuality,
          'City': manifestPiece.Origin.City,
          'Contact': manifestPiece.Origin.Contact,
          'Country': manifestPiece.Origin.Country,
          'DeliveryRoute': manifestPiece.Origin.DeliveryRoute,
          'DeliveryRouteSequence': manifestPiece.Origin.DeliveryRouteSequence,
          'FacilityCode': manifestPiece.Origin.FacilityCode,
          'Instruction': manifestPiece.Origin.Instruction,
          'Latitude': manifestPiece.Origin.Latitude,
          'LocationType': manifestPiece.Origin.LocationType,
          'Longitude': manifestPiece.Origin.Longitude,
          'Organization': manifestPiece.Origin.Organization,
          'Phone': manifestPiece.Origin.Phone,
          'PhoneExtension': manifestPiece.Origin.PhoneExtension,
          'PostalCode': manifestPiece.Origin.PostalCode,
          'State': manifestPiece.Origin.State,
          'StopIdentifier': manifestPiece.Origin.StopIdentifier,
          'UTCExpectedDeliveryBy': manifestPiece.Origin.UTCExpectedDeliveryBy,
          'Assigned': packageStatus[0].json.Assigned,
          'Attempted': packageStatus[0].json.Attempted,
          'Delivered': packageStatus[0].json.Delivered,
          'Loaded': packageStatus[0].json.Loaded,
          'PickUp': packageStatus[0].json.PickUp,
          'StopIdentifierForGrouping': "Origin" + manifestPiece.Origin.StopIdentifier,
          'show': true,
          'showSignature': false,
          'groupLength': "1"

        };
        manifest.defaultOrderIndex = defaultOrderIndex;
        _manifestView.push(manifest);
      }
      return;
    }

    function loadCustomData() {
      if (_customViewData.length === 0) {
        var filtereddata = $filter('groupBy')(_manifestView, 'StopIdentifierForGrouping');
        angular.forEach(filtereddata, function (key, index) {
          _customViewData.push(key);
        });
      } else {
        loop1: for (var item in _manifestView) {
          loop2: for (var index in _customViewData) {
            loop3: for (var i in _customViewData[index]) {
              if (_manifestView[item].Barcode == _customViewData[index][i].Barcode) {
                _customViewData[index][i].Assigned = _manifestView[item].Assigned;
                _customViewData[index][i].Attempted = _manifestView[item].Attempted;
                _customViewData[index][i].Delivered = _manifestView[item].Delivered;
                _customViewData[index][i].Loaded = _manifestView[item].Loaded;
                _customViewData[index][i].PickUp = _manifestView[item].PickUp;
                break loop2;
              }
            }
          }
        }
      }
      vm.customlist = _customViewData;
    }

    function loadGroupedView() {
      vm.groupedManifestView = [];
      var grpList = [];
      var orderedData = $filter('orderBy')(_manifestView, vm.orderBy);
      var filtereddata = $filter('filter')(orderedData, vm.filterOptions);
      var groupedData = $filter('groupBy')(filtereddata, 'StopIdentifierForGrouping');

      angular.forEach(groupedData, function (key, val) {
        if (key.length > 1) {
          var length;
          if (key.length == 1) {
            length = 0;
          } else {
            length = ((key.length) - 1);
          }
          var count = 0;
          var showSignatureCount = 0;
          for (var i in key) {
            key[i].groupLength = key.length;
            if (key[i].Delivered == true || key[i].PickUp == true) {
              count++
            }
            if (key[i].SignatureRequirement == 'Required') {
              showSignatureCount++

            }
            if ((i == length) && count == (key.length)) {
              for (var j in key) {
                key[j].show = false;
              }
            }
            if ((i == length) && count != (key.length)) {
              for (var k in key) {
                key[k].show = true;
              }
            }
            if ((i == length) && showSignatureCount >= 1) {
              for (var l in key) {
                key[l].showSignature = true;
              }
            }
          }
          grpList.push(key);
        } else {
          if (key[0].SignatureRequirement == 'Required') {
            key[0].showSignature = true;
          }
          key[0].groupLength = key.length;
          if (key[0].Delivered == true || key[0].PickUp == true) {
            key[0].show = false;
            grpList.push(key);
          } else {
            key[0].show = true;
            grpList.push(key);
          }
        }

      });
      vm.groupedManifestView = grpList;
      if(vm.groupedManifestView.length == 0){
        vm.manifestAvailable = false
      } 
      else {
        vm.manifestAvailable = true
      }      
      notificationService.hideLoad();
    }

  }
})();