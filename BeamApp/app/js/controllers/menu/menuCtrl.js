(function () {
  'use strict';

  angular
    .module('beam')
    .controller('menuCtrl', menuCtrl);

  menuCtrl.$inject = ['$scope', '$stateParams', '$state', '$ionicHistory', '$ionicModal', 'notificationService', 'AuthAdapterService', 'driverService', '$window', '$ionicSideMenuDelegate', 'manifestService', 'packageStatusService', 'scanService', '$location', 'jsonStoreService', 'customerService', 'dataSyncService', 'deviceService', 'AppVersion','$q','$log','rootScopeService','SyncStats'];

  function menuCtrl($scope, $stateParams, $state, $ionicHistory, $ionicModal, notificationService, AuthAdapterService, driverService, $window, $ionicSideMenuDelegate, manifestService,
    packageStatusService, scanService, $location, jsonStoreService, customerService, dataSyncService, deviceService, AppVersion,$q,$log,rootScopeService,SyncStats) {
    var vm = this;
    vm.hideMenuIcons = false;
    vm.onSwipeDown = onSwipeDown;
    vm.onSwipeUp = onSwipeUp;
    vm.logOut = logOut;
    vm.hideMenuIcon = hideMenuIcon;
    vm.refreshManifest = refreshManifest;
    vm.manifestRouting = manifestRouting;
    vm.help = help;
    vm.checkedOutPackages = 0;
    vm.deliveredPackageCount = 0;
    vm.atemptedPackage = 0;
    vm.undeliveredPackage = 0;
    vm.pickUpPackage = 0;
    vm.notAssignedPackage = 0;
    vm.packagesInCustody = 0;
    vm.showDefultImage= true;
    var undeliveredPackage = 0;

    $scope.$on('$ionicView.enter', function () {

     if (ionic.Platform.version() == 4.4) {
        document.addEventListener('touchstart', function (event) {
          if (!$ionicSideMenuDelegate.isOpen()) {
            if($(event.target).attr('id')=='picklemenuicon')
              event.preventDefault();
          }else{
            event.stopImmediatePropagation(); 
            event.preventDefault(); 
            return false; 
          }
        });
      }

      if (driverService.getDriverInfo().OfflineAuth)
        $window.addEventListener('online', reAuthSetup, false);
      $scope.$watch(function () {
        return $ionicSideMenuDelegate.getOpenRatio();
      },
        function (ratio) {
          if (ratio >= 0.5) {
            vm.hideMenuIcons = true;
          } else {
            vm.hideMenuIcons = false;
          }
        });
      $scope.$on('$stateChangeStart', function (e, next, previous, toState, toParams) {
        notificationService.showSpinner();
        $window.localStorage.previousURL = JSON.stringify(toParams);
        $scope.previousURL = toState.name;
        if ((next.name == 'app.scan.load') || (next.name == 'app.scan.delivery') || (next.name == 'app.scan.attempt') || (next.name == 'app.scan.pickup') || (next.name == 'app.scan.location') || (next.name == 'app.scan.door') || (next.name == 'app.scan') || (next.name == 'app.scan.stop')) {
          notificationService.hideLoad();
        }
      });

      manifestService.getManifestCount().then(
        function (data) {
          vm.ckeckoutdata = data;
        },
        function (err) {
          $log.error('Error getting PackagesInCustody count', err);
        }
      );
      packageStatusService.getPickUpPackageCount().then(
        function (data) {
          vm.pickUpcount = data;
        },
        function (err) {
          $log.error('Error getting pickup Package Count', err);
        }
      );
      packageStatusService.getDeliveredPackageCount().then(
        function (data) {
          vm.Deliveredcount = data;
        },
        function (err) {
          $log.error('Error getting Delivered Package Count', err);
        }
      );
    });

    function onSwipeDown() {
      var loc = $location.path().split('/');
      if (("/" + loc[1] + "/" + loc[2] + "/" + loc[3] == '/app/scan/load') || ("/" + loc[1] + "/" + loc[2] + "/" + loc[3] == '/app/scan/delivery') || ("/" + loc[1] + "/" + loc[2] + "/" + loc[3] == '/app/scan/attempt') || ("/" + loc[1] + "/" + loc[2] + "/" + loc[3] == '/app/scan/pickup') || ("/" + loc[1] + "/" + loc[2] + "/" + loc[3] == '/app/scan/location') || ("/" + loc[1] + "/" + loc[2] + "/" + loc[3] == '/app/scan/door')) {
        scanService.hideScanner();
      }
      $q.all([
        deliveredPackageCount(),     
        packagesInCustody(),
        checkedOutPackagesCount1(),
        checkedOutPackagesCount2(),
        attemptedPackageCount(),
        pickupPackageCount(),
        notAssignedPackageCount(),        
      ]).then(function(data){
         vm.deliveredPackageCount = data[0];
         vm.packagesInCustody = data[1];
         vm.checkedOutPackages = parseInt(data[2])+parseInt(data[3]);
         vm.atemptedPackage = data[4];
         vm.pickUpPackage = data[5];
         vm.notAssignedPackage = data[6];
         undeliveredPackage = (parseInt(data[2])+parseInt(data[3]))-(data[0]);
         if(undeliveredPackage > 0){
          vm.undeliveredPackage = undeliveredPackage;
         }else{
          vm.undeliveredPackage = 0;
         }
         moveProgressBar();
      },function(error){
        $log.warn("Error in driver profile Update");
      })
      $ionicModal.fromTemplateUrl('slideDown.html', {
        scope: $scope,
        animation: 'slide-in-down'
      }).then(function (modal) {
        $scope.modal = modal;
        $scope.modal.show();
        var driver = driverService.getDriverInfo();
        vm.driverName = driver.DriverName;
        vm.driverId = driver.driverId;
        if (driver.Photo === undefined || driver.Photo === "") {
           vm.showDefultImage= false;
        } else {
          vm.showDefultImage= true;
          vm.driverImage = driver.Photo;

        }
      });
    }

    function onSwipeUp() {
      $scope.modal.hide();
    }

    function hideMenuIcon() {
      vm.hideMenuIcons = vm.hideMenuIcons === false ? true : false;
    }

    function logOut() {
      notificationService.confirm('Confirm', 'Are you sure you want to log out?', 'Cancel', 'Ok', function () {
        notificationService.hideLoad();
      }, function () {
        processLogout();
        if($window.cordova.plugins.backgroundMode)
          $window.cordova.plugins.backgroundMode.disable();
      });
    }

    function processLogout() {
      var loc = $location.path().split('/');
      if (("/" + loc[1] + "/" + loc[2] + "/" + loc[3] == '/app/scan/load') || ("/" + loc[1] + "/" + loc[2] + "/" + loc[3] == '/app/scan/delivery') || ("/" + loc[1] + "/" + loc[2] + "/" + loc[3] == '/app/scan/attempt') || ("/" + loc[1] + "/" + loc[2] + "/" + loc[3] == '/app/scan/pickup') || ("/" + loc[1] + "/" + loc[2] + "/" + loc[3] == '/app/scan/location') || ("/" + loc[1] + "/" + loc[2] + "/" + loc[3] == '/app/scan/door')) {
        scanService.hideScanner();
      }
      notificationService.showLoad('Logging Out');
      AuthAdapterService.logOut().then(function (data) {
        notificationService.hideLoad();
        $state.go('login');
        dataSyncService.removeSync();
        dataSyncService.cancelLocationPoll();
        driverService.clearDriverInfo();
        dataSyncService.stopEventSyncr();
        deviceService.clearLocationWatcher();
        rootScopeService.clearLocalStorageVariables();
        rootScopeService.clearLoadedLocalVariables();
        $window.localStorage.removeItem("previousURL");
      }, function (err) {
        $log.warn('Error logging out', err);
        notificationService.hideLoad();
        $state.go('login');
        dataSyncService.removeSync();
        dataSyncService.cancelLocationPoll();
        driverService.clearDriverInfo();
        dataSyncService.stopEventSyncr();
        deviceService.clearLocationWatcher();
        rootScopeService.clearLocalStorageVariables();
        rootScopeService.clearLoadedLocalVariables();
        $window.localStorage.removeItem("previousURL");
      });
      if (driverService.getDriverInfo().OfflineAuth)
        $window.removeEventListener('online', reAuthSetup, false);
      SyncStats.synced = false;
      SyncStats.syncInProgress = false;
    }


    function checkedOutPackagesCount1() {
     return  packageStatusService.getLoadedPackageCount();
    }
    function checkedOutPackagesCount2() {
      return packageStatusService.getLoadedCount()
    }
    function deliveredPackageCount() {
      return packageStatusService.getDeliveredPackageCount();
    }
    function attemptedPackageCount() {
     return packageStatusService.getAttemptedPackageCount();
    }
    
    function pickupPackageCount() {
      return packageStatusService.getPickUpPackageCount();
    }
    function notAssignedPackageCount() {
      return packageStatusService.getNotAssignedPackageCount();
    }
    function packagesInCustody() {
      return manifestService.getManifestCount();
    }
    function refreshManifest() {
      vm.hideMenuIcons = vm.hideMenuIcons === false ? true : false;
      var driverInfo = driverService.getDriverInfo();
      driverService.findDriverInfo(driverInfo.driverId).then(function (res) {
        if (res.length > 0)
          notificationService.confirm('Session', 'Resume session or Start New', 'Resume', 'New', function () {
            jsonStoreService.clearCollsforResumeSession().then(function () {
              refreshManifestAndContinue();
            });
          }, function () {
            rootScopeService.clearLoadedLocalVariables();
            jsonStoreService.clearLoadedItems();
            jsonStoreService.clearCollsforNewSession().then(function () {
              refreshManifestAndContinue();
            });
          });
        else
          refreshManifestAndContinue();
        driverService.saveDriverInfoToStore();
      }, function (err) {
        $log.info('Error in finding driverinfo', err);
        driverService.saveDriverInfoToStore();
        $ionicHistory.nextViewOptions({
          disableBack: true
        });
        $state.go('app.manifest');
      });
    }


    function refreshManifestAndContinue() {
      manifestService.refreshManifestInfo().then(function (data) {
        notificationService.hideLoad();
        notificationService.showLoad('Updating Manifest');
        $ionicHistory.nextViewOptions({
          disableBack: true
        });     
        $state.go('app.manifest', {}, {reload: true});
        manifestService.getCustomerIds().then(function (data) {
          customerService.loadCustomerConfig(jQuery.unique(data));
          notificationService.hideLoad();
        }, function (error) {
          $log.error("Error in getting customer configuration", error);
          notificationService.hideLoad();
        });
      }, function (err) {
        notificationService.alert('','','Failed to refresh manifest',function(){
          $ionicHistory.nextViewOptions({
            disableBack: true
          });
          $state.go('app.manifest');
        });
        $log.warn('Failed to refresh Manifest', err);
        notificationService.hideLoad();
      });
    }

    function manifestRouting() {
      vm.hideMenuIcons = vm.hideMenuIcons === false ? true : false;
      var localStorageData = angular.fromJson($window.localStorage.preferenceView);
      if (localStorageData == 'undefined' || localStorageData === '' || angular.isUndefined(localStorageData)) {
        $ionicHistory.nextViewOptions({
          disableBack: true
        });
        $state.go('app.manifest');
      } else {
        if (localStorageData.path == "seqView") {
          $ionicHistory.nextViewOptions({
            disableBack: true
          });
          $state.go('app.seqmanifest');
        }
        if (localStorageData.path == "listView") {
          $ionicHistory.nextViewOptions({
            disableBack: true
          });
          $state.go('app.manifest');
        }
      }
    }
    function help() {
      vm.hideMenuIcons = vm.hideMenuIcons === false ? true : false;
      window.open('http://www.saptalabs.com', '_blank');
    }

    function moveProgressBar() {
      var progressTotal = 0;
      vm.animationLength = 0;
      if (vm.ckeckoutdata > 0 && (vm.Deliveredcount > 0 || vm.pickUpcount > 0)) {
        progressTotal = (((vm.pickUpcount + vm.Deliveredcount) / vm.ckeckoutdata) * 100)+ '%';
        //progressTotal = (progressTotal - 11) + '%';
      }
      $('.progress-bar').stop().animate({
        left: progressTotal
      }, vm.animationLength);
    }

    function reAuthSetup() {
      if (driverService.getDriverInfo().OfflineAuth) {
        $window.removeEventListener('online', reAuthSetup, false);
        processLogout();
      }
    }

  }
})();