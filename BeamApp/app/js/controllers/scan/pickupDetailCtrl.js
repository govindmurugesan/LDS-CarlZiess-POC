(function () {
     'use strict';

     angular
     .module('beam')
     .controller('pickupDetailCtrl', pickupDetailCtrl);

     pickupDetailCtrl.$inject = ['$scope', '$stateParams', '$state' ,'$ionicHistory', 'eventsService', 'manifestService', '$rootScope', 'packageStatusService', 'scanFunctionService','driverService','deviceService','notificationService','$window','$timeout','$log', '$q','rootScopeService'];

     function pickupDetailCtrl($scope, $stateParams, $state, $ionicHistory, eventsService, manifestService, $rootScope, packageStatusService,scanFunctionService, driverService,deviceService,notificationService,$window,$timeout,$log, $q,rootScopeService) {
          var vm = this;
          var name ="";
          var address = "";
          var notes="";
          var customer ="";
          var eventArray = [];
          var groupingGUID = "";
          vm.complete = complete;
          vm.getAddress = getAddress;
          vm.pickupBackRouting = pickupBackRouting;
          var endLocationEvents = [];
          var _pickupScanItems = [];
          var _locationScanItems = [];
          var _unassignPickupScanItems = [];
          var GUIDValue;
          
          $scope.$on('$ionicView.beforeEnter', function(){ 
               _pickupScanItems = rootScopeService.getPickupScanItems();
               _unassignPickupScanItems = rootScopeService.getUnassignPickupScanItems();
               _locationScanItems = rootScopeService.getLocationScanItems();
          })
          $scope.$on("$ionicView.enter", function(){ 
               vm.pickUpType = [];
               var data = driverService.getDriverInfo();
               angular.forEach(data.Containers, function (piece, index) {
                    vm.pickUpType.push(piece.Label);
               })
               angular.forEach(_pickupScanItems, function (piece, index) {
                  groupingGUID = groupingGUID + piece.barcode;
                });
               angular.forEach(_unassignPickupScanItems, function (piece, index) {
                  groupingGUID = groupingGUID + piece.barcode;
                });
               if(_pickupScanItems.length>0){
                    vm.pickUpCount=  _pickupScanItems.length;
                    $('#asgndcntval').val(vm.pickUpCount);
                    vm.pickUpbarcodes=  _pickupScanItems;
                    manifestService.getManifestDetailByBarcode(_pickupScanItems[0].barcode).then(
                         function(data){
                              vm.name= data[0].json.Origin.Contact;
                              vm.address= data[0].json.Origin.Address;
                              vm.state = data[0].json.Origin.City + " "+ data[0].json.Origin.State +" "+ data[0].json.Origin.PostalCode;
                              eventArray.push(data[0].json.Origin.Address);
                              eventArray.push(data[0].json.Origin.Address2);
                              eventArray.push(data[0].json.Origin.PostalCode);
                              eventArray.push(data[0].json.Origin.City);
                              eventArray.push(data[0].json.Origin.State);
                              eventArray.push("US");
                              $scope.$apply();
                              if(vm.address == "Not Available"){
                                   vm.unAssignedAddress = false;
                              }else{
                                   vm.unAssignedAddress = true;
                              }
                         },function(error){
                              $log.error("Error in getting manifest detail",error);
                         }
                         )        
               }else{
                    vm.unassignpickUpCount = "";
                    vm.unassignpickUpbarcodes = "";
                    vm.unassignpickUpCount = _unassignPickupScanItems.length;
                    $('#contval').val(vm.unassignpickUpCount);
                    vm.unassignpickUpbarcodes=  _unassignPickupScanItems;
               }
          });

          $scope.$on("$ionicView.afterEnter", function () {
               notificationService.hideLoad();
          });

          function complete(){
               var data = driverService.getDriverInfo();
               angular.forEach(vm.selectedContainers, function (selectedtype, value) {
                    angular.forEach(data.Containers, function (piece, index) {
                         if (piece.Label == selectedtype) {
                              vm.selectedContainers[value] = piece.Code;
                         }
                    })
               })
               
               GUIDValue = $window.CryptoJS.MD5(groupingGUID).toString();
               if(_pickupScanItems.length >0){
                    notificationService.showTextSpinner('Processing Pickups');
                    if(_unassignPickupScanItems.length >0){
                         var promiseColl = [];
                         angular.forEach(_unassignPickupScanItems, function (piece, index) { 
                              var barcode = piece.barcode;
                              promiseColl.push( 
                                   manifestService.addUnassignPickupManifestDetail(barcode,"Not Available","","").then(function(data){
                                   },function(error){
                                      $log.warn("Error in adding manifest detail",error);
                                   })
                              )
                         });
                         $q.all(promiseColl).then(function(barcodeDetail){
                              _unassignPickupScanItems = [];
                              rootScopeService.saveUnassignPickupScanItems(_unassignPickupScanItems);
                              pickupProcess();
                         },function(error){
                             $log.warn("Error in getting data from manifest adapter", error);
                         })
                    }else{
                         pickupProcess(); 
                    }
               }else{
                    name = vm.unAssignName;
                    var eventActionvalue;
                    if(vm.unAssignAddress == undefined || vm.unAssignAddress == ""){  
                         eventActionvalue ="" + '||||||';
                    }else{
                         address = vm.unAssignAddress.split(',');
                         if(address[2] != undefined){
                              var zipCity = address[2].split(' ');
                              eventActionvalue = (address[0]==undefined?"":address[0].trim()) + '||'+(zipCity[2]==undefined?"":zipCity[2].trim()) +'|'+(address[1]==undefined?"":address[1].trim())+'|'+(zipCity[1]==undefined?"":zipCity[1].trim()) +'|'+"US";                          
                         }
                         else{
                              eventActionvalue = (address[0]==undefined?"":address[0].trim()) + '||'+(address[1]==undefined?"":address[1].trim()) +'|'+(address[2]==undefined?"":address[2].trim()) +'|'+"US";
                         }
                    }
                    notes= vm.unAssignNotes;
                    customer = vm.unAssignCustomer;
                    if(vm.unAssignNotes == undefined){
                        notes = ""; 
                    }
                    if(vm.unAssignName == undefined){
                        name = ""; 
                    }
                    if(vm.unAssignCustomer == undefined){
                        customer = ""; 
                    }
                    deviceService.getLocation().then(function(data){ 
                         if( deviceService.getConnectionStatus() && data.lat != "" && data.long != "" && (vm.unAssignAddress == "" || vm.unAssignAddress == undefined)){
                               notificationService.alert('','','Please enter the address.',function(){
                              })
                         }else{
                              if(_unassignPickupScanItems.length>0){
                                   notificationService.showTextSpinner('Processing Pickups');
                                   var promiseColl = [];
                                   angular.forEach(_unassignPickupScanItems, function (piece, index) { 
                                        var barcode = piece.barcode;
                                        promiseColl.push( 
                                             manifestService.addUnassignPickupManifestDetail(barcode,"Not Available","","").then(function(data){
                                             },function(error){
                                                $log.warn("Error in adding manifest detail",error);
                                             })
                                        )
                                   });
                                   $q.all(promiseColl).then(function(barcodeDetail){
                                        packageStatusService.updateUnassignPickUpStatus(_unassignPickupScanItems).then(function(data){
                                             $ionicHistory.nextViewOptions({
                                                  disableBack: true
                                             });
                                             $state.go('app.manifest');
                                        },function(err){
                                             $log.warn("error in updating package status",err);
                                        });
                                        $q.all([ 
                                          completeEvent(),
                                        ]).then(function(data){
                                             if(_locationScanItems.length !=0){
                                                  var count = 0;
                                                  for (var i=0; i<_unassignPickupScanItems.length; i+=1) {      //chunk pickup events
                                                       var _chunkUnassignPickupEvents = _unassignPickupScanItems.slice(i,i+1);
                                                       count+=_chunkUnassignPickupEvents.length;
                                                       eventsService.pickUpEventUpdate(_chunkUnassignPickupEvents, notes, address, customer,name,eventActionvalue,_locationScanItems[0].barcode,vm.selectedContainers[i], GUIDValue).then(function(data){
                                                            if(_unassignPickupScanItems.length == count ){
                                                                 _unassignPickupScanItems = [];
                                                                 vm.unAssignCustomer = "";
                                                                 vm.unAssignNotes = "";
                                                                 vm.unAssignAddress ="";
                                                                 vm.unAssignName = "";
                                                                 if($rootScope.flag == 'locationpickup'){
                                                                      $timeout(function(){
                                                                           eventsService.locationScanBarcodesEventUpdate(endLocationEvents,'END').then(function(data){
                                                                                _locationScanItems = [];
                                                                                $rootScope.flag = "";
                                                                                if(deviceService.getConnectionStatus()){
                                                                                     eventsService.sendEvents();
                                                                                }
                                                                                scanFunctionService.clearRootScope();
                                                                                rootScopeService.clearLocalStorage();
                                                                           },function(err){
                                                                                $log.warn("Error in location event update",err);
                                                                           })
                                                                      },5000);
                                                                 }else{
                                                                      eventsService.locationScanBarcodesEventUpdate(_locationScanItems,'').then(function(data){
                                                                           _locationScanItems = [];
                                                                           if(deviceService.getConnectionStatus()){
                                                                                eventsService.sendEvents();
                                                                           }
                                                                           scanFunctionService.clearRootScope();
                                                                           rootScopeService.clearLocalStorage();
                                                                      },function(err){
                                                                           $log.warn("Error in location event update",err);
                                                                      })
                                                                 }
                                                            } 
                                                       },function(err){
                                                            $log.warn("Error in pickup event update",err);
                                                       })
                                                  }
                                             }else{
                                                  var count = 0;
                                                  for (var i=0; i<_unassignPickupScanItems.length; i+=1) {      //chunk pickup events
                                                       var _chunkUnassignPickupEvents = _unassignPickupScanItems.slice(i,i+1);
                                                       count+=_chunkUnassignPickupEvents.length;
                                                       eventsService.pickUpEventUpdate(_chunkUnassignPickupEvents, notes, address, customer,name,eventActionvalue,"",vm.selectedContainers[i], GUIDValue).then(function(data){
                                                            if(_unassignPickupScanItems.length == count ){
                                                                 _unassignPickupScanItems = [];
                                                                 vm.unAssignCustomer = "";
                                                                 vm.unAssignNotes = "";
                                                                 vm.unAssignAddress ="";
                                                                 vm.unAssignName = "";
                                                                 if(deviceService.getConnectionStatus()){
                                                                      eventsService.sendEvents();
                                                                 }
                                                                 scanFunctionService.clearRootScope();
                                                                 rootScopeService.clearLocalStorage();
                                                            }
                                                       },function(err){
                                                            $log.warn("Error in unassign pickup event update",err);
                                                       })
                                                  }
                                             }
                                        },function(error){
                                            $log.warn("Error in updating events",error); 
                                        })
                                   },function(error){
                                       $log.warn("Error in getting data from manifest adapter", error);
                                   })
                                   
                              }
                         }
                    })
               }
          }

          function getAddress(){
               deviceService.getLocation().then(function(data){
                         var lat = data.lat;
                         var lng = data.long;
                         var latlng = new google.maps.LatLng(lat, lng);
                         var geocoder = geocoder = new google.maps.Geocoder();
                         if(lat=="" && lng=="" ){
                              $log.info("Location not found");
                              notificationService.alert('','','Location not found.',function(){
                                   setTimeout(function (){
                                      $('#locAdrs').focus();
                                  },0);
                              })
                         }else{
                              geocoder.geocode({ 'latLng': latlng }, function (results, status) {
                              if (status == google.maps.GeocoderStatus.OK) {
                                   if (results) {
                                        angular.forEach(results,function(piece,index){
                                             if(piece.types[0] == "street_address")
                                                  vm.unAssignAddress = piece.formatted_address;
                                             else
                                                  vm.unAssignAddress = results[0].formatted_address;
                                        })
                                        $scope.$apply();
                                   }else{
                                        notificationService.alert('','',"Fails to get location info" ,null);
                                   }
                              }
                         });
                    }
               })
          }

          function pickupBackRouting(){
               $ionicHistory.nextViewOptions({
                    disableBack: true
               });
               $state.go($scope.previousURL);
          }

          $(':input').on("keydown",function(event){
               if (event.keyCode == 13) {
                    cordova.plugins.Keyboard.close();
               }
          });

          $('.manfstCnt').keyup(function(e){
               if (this.value != this.value.replace(/[^0-9\.]/g, '')){ 
                    this.value = this.value.replace(/[^0-9\.]/g, ''); 
               }
          });

          function completeEvent(){
               var completeTime = new Date();
               angular.forEach(_locationScanItems, function (piece, index) {
                    endLocationEvents.push({
                         barcode:piece.barcode,
                         scannedTime:completeTime
                    })
               });
          }

          function pickupProcess(){
               customer =  vm.assignPickUpCustomer;
               notes = vm.assignNotes;
               address = "";
               if(vm.assignNotes == undefined){
                   notes = ""; 
               }
               if(vm.assignPickUpCustomer == undefined){
                   customer = ""; 
               }
               var eventActionvalue;
               if(vm.address == "Not Available"){
                    if(vm.unAssignAddress == undefined || vm.unAssignAddress == ""){  
                         eventActionvalue ="" + '||||||';
                    }else{
                         address = vm.unAssignAddress.split(',');
                         if(address[2] != undefined){
                              var zipCity = address[2].split(' ');
                              eventActionvalue = (address[0]==undefined?"":address[0].trim()) + '||'+(zipCity[2]==undefined?"":zipCity[2].trim()) +'|'+(address[1]==undefined?"":address[1].trim())+'|'+(zipCity[1]==undefined?"":zipCity[1].trim()) +'|'+"US";                          
                         }
                         else{
                              eventActionvalue = (address[0]==undefined?"":address[0].trim()) + '||'+(address[1]==undefined?"":address[1].trim()) +'|'+(address[2]==undefined?"":address[2].trim()) +'|'+"US";
                         }
                    }
               }else{
                   eventActionvalue = eventArray.join("|");
               }
               packageStatusService.updatePickUpStatus(_pickupScanItems).then(function(data){
                    $ionicHistory.nextViewOptions({
                         disableBack: true
                    });
                    $state.go('app.manifest');
               },function(err){
                    $log.warn("Error in updating package status",err);
               })
               $q.all([ 
                 completeEvent(),
               ]).then(function(data){
                    if(_locationScanItems.length !=0){
                         var count = 0;
                         for (var i=0; i<_pickupScanItems.length; i+=1) {      //chunk pickup events
                              var _chunkPickupEvents = _pickupScanItems.slice(i,i+1);
                              count+=_chunkPickupEvents.length;
                              eventsService.pickUpEventUpdate(_chunkPickupEvents, notes, address, customer,name,eventActionvalue,_locationScanItems[0].barcode,vm.selectedContainers[i], GUIDValue).then(function(data){
                                   if(_pickupScanItems.length == count ){
                                        _pickupScanItems = [];
                                        if($rootScope.flag == 'locationpickup'){
                                             $timeout(function(){
                                                  eventsService.locationScanBarcodesEventUpdate(endLocationEvents,'END').then(function(data){
                                                       _locationScanItems = [];
                                                       $rootScope.flag = "";
                                                       if(deviceService.getConnectionStatus()){
                                                         eventsService.sendEvents();
                                                       }
                                                       scanFunctionService.clearRootScope();
                                                       rootScopeService.clearLocalStorage();
                                                  },function(err){
                                                       $log.warn("Error in location event update",err);
                                                  })
                                             },5000);
                                        }else{
                                             eventsService.locationScanBarcodesEventUpdate(_locationScanItems,'').then(function(data){
                                                  _locationScanItems = [];
                                                  if(deviceService.getConnectionStatus()){
                                                    eventsService.sendEvents();
                                                  }
                                                  scanFunctionService.clearRootScope();
                                                  rootScopeService.clearLocalStorage();
                                             },function(err){
                                                  $log.warn("Error in location event update",err);
                                             })
                                        }
                                   } 
                              })
                         }
                    }else{
                         var count = 0;
                         for (var i=0; i<_pickupScanItems.length; i+=1) {      //chunk pickup events
                              var _chunkPickupEvents = _pickupScanItems.slice(i,i+1);
                              count+=_chunkPickupEvents.length;
                              eventsService.pickUpEventUpdate(_chunkPickupEvents, notes, address, customer,name,eventActionvalue,"",vm.selectedContainers[i], GUIDValue).then(function(data){
                                   if(_pickupScanItems.length == count ){
                                        _pickupScanItems = [];
                                        if(deviceService.getConnectionStatus()){
                                          eventsService.sendEvents();
                                        }
                                        scanFunctionService.clearRootScope();
                                        rootScopeService.clearLocalStorage();
                                   }
                              },function(error){
                                   $log.warn("Error in updating pickup event",error);
                              })
                         }
                    }  
               },function(error){
                    $log.warn("Error in updating events",error);
               })
          }
     }

})();