(function () {
  'use strict';

  angular
  .module('beam')
  .controller('scanCtrl', scanCtrl);

  scanCtrl.$inject = ['$scope', '$state', '$stateParams', 'thumbWheelService', '$ionicSideMenuDelegate', '$ionicHistory','scanService', 'notificationService', '$location', 'manifestService', '$rootScope', '$ionicPopup', 'eventsService', 'scanFunctionService', 'packageStatusService', '$window', 'manifestDetailService','$q','customerService','$filter','$log','rootScopeService','$timeout','deviceService','jsonStoreService'];

  function scanCtrl($scope, $state, $stateParams, thumbWheelService, $ionicSideMenuDelegate, $ionicHistory, scanService, notificationService, $location, manifestService, $rootScope, $ionicPopup, eventsService, scanFunctionService, packageStatusService, $window, manifestDetailService,$q,customerService,$filter,$log,rootScopeService,$timeout,deviceService,jsonStoreService) {
    var vm = this;
    vm.detailPage = detailPage;
    vm.buttonChangeEvent = buttonChangeEvent;
    vm.adddetail = adddetail;
    var currentStopIdentifier;
    vm.scannedBarcode = [];
    vm.scannedItems = [];
    vm.CustomerConfig = [];
    var CustomerID = [];
    var promiseColl = [];
    vm.unassignedLoadItems = [];
    vm.loadScanRedirect = loadScanRedirect;
    var unassignedSelectedItems = [];
    var loadedBarcode = [];
    var currentCount = 0;
    var barcodesForCurrentStop = [];
    var barcodesForStop = [];
    var startLocationEvents = [];
    var _loadScanItems = [];
    var _unassignLoadScanItems = [];
    var _deliveryScanItems = [];
    var _attemptScanItems = [];
    var _pickupScanItems = [];
    var _locationScanItems = [];
    var _unassignPickupScanItems = [];
    var _doorScanItems = [];
    var _doorScanItemBarcode = [];
    var _scannedLoadItems = [];
    var _unassignLoadItems = [];
    var _unknownLoadItems = [];
    var locationInfo = "";
    var loadedItems = jsonStoreService.getLoadedItems();
    var unassignLoadedItems = jsonStoreService.getUnassignLoadedItems();

    vm.clearScanItem = [];
    vm.loadedmanifests = [];
    vm.scannedItemsLength = 0;
    vm.totalBarcodesCountToScan = 0;
    var totalBarcodesCount = 0;
    vm.gotoScan = gotoScan;
    vm.gotoSelectScan = gotoSelectScan;
    var lastScanClick = 0;
    vm.leftClick = leftClick;
    vm.rightClick = rightClick;
    vm.barcodesForBinding = [];
    //loadScan Section
    vm.loadScanAction = loadScanAction;
    vm.manifest = manifest;
    vm.onSwipeLoadFirstButton = onSwipeLoadFirstButton;
    vm.onSwipeLoadSecondButton = onSwipeLoadSecondButton;
    /*vm.onSwipeLoadSeventhButton = onSwipeLoadSeventhButton;*/
    vm.loadLeft = loadLeft;
    vm.loadRight = loadRight;
    vm.load_location = load_location;
    vm.onSwipeLoadRightFirstButton = onSwipeLoadRightFirstButton;
    vm.onSwipeLoadLeftFirstButton = onSwipeLoadLeftFirstButton;
    vm.onSwipeLoadRightSecondButton = onSwipeLoadRightSecondButton;
    vm.onSwipeLoadLeftSecondButton = onSwipeLoadLeftSecondButton;
    /*vm.onSwipeLoadRightSeventhButton = onSwipeLoadRightSeventhButton;
    vm.onSwipeLoadLeftSeventhButton = onSwipeLoadLeftSeventhButton;*/

    //deliveryScan Section
    vm.deliveryScanAction = deliveryScanAction;
    vm.onSwipeDeliveryFirstButton = onSwipeDeliveryFirstButton;
    vm.onSwipeDeliverySecondButton = onSwipeDeliverySecondButton;
    /*vm.onSwipeDeliveryThirdButton = onSwipeDeliveryThirdButton;
    vm.onSwipeDeliveryFourthButton = onSwipeDeliveryFourthButton;*/
    vm.onSwipeDeliverySixthButton = onSwipeDeliverySixthButton;
    vm.onSwipeDeliverySeventhButton = onSwipeDeliverySeventhButton;
    vm.deliveryLeft = deliveryLeft;
    vm.deliveryRight = deliveryRight;
    vm.delivery_attempt = delivery_attempt;
    vm.delivery_clear = delivery_clear;
    vm.delivery_pickup = delivery_pickup;
    vm.delivery_location = delivery_location;
    vm.onSwipeDeliveryRightFirstButton = onSwipeDeliveryRightFirstButton;
    vm.onSwipeDeliveryLeftFirstButton = onSwipeDeliveryLeftFirstButton;
    vm.onSwipeDeliveryRightSecondButton = onSwipeDeliveryRightSecondButton;
    vm.onSwipeDeliveryLeftSecondButton = onSwipeDeliveryLeftSecondButton;
    /*vm.onSwipeDeliveryRightThirdButton = onSwipeDeliveryRightThirdButton;
    vm.onSwipeDeliveryLeftThirdButton = onSwipeDeliveryLeftThirdButton;
    vm.onSwipeDeliveryRightFourthButton = onSwipeDeliveryRightFourthButton;
    vm.onSwipeDeliveryLeftFourthButton = onSwipeDeliveryLeftFourthButton;*/
    vm.onSwipeDeliveryRightSixthButton = onSwipeDeliveryRightSixthButton;
    vm.onSwipeDeliveryLeftSixthButton = onSwipeDeliveryLeftSixthButton;
    vm.onSwipeDeliveryRightSeventhButton = onSwipeDeliveryRightSeventhButton;
    vm.onSwipeDeliveryLeftSeventhButton = onSwipeDeliveryLeftSeventhButton;

    //attempScan Section
    vm.attemptScanAction = attemptScanAction;
    vm.onSwipeAttemptFirstButton = onSwipeAttemptFirstButton;
    vm.onSwipeAttemptSecondButton = onSwipeAttemptSecondButton;
   /* vm.onSwipeAttemptThirdButton = onSwipeAttemptThirdButton;
    vm.onSwipeAttemptSixthButton = onSwipeAttemptSixthButton;*/
    vm.onSwipeAttemptSeventhButton = onSwipeAttemptSeventhButton;
    vm.attemptRight = attemptRight;
    vm.attemptLeft = attemptLeft;
    vm.attempt_clear = attempt_clear;
    vm.attempt_location = attempt_location;
    vm.attempt_pickup = attempt_pickup;
    vm.onSwipeAttemptRightFirstButton = onSwipeAttemptRightFirstButton;
    vm.onSwipeAttemptLeftFirstButton = onSwipeAttemptLeftFirstButton;
    vm.onSwipeAttemptRightSecondButton = onSwipeAttemptRightSecondButton;
    vm.onSwipeAttemptLeftSecondButton = onSwipeAttemptLeftSecondButton;
    /*vm.onSwipeAttemptRightThirdButton = onSwipeAttemptRightThirdButton;
    vm.onSwipeAttemptLeftThirdButton = onSwipeAttemptLeftThirdButton;
    vm.onSwipeAttemptRightSixthButton = onSwipeAttemptRightSixthButton;
    vm.onSwipeAttemptLeftSixthButton = onSwipeAttemptLeftSixthButton;*/
    vm.onSwipeAttemptRightSeventhButton = onSwipeAttemptRightSeventhButton;
    vm.onSwipeAttemptLeftSeventhButton = onSwipeAttemptLeftSeventhButton;

    //locationScan Section
    vm.locationScanAction = locationScanAction;
    vm.onSwipeLocationFirstButton = onSwipeLocationFirstButton;
    vm.onSwipeLocationSecondButton = onSwipeLocationSecondButton;
    vm.onSwipeLocationThirdButton = onSwipeLocationThirdButton;
    vm.onSwipeLocationFourthButton = onSwipeLocationFourthButton;
    vm.onSwipeLocationFifthButton = onSwipeLocationFifthButton;
    vm.onSwipeLocationSixthButton = onSwipeLocationSixthButton;
    vm.onSwipeLocationSeventhButton = onSwipeLocationSeventhButton;
    vm.locationLeft = locationLeft;
    vm.locationRight = locationRight;
    vm.location_clear = location_clear;
    vm.location_pickup = location_pickup;
    vm.location_load = location_load;
    vm.location_attempt = location_attempt;
    vm.location_delivery = location_delivery;
    vm.onSwipeLocationRightFirstButton = onSwipeLocationRightFirstButton;
    vm.onSwipeLocationLeftFirstButton = onSwipeLocationLeftFirstButton;
    vm.onSwipeLocationRightSecondButton = onSwipeLocationRightSecondButton;
    vm.onSwipeLocationLeftSecondButton = onSwipeLocationLeftSecondButton;
    vm.onSwipeLocationRightThirdButton = onSwipeLocationRightThirdButton;
    vm.onSwipeLocationLeftThirdButton = onSwipeLocationLeftThirdButton;
    vm.onSwipeLocationRightFourthButton = onSwipeLocationRightFourthButton;
    vm.onSwipeLocationLeftFourthButton = onSwipeLocationLeftFourthButton;
    vm.onSwipeLocationRightFifthButton = onSwipeLocationRightFifthButton;
    vm.onSwipeLocationLeftFifthButton = onSwipeLocationLeftFifthButton;
    vm.onSwipeLocationRightSixthButton = onSwipeLocationRightSixthButton; 
    vm.onSwipeLocationLeftSixthButton = onSwipeLocationLeftSixthButton;
    vm.onSwipeLocationRightSeventhButton = onSwipeLocationRightSeventhButton;
    vm.onSwipeLocationLeftSeventhButton = onSwipeLocationLeftSeventhButton;
    vm.locationDDU = locationDDU;

    //pickupScan Section
    vm.pickupScanAction = pickupScanAction;
    vm.onSwipePickupFirstButton = onSwipePickupFirstButton;
    vm.onSwipePickupSecondButton = onSwipePickupSecondButton;
    vm.onSwipePickupSeventhButton = onSwipePickupSeventhButton;
    vm.pickupLeft = pickupLeft;
    vm.pickupRight = pickupRight;
    vm.pickup_clear = pickup_clear;
    vm.onSwipePickupRightFirstButton = onSwipePickupRightFirstButton;
    vm.onSwipePickupLeftFirstButton = onSwipePickupLeftFirstButton;
    vm.onSwipePickupRightSecondButton = onSwipePickupRightSecondButton;
    vm.onSwipePickupLeftSecondButton = onSwipePickupLeftSecondButton;
    vm.onSwipePickupRightSeventhButton = onSwipePickupRightSeventhButton;
    vm.onSwipePickupLeftSeventhButton = onSwipePickupLeftSeventhButton;

    //doorScan Section
    vm.onSwipeDoorFirstButton = onSwipeDoorFirstButton;
    vm.onSwipeDoorSecondButton = onSwipeDoorSecondButton;
    vm.onSwipeDoorSeventhButton = onSwipeDoorSeventhButton;
    vm.door_clear = door_clear;
    vm.attemptEventTrigger = attemptEventTrigger;
    vm.attemptReasons = attemptReasons; 

    //selectScan Section
    vm.onSelectRightFirstButton = onSelectRightFirstButton;
    vm.onSelectLeftFirstButton = onSelectLeftFirstButton;
    vm.onSelectFirstButton = onSelectFirstButton;
    vm.onSelectRightSecondButton = onSelectRightSecondButton;
    vm.onSelectLeftSecondButton = onSelectLeftSecondButton;
    vm.onSelectSecondButton = onSelectSecondButton;
    vm.onSelectRightThirdButton = onSelectRightThirdButton;
    vm.onSelectLeftThirdButton = onSelectLeftThirdButton;
    vm.onSelectThirdButton = onSelectThirdButton;
    vm.onSelectRightSixthButton = onSelectRightSixthButton;
    vm.onSelectLeftSixthButton = onSelectLeftSixthButton;
    vm.onSelectSixthButton = onSelectSixthButton;
    vm.onSelectRightSeventhButton = onSelectRightSeventhButton;
    vm.onSelectLeftSeventhButton = onSelectLeftSeventhButton;
    vm.onSelectSeventhButton = onSelectSeventhButton;
    vm.selectRightClick = selectRightClick;
    vm.selectLeftClick = selectLeftClick;

    var packagesCountInManifest;
    var scanType = "";

    if (($location.path() == '/app/scan')) {
      vm.scannedItems = [];
      vm.scannedBarcode = [];
      vm.lastScanItem = [];
      vm.scannedItemsLength = 0;
      vm.totalBarcodesCountToScan = 0;
      _loadScanItems = [];
      _unassignLoadScanItems = [];
      _deliveryScanItems = [];
      _attemptScanItems = [];
      _pickupScanItems = [];
      _locationScanItems = [];
      _unassignPickupScanItems = [];
      _doorScanItems = [];
      _doorScanItemBarcode =[];
      saveRootScopes();
    }

    $scope.$on('$ionicView.beforeEnter', function(){ 
      vm.scannedItemsLength = 0;
      vm.totalBarcodesCountToScan = 0;
      vm.scannedItems = [];
      vm.scannedBarcode= [];
      vm.lastScanItem = [];
      _loadScanItems = [];
      _unassignLoadScanItems = [];
      _deliveryScanItems = [];
      _attemptScanItems = [];
      _pickupScanItems = [];
      _locationScanItems = [];
      _unassignPickupScanItems = [];
      _doorScanItems = [];
      _doorScanItemBarcode =[];
      _unknownLoadItems = [];
      $scope.$watch(function() {
        return $state.params;
      });
      currentStopIdentifier = $state.params.stopIdentifier;

      manifestService.getManifestDeliveryCount().then(
        function (data) {
          packagesCountInManifest = data;
          var loc = $location.path().split('/');   
          if(("/"+loc[1]+"/"+loc[2]+"/"+loc[3] == '/app/scan/load') && ($scope.previousURL == 'app.scan')){
            vm.totalBarcodesCountToScan = packagesCountInManifest;
          }      
        },
        function (err) {
          $log.info('Error getting packagesincustody count', err);
        }
      ); 
      
      manifestService.getManifestDetailByStopIdentifier(currentStopIdentifier).then(
        function(data){
          if(data.length > 0 && data[0].json.Destination != undefined && data[0].json.Origin == undefined){
            barcodesForCurrentStop = [];
            vm.packagesForLocation = data; 
            var loc = $location.path().split('/'); 
            if(("/"+loc[1]+"/"+loc[2]+"/"+loc[3] == '/app/scan/selectScan')){
              if($scope.previousURL == 'app.manifest' || $scope.previousURL == 'app.seqmanifest'){
                notificationService.confirm('','Are you at'+' '+data[0].json.Destination.Address+'?', 'Yes', 'No',function(){
                  $log.info('At the current address');
                },function(){
                  $log.info('Not at the current address');
                })
              }
             }  
             if((("/"+loc[1]+"/"+loc[2]+"/"+loc[3] == '/app/scan/delivery') || ("/"+loc[1]+"/"+loc[2]+"/"+loc[3] == '/app/scan/attempt'))){
               vm.totalBarcodesCountToScan = vm.packagesForLocation.length;
               totalBarcodesCount = vm.packagesForLocation.length;
             } 
              angular.forEach(data, function (piece, index) {
                barcodesForCurrentStop.push(piece.json.Barcode);
              });
          }  
        },function(error){
          $log.info('error in getting packages by stopIdentifier',error);
        })
      manifestService.getPickUpDetailByStopIdentifier(currentStopIdentifier).then(
        function(data){
          if(data.length > 0 && data[0].json.Origin != undefined && data[0].json.Destination == undefined){
            vm.packagesForLocation = data; 
            barcodesForStop = [];
            var loc = $location.path().split('/'); 
            if(("/"+loc[1]+"/"+loc[2]+"/"+loc[3] == '/app/scan/selectScan')){
              if($scope.previousURL == 'app.manifest' || $scope.previousURL == 'app.seqmanifest'){
                notificationService.confirm('','Are you at'+' '+data[0].json.Origin.Address+'?', 'Yes', 'No',function(){
                  $log.info('At the current address');
                },function(){
                  $log.info('Not at the current address');
                })
              }
             } 
             if(("/"+loc[1]+"/"+loc[2]+"/"+loc[3] == '/app/scan/pickup')){
               vm.totalBarcodesCountToScan = vm.packagesForLocation.length;
               totalBarcodesCount = vm.packagesForLocation.length;
               angular.forEach(data, function (piece, index) {
                barcodesForStop.push(piece.json.Barcode);
              });
             }  
           }    
        },function(error){
          $log.info('error in getting packages by stopIdentifier',error);
        })
      manifestService.getManifestInfo().then(
        function (data) {
          $scope.$apply(function () {
            angular.forEach(data, function (piece, index) {
              vm.loadedmanifests.push(piece.json);
            });
            notificationService.hideLoad();
          });
        },
        function (err) {
          $log.warn('Error getting manifest data', err);
        }
      );

      manifestService.getManifestDeliveryBarcode().then(
        function (data) {
          var barcodes = [];
          angular.forEach(data, function (piece, index) {
            barcodes.push(piece.json.Barcode);
            rootScopeService.saveLoadBarcodes(barcodes);
          });
          $scope.$apply();       
        },
        function (err) {
          $log.error('Error getting PackagesInCustody count', err);
        }
      );

      manifestService.getManifestPickupBarcode().then(
        function (data) {
          var barcodes = [];
          angular.forEach(data, function (piece, index) {
            barcodes.push(piece.json.Barcode);
            rootScopeService.savePickupBarcodes(barcodes);
          });
          $scope.$apply();       
        },
        function (err) {
          $log.error('Error getting pickup barcodes', err);
        }
      );
      _loadScanItems = rootScopeService.getLoadItems();
      _unassignLoadScanItems = rootScopeService.getUnasignLoadItems();
      _deliveryScanItems = rootScopeService.getDeliveryScanItems();
      _attemptScanItems = rootScopeService.getAttemptScanItems();
      _pickupScanItems = rootScopeService.getPickupScanItems();
      _locationScanItems = rootScopeService.getLocationScanItems();
      _unassignPickupScanItems = rootScopeService.getUnassignPickupScanItems();
      _doorScanItems = rootScopeService.getDoorScanItems();
      _doorScanItemBarcode = rootScopeService.getDoorScanItemBarcode();
      _scannedLoadItems = rootScopeService.getScannedLoadItems();
      _unassignLoadItems = rootScopeService.getScannedUnassignLoadItems();
      _unknownLoadItems = rootScopeService.getUnknownLoadItems();
    });
    

    $scope.$on('$ionicView.enter', function(){ 
      $ionicSideMenuDelegate.canDragContent(false);
      var thumbWheelAnimation = thumbWheelService;
      thumbWheelAnimation.thumbWheelServiceCtr($scope);
      var scanInitial=scanService.initScanner();
      var loc = $location.path().split('/');
      if (($location.path() == '/app/scan')) {
        if(scanInitial)
        {
          scanInitial=false;
        }
      }
      if(($scope.previousURL == 'app.manifest') || ($scope.previousURL == 'app.detail') || ($scope.previousURL == 'app.scan')){
        $rootScope.prePreviousURL = $scope.previousURL;
      }
      if( ("/"+loc[1]+"/"+loc[2]+"/"+loc[3] == '/app/scan/delivery') && ($scope.previousURL == 'app.signature')){
        var count = 1;
        if(_locationScanItems.length > 0){
          angular.forEach(_locationScanItems, function (piece, index) {
            vm.scannedBarcode.push({
              barcode : piece.barcode,
              scantype : 'location'
            });
            vm.scannedItemsLength = count++;
            vm.scannedItems.push(piece.barcode);
          });
        }
        if(_attemptScanItems.length > 0){
          angular.forEach(_attemptScanItems, function (piece, index) {
            vm.scannedBarcode.push({
              barcode : piece.barcode,
              scantype : 'attempt'
            });
            vm.scannedItemsLength = count++;
            vm.scannedItems.push(piece.barcode);
          });
        }
        if(_pickupScanItems.length > 0){
          angular.forEach(_pickupScanItems, function (piece, index) {
            vm.scannedBarcode.push({
              barcode : piece.barcode,
              scantype : 'pickup'
            });
            vm.scannedItemsLength = count++;
            vm.scannedItems.push(piece.barcode);
          });
        }
        if(_unassignPickupScanItems.length > 0){
          angular.forEach(_unassignPickupScanItems, function (piece, index) {
            vm.scannedBarcode.push({
              barcode : piece.barcode,
              scantype : 'pickup'
            });
            vm.scannedItemsLength = count++;
            vm.scannedItems.push(piece.barcode);
          });
        }
        angular.forEach(_deliveryScanItems, function (piece, index) {
          vm.scannedBarcode.push({
              barcode : piece.barcode,
              scantype : 'delivery'
          });
          vm.scannedItemsLength = count++;
          vm.scannedItems.push(piece.barcode);
        });
        if(totalBarcodesCount <= _deliveryScanItems.length){
          vm.totalBarcodesCountToScan = ((totalBarcodesCount)+(_deliveryScanItems.length-$rootScope.BarcodesBelongsToStop.length))
        }
        if(currentStopIdentifier == "" || currentStopIdentifier == undefined){
          vm.totalBarcodesCountToScan = _deliveryScanItems.length + _attemptScanItems.length + _pickupScanItems.length + _unassignPickupScanItems.length + _locationScanItems.length;
        }
        vm.lastScanItem = vm.scannedBarcode.slice(-1)[0].barcode;
        vm.clearScanItem = vm.scannedBarcode.slice(-1)[0].barcode;
        $scope.$apply();
      }
      if((_deliveryScanItems.length > 0 || _locationScanItems.length > 0 || _attemptScanItems.length > 0 || _pickupScanItems.length > 0 || _unassignPickupScanItems.length > 0) && ($scope.previousURL == 'app.pickupDetail')){
        var count = 1;
        if(_locationScanItems.length > 0){
          angular.forEach(_locationScanItems, function (piece, index) {
            vm.scannedBarcode.push({
              barcode : piece.barcode,
              scantype : 'location'
            });
            vm.scannedItemsLength = count++;
            vm.scannedItems.push(piece.barcode);
          });
        }
        if(_attemptScanItems.length > 0){
          angular.forEach(_attemptScanItems, function (piece, index) {
            vm.scannedBarcode.push({
              barcode : piece.barcode,
              scantype : 'attempt'
            });
            vm.scannedItemsLength = count++;
            vm.scannedItems.push(piece.barcode);
          });
        }
        if(_pickupScanItems.length > 0){
          angular.forEach(_pickupScanItems, function (piece, index) {
            vm.scannedBarcode.push({
              barcode : piece.barcode,
              scantype : 'pickup'
            });
            vm.scannedItemsLength = count++;
            vm.scannedItems.push(piece.barcode);
          });
        }
        if(_unassignPickupScanItems.length > 0){
          angular.forEach(_unassignPickupScanItems, function (piece, index) {
            vm.scannedBarcode.push({
              barcode : piece.barcode,
              scantype : 'pickup'
            });
            vm.scannedItemsLength = count++;
            vm.scannedItems.push(piece.barcode);
          });
        }
        angular.forEach(_deliveryScanItems, function (piece, index) {
          vm.scannedBarcode.push({
              barcode : piece.barcode,
              scantype : 'delivery'
            });
          vm.scannedItemsLength = count++;
          vm.scannedItems.push(piece.barcode);
        });
        vm.totalBarcodesCountToScan = vm.scannedItemsLength;
        vm.lastScanItem = vm.scannedBarcode.slice(-1)[0].barcode;
        vm.clearScanItem = vm.scannedBarcode.slice(-1)[0].barcode;
        $scope.$apply();
      }

      if((_deliveryScanItems.length > 0 || _locationScanItems.length > 0 || _attemptScanItems.length > 0 || _pickupScanItems.length > 0 || _unassignPickupScanItems.length > 0) && ($scope.previousURL == 'app.unassignPickupDetail')){
        var count = 1;
        if(_locationScanItems.length > 0){
          angular.forEach(_locationScanItems, function (piece, index) {
            vm.scannedBarcode.push({
              barcode : piece.barcode,
              scantype : 'location'
            });
            vm.scannedItemsLength = count++;
            vm.scannedItems.push(piece.barcode);
          });
        }
        if(_attemptScanItems.length > 0){
          angular.forEach(_attemptScanItems, function (piece, index) {
            vm.scannedBarcode.push({
              barcode : piece.barcode,
              scantype : 'attempt'
            });
            vm.scannedItemsLength = count++;
            vm.scannedItems.push(piece.barcode);
          });
        }
        if(_pickupScanItems.length > 0){
          angular.forEach(_pickupScanItems, function (piece, index) {
            vm.scannedBarcode.push({
              barcode : piece.barcode,
              scantype : 'pickup'
            });
            vm.scannedItemsLength = count++;
            vm.scannedItems.push(piece.barcode);
          });
        }
        if(_unassignPickupScanItems.length > 0){
          angular.forEach(_unassignPickupScanItems, function (piece, index) {
            vm.scannedBarcode.push({
              barcode : piece.barcode,
              scantype : 'pickup'
            });
            vm.scannedItemsLength = count++;
            vm.scannedItems.push(piece.barcode);
          });
        }
        angular.forEach(_deliveryScanItems, function (piece, index) {
          vm.scannedBarcode.push({
              barcode : piece.barcode,
              scantype : 'delivery'
            });
          vm.scannedItemsLength = count++;
          vm.scannedItems.push(piece.barcode);
        });
        vm.totalBarcodesCountToScan = vm.scannedItemsLength;
        vm.lastScanItem = vm.scannedBarcode.slice(-1)[0].barcode;
        vm.clearScanItem = vm.scannedBarcode.slice(-1)[0].barcode;
        $scope.$apply();
      }

      if((_loadScanItems.length > 0  || _locationScanItems !== []) && ($scope.previousURL == 'app.loadreport') && ($location.path() != '/app/scan')){
        var count = 1;
        if(_locationScanItems !== []){
          angular.forEach(_locationScanItems, function (piece, index) {
            vm.scannedBarcode.push({
              barcode : piece.barcode,
              scantype : 'location'
            });
            vm.scannedItemsLength = count++;
            vm.scannedItems.push(piece.barcode);
          });
        }
        if(_unassignLoadScanItems !== []){
          angular.forEach(_unassignLoadScanItems, function (piece, index) {
            if($rootScope.BarcodesBelongsToStop.indexOf(piece.barcode) == -1){
              $rootScope.BarcodesBelongsToStop.push(piece.barcode);
            }
          });
        }
        if(_scannedLoadItems.length >0){
          angular.forEach(_scannedLoadItems, function (piece, index) {
            vm.scannedBarcode.push({
                barcode : piece.barcode,
                scantype : 'load'
            });
            vm.scannedItemsLength = count++;
            vm.scannedItems.push(piece.barcode);
          });
        }

        if(_unassignLoadItems.length >0){
          angular.forEach(_unassignLoadItems, function (piece, index) {
            vm.scannedBarcode.push({
                barcode : piece.barcode,
                scantype : 'load'
            });
            vm.scannedItemsLength = count++;
            vm.scannedItems.push(piece.barcode);
          });
        }

        if($rootScope.unCheckedScanItems.length > 0){
          angular.forEach($rootScope.unCheckedScanItems, function (piece, index) {
            vm.scannedBarcode.push({
                barcode : piece.barcode,
                scantype : 'load'
            });
            vm.scannedItemsLength = count++;
            vm.scannedItems.push(piece.barcode);
            vm.unassignedLoadItems.push(piece.barcode);
          });
        }
        vm.lastScanItem = vm.scannedBarcode.slice(-1)[0].barcode;
        vm.clearScanItem = vm.scannedBarcode.slice(-1)[0].barcode;
        vm.totalBarcodesCountToScan = packagesCountInManifest + $rootScope.unCheckedScanItems.length;
        $scope.$apply();
      }

      $scope.$watch(function () {  
        return $ionicSideMenuDelegate.getOpenRatio();
      },
      function (ratio) {
        if (ratio >= 0.5){ 
          var loc = $location.path().split('/');
          if(("/"+loc[1]+"/"+loc[2]+"/"+loc[3] == '/app/scan/load') || ("/"+loc[1]+"/"+loc[2]+"/"+loc[3] == '/app/scan/delivery') || ("/"+loc[1]+"/"+loc[2]+"/"+loc[3] == '/app/scan/attempt') || ("/"+loc[1]+"/"+loc[2]+"/"+loc[3] == '/app/scan/pickup') || ("/"+loc[1]+"/"+loc[2]+"/"+loc[3] == '/app/scan/location') || ("/"+loc[1]+"/"+loc[2]+"/"+loc[3] == '/app/scan/door')) {
            scanService.hideScanner();
          }
        }else{ 
          var loc = $location.path().split('/');         
          if(("/"+loc[1]+"/"+loc[2]+"/"+loc[3] == '/app/scan/load') || ("/"+loc[1]+"/"+loc[2]+"/"+loc[3] == '/app/scan/delivery') || ("/"+loc[1]+"/"+loc[2]+"/"+loc[3] == '/app/scan/attempt') || ("/"+loc[1]+"/"+loc[2]+"/"+loc[3] == '/app/scan/pickup') || ("/"+loc[1]+"/"+loc[2]+"/"+loc[3] == '/app/scan/location') || ("/"+loc[1]+"/"+loc[2]+"/"+loc[3] == '/app/scan/door')) {
            scanService.showScanner(successScan,errorScan);
          }
        }       
      });
    });

    $scope.$on('$ionicView.leave', function(){
      $ionicSideMenuDelegate.canDragContent(true);
      scanService.hideScanner();
      $window.plugins.insomnia.allowSleepAgain();
    });
    
    $scope.$on("$ionicView.afterEnter", function () {
      notificationService.hideLoad();
      $window.plugins.insomnia.keepAwake();
    });

    function successScan(session){
      $scope.$apply(function(){
      if((vm.scannedItems.indexOf(session.newlyRecognizedCodes[0].data) == -1) || ((session.newlyRecognizedCodes[0].data) == -1 && $scope.previousURL == 'app.signature') || ((session.newlyRecognizedCodes[0].data) != -1 && $scope.previousURL == 'app.scan.attempt')){
        var scannedTime = new Date();
        vm.lastScanItem = [];   
        vm.lastScanItem = session.newlyRecognizedCodes[0].data;
        vm.scannedItems.push(session.newlyRecognizedCodes[0].data);  
        vm.scannedItemsLength = vm.scannedItems.length;
        vm.clearScanItem =  session.newlyRecognizedCodes[0].data;
        var loc = $location.path().split('/');
        scanType = scanFunctionService.scanType();
        vm.scannedTypeIcon =  scanType;
        switch (scanType) {
          case 'delivery':      
          packageStatusService.getPackageByBarcode(vm.lastScanItem).then(function(data){
            if(data.length > 0 && data[0].json.Delivered == true){
              pauseIfScnig();
              notificationService.alert('','','This piece is already delivered',function(){
                rsmeIfPausd();
                if(vm.scannedItemsLength > 0){
                  vm.scannedItemsLength = vm.scannedItemsLength - 1;
                  vm.scannedItems.splice(-1,1);
                }
                if(vm.scannedBarcode.length > 0){
                  vm.lastScanItem = vm.scannedBarcode.slice(-1)[0].barcode;
                }else{
                  vm.lastScanItem = vm.scannedBarcode.slice(-1);
                }
              });
            }else{
              // check with current stop Identifer
            manifestService.getManifestDetailByBarcode(vm.lastScanItem).then(
              function(data){
                if(data.length > 0){
                  if(data[0].json.Destination != undefined && (currentStopIdentifier == "" || currentStopIdentifier == undefined)){
                    currentStopIdentifier = data[0].json.Destination.StopIdentifier;
                    locationInfo = "addressPrompt";
                    manifestService.getManifestDetailByStopIdentifier(currentStopIdentifier).then(
                      function(data){
                        barcodesForCurrentStop = [];
                        vm.packagesForLocation = data; 
                        vm.totalBarcodesCountToScan = vm.packagesForLocation.length;
                        totalBarcodesCount = vm.packagesForLocation.length;
                        angular.forEach(data, function (piece, index) {
                            barcodesForCurrentStop.push(piece.json.Barcode);
                        }); 
                        $scope.$apply();  
                      },function(error){
                        $log.info('error in getting packages by stopIdentifier',error);
                      })
                  }
                  if(data[0].json.Destination != undefined && data[0].json.Destination.Instruction != ""){
                    pauseIfScnig();
                    notificationService.alert('','scanpopup',data[0].json.Destination.Instruction,function(){
                      if(locationInfo == "addressPrompt"){
                        notificationService.confirm('','Are you at'+' '+data[0].json.Destination.Address+'?', 'Yes', 'No',function(event){
                          $(event.toElement).attr('disabled', true);
                          $log.info('At the current address');
                          locationInfo = "";
                           rsmeIfPausd();
                        },function(event){
                          $(event.toElement).attr('disabled', true);
                          $log.info('Not at the current address');
                          locationInfo = "";
                           rsmeIfPausd();
                        })
                      }
                    });
                  }else if(locationInfo == "addressPrompt"){
                    pauseIfScnig();
                    notificationService.confirm('','Are you at'+' '+data[0].json.Destination.Address+'?', 'Yes', 'No',function(event){
                      $(event.toElement).attr('disabled', true);
                      $log.info('At the current address');
                      locationInfo = "";
                      rsmeIfPausd();
                    },function(event){
                      $(event.toElement).attr('disabled', true);
                      $log.info('Not at the current address');
                      locationInfo = "";
                      rsmeIfPausd();
                    })
                  }
                  // current stop Identifer == scanned item stop Identifer
                  if(data[0].json.Destination != undefined && data[0].json.Destination.StopIdentifier == currentStopIdentifier) {
                    currentCount = currentCount+1;
                    vm.scannedBarcode.push({
                      barcode : session.newlyRecognizedCodes[0].data,
                      scantype : 'delivery'
                    });
                    _deliveryScanItems.push({
                      barcode:session.newlyRecognizedCodes[0].data,
                      scannedTime:scannedTime
                    })
                    $rootScope.BarcodesBelongsToStop.push(session.newlyRecognizedCodes[0].data);
                    $scope.$apply();
                  }else if(data[0].json.Origin != undefined){
                    pauseIfScnig();
                    notificationService.alert('','','You’ve scanned an item that is not for delivery',function(){
                      rsmeIfPausd();
                    });
                    if(vm.scannedItemsLength > 0){
                      vm.scannedItemsLength = vm.scannedItemsLength - 1;
                      vm.scannedItems.splice(-1,1);
                    }
                   if(vm.scannedBarcode.length > 0){
                      vm.lastScanItem = vm.scannedBarcode.slice(-1)[0].barcode;
                    }else{
                      vm.lastScanItem = vm.scannedBarcode.slice(-1);
                    }
                  }else if(data[0].json.Destination.StopIdentifier != currentStopIdentifier && currentStopIdentifier != undefined && currentStopIdentifier != ""){
                  // current stop Identifer != scanned item stop Identifer
                    pauseIfScnig();
                    notificationService.confirm('', 'You’ve scanned a piece that’s not at this stop. Continue?', 'Yes', 'No', 
                      function (event) {
                        $(event.toElement).attr('disabled', true);
                        rsmeIfPausd();
                        vm.scannedBarcode.push({
                          barcode : session.newlyRecognizedCodes[0].data,
                          scantype : 'delivery'
                        });
                        _deliveryScanItems.push({
                          barcode:session.newlyRecognizedCodes[0].data,
                          scannedTime:scannedTime
                        })
                        vm.totalBarcodesCountToScan += 1;
                      }, function (event) {
                        $(event.toElement).attr('disabled', true);
                        rsmeIfPausd();
                        if(vm.scannedItemsLength > 0){
                          vm.scannedItemsLength = vm.scannedItemsLength - 1;
                          vm.scannedItems.splice(-1,1);
                        }
                        if(vm.scannedBarcode.length > 0){
                          vm.lastScanItem = vm.scannedBarcode.slice(-1)[0].barcode;
                        }else{
                          vm.lastScanItem = vm.scannedBarcode.slice(-1);
                        }
                      });
                  }else{
                    vm.scannedBarcode.push({
                      barcode : session.newlyRecognizedCodes[0].data,
                      scantype : 'delivery'
                    });
                    _deliveryScanItems.push({
                      barcode:session.newlyRecognizedCodes[0].data,
                      scannedTime:scannedTime
                    });
                    vm.totalBarcodesCountToScan += 1;
                    $scope.$apply();
                  }   
                }else{
                  //data not available in manifest
                  pauseIfScnig();
                  notificationService.confirm('', 'Piece not in manifest. Add it?', 'Yes', 'No', 
                    function (event) {
                      $(event.toElement).attr('disabled', true);
                      rsmeIfPausd();
                      vm.scannedBarcode.push({
                        barcode : session.newlyRecognizedCodes[0].data,
                        scantype : 'delivery'
                      });
                      _deliveryScanItems.push({
                        barcode:session.newlyRecognizedCodes[0].data,
                        scannedTime:scannedTime
                      })
                      vm.totalBarcodesCountToScan += 1;
                      var barcode = session.newlyRecognizedCodes[0].data;
                      manifestDetailService.getManifestDetail(barcode).then(function(data){
                        if(data.Pieces != undefined && data.Pieces.length > 0 && data.Pieces[0].Destination != undefined){
                             manifestService.addManifestData(data,barcode).then(function(data){
                             },function(err){
                              $log.error("Error in getting manifest detail",err);
                             })
                        }else{
                          manifestService.addManifestDetail(barcode,"Not Available","","").then(function(data){
                          },function(error){
                            $log.warn("Error in adding manifest detail",error);
                          });
                        }
                      },function(error){
                        $log.warn("Error in getting data from manifest adapter", error);
                        manifestService.addManifestDetail(barcode,"Not Available","","").then(function(data){
                        },function(error){
                          $log.info("Error in adding manifest detail",error);
                        });
                      })
                        
                    }, function (event) {
                      $(event.toElement).attr('disabled', true);
                      rsmeIfPausd();
                      if(vm.scannedItemsLength > 0){
                        vm.scannedItemsLength = vm.scannedItemsLength - 1;
                        vm.scannedItems.splice(-1,1);
                      }
                      if(vm.scannedBarcode.length > 0){
                        vm.lastScanItem = vm.scannedBarcode.slice(-1)[0].barcode;
                      }else{
                        vm.lastScanItem = vm.scannedBarcode.slice(-1);
                      }
                    });                    
                }
              },function(error){
                $log.info('error in getting packages by barcode',error);
              })
            }
          },function(error){
            $log.info('error in getting package status by barcode',error);
          })  
              
          break;
          case 'load':
            if($window.localStorage.loadBarcodes != undefined && (($window.localStorage.loadBarcodes).indexOf(session.newlyRecognizedCodes[0].data) != -1)){
              loadedBarcode.push({
                barcode:session.newlyRecognizedCodes[0].data,
                scannedTime:scannedTime
              })
              vm.scannedBarcode.push({
                barcode : session.newlyRecognizedCodes[0].data,
                scantype : 'load'
              });                     
              _loadScanItems.push({
                barcode:session.newlyRecognizedCodes[0].data,
                scannedTime:scannedTime
              });
              _scannedLoadItems.push({
                barcode:session.newlyRecognizedCodes[0].data,
                scannedTime:scannedTime
              });
              loadedItems.add(loadedBarcode);
              loadedItems.findAll().then(function (data) {
                  rootScopeService.saveScannedLoadScanItems(data);
              }, function (err) {
                  $log.warn("Failed to save scanned items",err);
              });
              eventsService.loadEventUpdate(loadedBarcode,"", "Loaded").then(function(data){
                loadedBarcode = [];
              },function(err){
                  $log.warn("Error in load eventupdate",err);
              });
            }else if($window.localStorage.pickupBarcodes != undefined && (($window.localStorage.pickupBarcodes).indexOf(session.newlyRecognizedCodes[0].data) != -1)){
              if(vm.scannedItemsLength > 0){
                vm.scannedItemsLength = vm.scannedItemsLength - 1;
                vm.scannedItems.splice(-1,1);
              }
              if(vm.scannedBarcode.length > 0){
                vm.lastScanItem = vm.scannedBarcode.slice(-1)[0].barcode;
              }else{
                vm.lastScanItem = vm.scannedBarcode.slice(-1);
              }
            }else{
              vm.totalBarcodesCountToScan += 1;
              vm.scannedBarcode.push({
                barcode : session.newlyRecognizedCodes[0].data,
                scantype : 'load'
              });                      
              _unassignLoadScanItems.push({
                barcode:session.newlyRecognizedCodes[0].data,
                scannedTime:scannedTime
              })
              var barcode = session.newlyRecognizedCodes[0].data;
              loadedBarcode.push({
                barcode:barcode,
                scannedTime:scannedTime
              })
              _scannedLoadItems.push({
                barcode:barcode,
                scannedTime:scannedTime
              });
              _unknownLoadItems.push({
                barcode:barcode,
                scannedTime:scannedTime
              })
              unassignLoadedItems.add(loadedBarcode);
              unassignLoadedItems.findAll().then(function (data) {
                  rootScopeService.unassignLoadItems(data);
              }, function (err) {
                  $log.warn("Failed to save scanned items",err);
              });
              rootScopeService.saveUnknownLoadItems(_unknownLoadItems);
              eventsService.loadEventUpdate(loadedBarcode,"", "Loaded").then(function(data){
                loadedBarcode = [];
              },function(err){
                  $log.warn("Error in load event update",err);
              }); 
            }            
          break;
          case 'attempt': 
              // check with current stop Identifer
            manifestService.getManifestDetailByBarcode(vm.lastScanItem).then(
              function(data){
                if(data.length > 0){
                  if(data[0].json.Destination != undefined && (currentStopIdentifier == "" || currentStopIdentifier == undefined)){
                    currentStopIdentifier = data[0].json.Destination.StopIdentifier;
                    locationInfo = "addressPrompt";
                    manifestService.getManifestDetailByStopIdentifier(currentStopIdentifier).then(
                      function(data){
                        barcodesForCurrentStop = [];
                        vm.packagesForLocation = data; 
                        vm.totalBarcodesCountToScan = vm.packagesForLocation.length;
                        totalBarcodesCount = vm.packagesForLocation.length;
                        angular.forEach(data, function (piece, index) {
                            barcodesForCurrentStop.push(piece.json.Barcode);
                        }); 
                        $scope.$apply();  
                      },function(error){
                        $log.info('error in getting packages by stopIdentifier',error);
                      })
                  }
                  if(data[0].json.Destination != undefined && data[0].json.Destination.Instruction != ""){
                    pauseIfScnig();
                    notificationService.alert('','scanpopup',data[0].json.Destination.Instruction,function(){
                      if(locationInfo == "addressPrompt"){
                        notificationService.confirm('','Are you at'+' '+data[0].json.Destination.Address+'?', 'Yes', 'No',function(event){
                          $(event.toElement).attr('disabled', true);
                          $log.info('At the current address');
                          locationInfo = "";
                           rsmeIfPausd();
                        },function(event){
                          $(event.toElement).attr('disabled', true);
                          $log.info('Not at the current address');
                          locationInfo = "";
                           rsmeIfPausd();
                        })
                      }
                    });
                  }else if(locationInfo == "addressPrompt"){
                    pauseIfScnig();
                    notificationService.confirm('','Are you at'+' '+data[0].json.Destination.Address+'?', 'Yes', 'No',function(event){
                      $(event.toElement).attr('disabled', true);
                      $log.info('At the current address');
                      locationInfo = "";
                      rsmeIfPausd();
                    },function(event){
                      $(event.toElement).attr('disabled', true);
                      $log.info('Not at the current address');
                      locationInfo = "";
                      rsmeIfPausd();
                    })
                  }
                  // current stop Identifer == scanned item stop Identifer
                  if(data[0].json.Destination != undefined && data[0].json.Destination.StopIdentifier == currentStopIdentifier) {
                    currentCount = currentCount+1;
                    vm.scannedBarcode.push({
                      barcode : session.newlyRecognizedCodes[0].data,
                      scantype : 'attempt'
                    }); 
                    _attemptScanItems.push({
                      barcode:session.newlyRecognizedCodes[0].data,
                      scannedTime:scannedTime
                    });
                    $rootScope.BarcodesBelongsToStop.push(session.newlyRecognizedCodes[0].data);
                    $scope.$apply();
                  }else if(data[0].json.Destination != undefined && data[0].json.Destination.StopIdentifier != currentStopIdentifier && currentStopIdentifier != undefined && currentStopIdentifier != ""){
                  // current stop Identifer != scanned item stop Identifer
                    pauseIfScnig();
                    notificationService.confirm('', 'You’ve scanned a piece that’s not at this stop. Continue?', 'Yes', 'No', 
                      function (event) {
                        $(event.toElement).attr('disabled', true);
                        rsmeIfPausd();
                        vm.scannedBarcode.push({
                          barcode : session.newlyRecognizedCodes[0].data,
                          scantype : 'attempt'
                        }); 
                        _attemptScanItems.push({
                          barcode:session.newlyRecognizedCodes[0].data,
                          scannedTime:scannedTime
                        });
                        vm.totalBarcodesCountToScan += 1;
                      }, function (event) {
                        $(event.toElement).attr('disabled', true);
                        rsmeIfPausd();
                        if(vm.scannedItemsLength > 0){
                          vm.scannedItemsLength = vm.scannedItemsLength - 1;
                          vm.scannedItems.splice(-1,1);
                        }
                        if(vm.scannedBarcode.length > 0){
                          vm.lastScanItem = vm.scannedBarcode.slice(-1)[0].barcode;
                        }else{
                          vm.lastScanItem = vm.scannedBarcode.slice(-1);
                        }
                      });

                  }else if(data[0].json.Origin != undefined){
                    pauseIfScnig();
                    notificationService.alert('','','You’ve scanned a Pickup Item',function(){
                        rsmeIfPausd();
                    });
                    if(vm.scannedItemsLength > 0){
                      vm.scannedItemsLength = vm.scannedItemsLength - 1;
                      vm.scannedItems.splice(-1,1);
                    }
                    if(vm.scannedBarcode.length > 0){
                      vm.lastScanItem = vm.scannedBarcode.slice(-1)[0].barcode;
                    }else{
                      vm.lastScanItem = vm.scannedBarcode.slice(-1);
                    }
                  }else{
                    vm.scannedBarcode.push({
                      barcode : session.newlyRecognizedCodes[0].data,
                      scantype : 'attempt'
                    }); 
                    _attemptScanItems.push({
                      barcode:session.newlyRecognizedCodes[0].data,
                      scannedTime:scannedTime
                    });
                    vm.totalBarcodesCountToScan += 1;
                  }   
                }else{
                  //data not available in manifest
                  pauseIfScnig();
                  notificationService.confirm('', 'Piece not in manifest. Add it?', 'Yes', 'No', 
                    function (event) {
                      $(event.toElement).attr('disabled', true);
                      rsmeIfPausd();
                      vm.scannedBarcode.push({
                        barcode : session.newlyRecognizedCodes[0].data,
                        scantype : 'attempt'
                      }); 
                      _attemptScanItems.push({
                        barcode:session.newlyRecognizedCodes[0].data,
                        scannedTime:scannedTime
                      });
                      vm.totalBarcodesCountToScan += 1;
                      var barcode = session.newlyRecognizedCodes[0].data;
                      manifestDetailService.getManifestDetail(barcode).then(function(data){
                        if(data.Pieces != undefined && data.Pieces.length > 0 && data.Pieces[0].Destination != undefined){
                             manifestService.addManifestData(data,barcode).then(function(data){
                             },function(err){
                              $log.error("Error in getting manifest detail",err);
                             })
                        }else{
                          manifestService.addManifestDetail(barcode,"Not Available","","").then(function(data){
                          },function(error){
                            $log.warn("Error in adding manifest detail",error);
                          });
                        }
                      },function(error){
                         $log.warn("Error in getting data from manifest adapter", error);
                         manifestService.addManifestDetail(barcode,"Not Available","","").then(function(data){
                        },function(error){
                          $log.info("Error in adding manifest detail",error);
                        });
                      })
                    }, function (event) {
                      rsmeIfPausd();
                      $(event.toElement).attr('disabled', true);
                      if(vm.scannedItemsLength > 0){
                        vm.scannedItemsLength = vm.scannedItemsLength - 1;
                        vm.scannedItems.splice(-1,1);
                      }
                      if(vm.scannedBarcode.length > 0){
                        vm.lastScanItem = vm.scannedBarcode.slice(-1)[0].barcode;
                      }else{
                        vm.lastScanItem = vm.scannedBarcode.slice(-1);
                      }
                    });                    
                }
                attemptReasons();
              },function(error){
                $log.info('Error in getting packages by barcode',error);
              })
          break;
          case 'pickup':
            if($window.localStorage.pickupBarcodes != undefined && (($window.localStorage.pickupBarcodes).indexOf(session.newlyRecognizedCodes[0].data) != -1)){
              if(barcodesForStop.indexOf(session.newlyRecognizedCodes[0].data) != -1){
                vm.scannedBarcode.push({
                  barcode : session.newlyRecognizedCodes[0].data,
                  scantype : 'pickup'
                }); 
                _pickupScanItems.push({
                  barcode:session.newlyRecognizedCodes[0].data,
                  scannedTime:scannedTime
                });
              }else{
                if(currentStopIdentifier == "" || currentStopIdentifier == undefined){
                  manifestService.getManifestDetailByBarcode(vm.lastScanItem).then(
                    function(data){
                      if(data.length > 0){
                        currentStopIdentifier = data[0].json.Origin.StopIdentifier;
                        pauseIfScnig();
                        notificationService.confirm('','Are you at'+' '+data[0].json.Origin.Address+'?', 'Yes', 'No',function(event){
                          $(event.toElement).attr('disabled', true);
                          $log.info('At the current address');
                          rsmeIfPausd();
                        },function(event){
                          $(event.toElement).attr('disabled', true);
                          $log.info('Not at the current address');
                          rsmeIfPausd();
                        })
                      }
                    },function(error){
                      $log.info("Error in adding manifest detail",error);
                    });
                }
                vm.scannedBarcode.push({
                  barcode : session.newlyRecognizedCodes[0].data,
                  scantype : 'pickup'
                }); 
                _pickupScanItems.push({
                  barcode:session.newlyRecognizedCodes[0].data,
                  scannedTime:scannedTime
                });
                vm.totalBarcodesCountToScan += 1;
              }
            }else{
              vm.scannedBarcode.push({
                barcode : session.newlyRecognizedCodes[0].data,
                scantype : 'pickup'
              }); 
              _unassignPickupScanItems.push({
                barcode:session.newlyRecognizedCodes[0].data,
                scannedTime:scannedTime
              });
              vm.totalBarcodesCountToScan += 1;
            }
          break;
          case 'location':
            vm.scannedBarcode.push({
              barcode : session.newlyRecognizedCodes[0].data,
              scantype : 'location'
            });
            _locationScanItems.push({
              barcode:session.newlyRecognizedCodes[0].data,
              scannedTime:scannedTime
            });
            vm.totalBarcodesCountToScan += 1;

            if(("/"+loc[1]+"/"+loc[2]+"/"+loc[3] == '/app/scan/location')){
              locationDDU();
            } 
            
          break;
          case 'clear': 
            $log.info('clear scanner');

          break;
          case 'door': 
            vm.scannedBarcode.push({
              barcode : session.newlyRecognizedCodes[0].data,
              scantype : 'door'
            }); 
            _doorScanItems.push({
              barcode:session.newlyRecognizedCodes[0].data,
              scannedTime:scannedTime
            });
            vm.totalBarcodesCountToScan += 1;
            doorScanAction();
          break;
          case '': 
            $log.info('Not Selected any scanner');

          break;
        }
      }else{
         var loc = $location.path().split('/');      
        if(("/"+loc[1]+"/"+loc[2]+"/"+loc[3] != '/app/scan/load') && ("/"+loc[1]+"/"+loc[2]+"/"+loc[3] != '/app/scan/pickup')){
            pauseIfScnig();
            notificationService.alert('','','This item already scanned',function(){
              rsmeIfPausd();
            });
        }   
      }
      });
    }

    function errorScan(error){
      if(error == 'Canceled'){
        console.warn('Error Scan :', error);
      }else{
        notificationService.alert('','',error,function(){
          $ionicHistory.nextViewOptions({
            disableBack: true
          });
          $state.go('app.scan');
        });
      }
    }

    function gotoScan(scantype){
      var currentScanClick = new Date();
      currentScanClick = currentScanClick.getTime();
      if((currentScanClick - lastScanClick) <= 500){
        lastScanClick = currentScanClick;
      }else{
        lastScanClick = currentScanClick;
        if(scantype == 'load')
          loadScan();
        else if(scantype == 'delivery')
          deliveryScan();
        else if(scantype == 'attempt')
          attemptScan();
        else if(scantype == 'pickup')
          pickupScan();
        else if(scantype == 'location')
          locationScan();
      }    
    }

    function deliveryScan() {
      if(vm.scannedItemsLength > 0){
        vm.scannedItemsLength = 0;
        vm.totalBarcodesCountToScan = 0;
        vm.scannedItems = [];
        vm.scannedBarcode= [];
        vm.lastScanItem = [];
      }
      $ionicHistory.nextViewOptions({
        disableBack: true
      });
      $timeout(function(){
        $state.go('app.scan.delivery',{'stopIdentifier': ''});
      },250);
      scanService.showScanner(successScan,errorScan);
    }

    function locationScan() {
      if(vm.scannedItemsLength > 0){
        vm.scannedItemsLength = 0;
        vm.totalBarcodesCountToScan = 0;
        vm.scannedItems = [];
        vm.scannedBarcode= [];
        vm.lastScanItem = [];
      }
      $ionicHistory.nextViewOptions({
        disableBack: true
      });
      $timeout(function(){
        $state.go('app.scan.location',{'stopIdentifier': ''});
      },250);
      scanService.showScanner(successScan,errorScan);
    }

    function loadScan() {
      var count = 1;
        if(_scannedLoadItems.length >0){
          angular.forEach(_scannedLoadItems, function (piece, index) {
            vm.scannedBarcode.push({
                barcode : piece.barcode,
                scantype : 'load'
            });
            vm.scannedItemsLength = count++;
            vm.scannedItems.push(piece.barcode);
          });
        }
        if(_unassignLoadItems.length >0){
          angular.forEach(_unassignLoadItems, function (piece, index) {
            vm.scannedBarcode.push({
                barcode : piece.barcode,
                scantype : 'load'
            });
            vm.scannedItemsLength = count++;
            vm.scannedItems.push(piece.barcode);
          });
        }
        if(vm.scannedBarcode.length > 0){
          vm.lastScanItem = vm.scannedBarcode.slice(-1)[0].barcode;
          vm.clearScanItem = vm.scannedBarcode.slice(-1)[0].barcode;
        }
      scanService.showScanner(successScan,errorScan);
      manifestService.getManifestDeliveryBarcode().then(
        function (data) {
          packagesCountInManifest = data.length;
          vm.totalBarcodesCountToScan = packagesCountInManifest; 
          $scope.$apply();       
        },
        function (err) {
          $log.error('Error getting PackagesInCustody count', err);
        }
      );
      $ionicHistory.nextViewOptions({
        disableBack: true
      });
      $timeout(function(){
        $state.go('app.scan.load',{'stopIdentifier': ''});
      },250);
    }

    function pickupScan() {
      if(vm.scannedItemsLength > 0){
        vm.scannedItemsLength = 0;
        vm.totalBarcodesCountToScan = 0;
        vm.scannedItems = [];
        vm.scannedBarcode= [];
        vm.lastScanItem = [];
      }
      $ionicHistory.nextViewOptions({
        disableBack: true
      });
      $timeout(function(){
        $state.go('app.scan.pickup',{'stopIdentifier': ''});
      },250);
      scanService.showScanner(successScan,errorScan);
    }

    function attemptScan() {
      if(vm.scannedItemsLength > 0){
        vm.scannedItemsLength = 0;
        vm.totalBarcodesCountToScan = 0;
        vm.scannedItems = [];
        vm.scannedBarcode= [];
        vm.lastScanItem = [];
      }
      $ionicHistory.nextViewOptions({
        disableBack: true
      });
      $timeout(function(){
        $state.go('app.scan.attempt',{'stopIdentifier': ''});
      },250);
      scanService.showScanner(successScan,errorScan);
    }    
    
    function leftClick(){
      var classNames = $('.swipeCont').attr('class').split(" ");
      var className = classNames[2];
      switch (className) {
        case 'first':               
        $('.swipeCont.'+className).removeClass(className).addClass('second');
        break;
        case 'second':
        $('.swipeCont.'+className).removeClass(className).addClass('third');
        break;
        case 'third':
        $('.swipeCont.'+className).removeClass(className).addClass('fourth');
        break;
        case 'fourth':
        $('.swipeCont.'+className).removeClass(className).addClass('fifth');
        break;
        case 'fifth':
        $('.swipeCont.'+className).removeClass(className).addClass('sixth');
        break;
        case 'sixth': 
        $('.swipeCont.'+className).removeClass(className).addClass('seventh');
        break;
        case 'seventh': 
        $('.swipeCont.'+className).removeClass(className).addClass('first');
        break;
      }
    }
    function rightClick(){
      var classNames = $('.swipeCont').attr('class').split(" ");
      var className = classNames[2];
      switch (className) {
        case 'first':               
        $('.swipeCont.'+className).removeClass(className).addClass('seventh');
        break;
        case 'second':
        $('.swipeCont.'+className).removeClass(className).addClass('first');
        break;
        case 'third':
        $('.swipeCont.'+className).removeClass(className).addClass('second');
        break;
        case 'fourth':
        $('.swipeCont.'+className).removeClass(className).addClass('third');
        break;
        case 'fifth':
        $('.swipeCont.'+className).removeClass(className).addClass('fourth');
        break;
        case 'sixth': 
        $('.swipeCont.'+className).removeClass(className).addClass('fifth');
        break;
        case 'seventh': 
        $('.swipeCont.'+className).removeClass(className).addClass('sixth');
        break;
      }
    }

    //loadScan functionality
    function loadScanAction() {
      if(angular.element('#loadAction').hasClass('first') && angular.element('#l_load').hasClass('boxshadow')){
        $('#l_load').addClass('boxshadow');
        $('#load_location, #load_manifest').removeClass('boxshadow');
      }else{
        scanService.hideScanner();
          loadScanRedirect();
      }
    }

    function manifest(){
      $ionicHistory.nextViewOptions({
        disableBack: true
      });
      $state.go('app.manifest');
    }

    function onSwipeLoadFirstButton() { 
      $('.loadcont,.loadLeft,.loadRight').removeClass('second third fourth fifth sixth seventh').addClass('first');
      $('#l_load').toggleClass('boxshadow');
      $('#load_location, #load_manifest').removeClass('boxshadow');
    }

    function onSwipeLoadSecondButton() {
      $('.loadcont').removeClass('first third fourth fifth sixth seventh').addClass('second');
      $('#load_manifest').toggleClass('boxshadow');
      $('#load_location, #l_load').removeClass('boxshadow');
    }

    /*function onSwipeLoadSeventhButton() {
      $('.loadcont').removeClass('first second third fifth fourth sixth').addClass('seventh');
      $('#load_location').toggleClass('boxshadow');
      $('#l_load, #load_manifest').removeClass('boxshadow');
      $('#l_load, #load_manifest').removeClass('boxshadow');
    }*/

    function onSwipeLoadRightFirstButton() { 
      $('.loadcont').removeClass('first second third fourth fifth sixth').addClass('seventh');
      $('#load_manifest').toggleClass('boxshadow');
      $('#l_load').removeClass('boxshadow');
    }

    function onSwipeLoadLeftFirstButton() { 
      $('.loadcont').removeClass('first third fourth fifth sixth seventh').addClass('second');
      $('#l_load').toggleClass('boxshadow');
      $('#load_manifest').removeClass('boxshadow');
    }

    function onSwipeLoadRightSecondButton() { 
      $('.loadcont').removeClass('second third fourth fifth sixth seventh').addClass('first');
      $('#l_load').toggleClass('boxshadow');
      $('#load_location, #load_manifest').removeClass('boxshadow');
    }

    function onSwipeLoadLeftSecondButton() { 
      $('.loadcont').removeClass('first second third fourth fifth').addClass('seventh');
      $('#load_manifest').toggleClass('boxshadow');
      $('#l_load').removeClass('boxshadow');
    }

    /*function onSwipeLoadRightSeventhButton() { 
      $('.loadcont').removeClass('first second third fourth fifth').addClass('seventh');
      $('#load_manifest').toggleClass('boxshadow');
      $('#l_load, #load_location').removeClass('boxshadow');
    }

    function onSwipeLoadLeftSeventhButton() { 
      $('.loadcont').removeClass('second third fourth fifth sixth seventh').addClass('first');
      $('#l_load').toggleClass('boxshadow');
      $('#load_location, #load_manifest').removeClass('boxshadow');
    }*/

    function loadLeft(){
      var classNames = $('.loadcont').attr('class').split(" ");
      var className = classNames[2];
      switch (className) {
        case 'first':               
        $('.loadcont.'+className).removeClass(className).addClass('second');
        $('#load_manifest').toggleClass('boxshadow');
        $('#load_location, #l_load').removeClass('boxshadow');
        break;
        case 'second':
        $('.loadcont.'+className).removeClass(className).addClass('first');
        $('#l_load').toggleClass('boxshadow');
        $('#load_manifest').removeClass('boxshadow');
        break;
        /*case 'seventh': 
        $('.loadcont.'+className).removeClass(className).addClass('first');
        $('#l_load').toggleClass('boxshadow');
        $('#load_location, #load_manifest').removeClass('boxshadow');
        break;*/
      }
    }

    function loadRight(){
      var classNames = $('.loadcont').attr('class').split(" ");
      var className = classNames[2];
      switch (className) {
        case 'first':               
        $('.loadcont.'+className).removeClass(className).addClass('second');
        $('#load_manifest').toggleClass('boxshadow');
        $('#l_load').removeClass('boxshadow');
        break;
        case 'second':
        $('.loadcont.'+className).removeClass(className).addClass('first');
        $('#l_load').toggleClass('boxshadow');
        $('#load_location, #load_manifest').removeClass('boxshadow');
        break;
        /*case 'seventh': 
        $('.loadcont.'+className).removeClass(className).addClass('second');
        $('#load_manifest').toggleClass('boxshadow');
        $('#load_location, #l_load').removeClass('boxshadow');
        break;*/
      }
    }

   function load_location(){
     $('#load_location').addClass('boxshadow');
     $('#l_load, #load_manifest').removeClass('boxshadow');
   }
    //deliveryScan functionality
    function deliveryScanAction() {
      if(angular.element('#deliveryAction').hasClass('first') && angular.element('#d_delivery').hasClass('boxshadow')){
        $('#d_delivery').addClass('boxshadow');
        $('#delivery_attempt, #delivery_clear, #delivery_pickup, #delivery_location, #delivery_manifest').removeClass('boxshadow');

      }else{
        scanService.hideScanner();
        if(vm.scannedItems.length == '0'){
          notificationService.alert('','','You have not scanned a barcode.',function(){
            $ionicHistory.nextViewOptions({
              disableBack: true
            });
            if($scope.previousURL == 'app.signature'){
              if($scope.prePreviousURL == 'app.detail'){
                $state.go($rootScope.prePreviousURL, { 'stopIdentifier': currentStopIdentifier, 'type': 'delivery'});
              }else{
                $state.go($rootScope.prePreviousURL);
              }
            }else{
              var previousURLParams = angular.fromJson(window.localStorage["previousURL"]);
              if($scope.previousURL == 'app.scan.stop'){
                if($window.localStorage["previousURL"] == "{}"){
                  $state.go($scope.prevURL);
                }else{
                  $state.go($scope.prevURL, { 'stopIdentifier': previousURLParams.stopIdentifier, 'type': previousURLParams.type});
                }
              }else{
                if($window.localStorage["previousURL"] == "{}"){
                  $state.go($scope.previousURL);
                }else{
                  $state.go($scope.previousURL, { 'stopIdentifier': previousURLParams.stopIdentifier, 'type': previousURLParams.type});
                }
              }
            }
          })
        }else{ 
          if(_unassignPickupScanItems.length > 0){
            if(_pickupScanItems.length > 0){
              var deferred = $q.defer();
              var promises = [];
              promises.push(UnassignPickUpBarcodeLoading());
              $q.all(promises).then(function (data) {
                //_unassignPickupScanItems = [];
                saveRootScopes();
                deliveryProcess();
              })
            }else{
              saveRootScopes();
              deliveryProcess();
            }
          }else{
            deliveryProcess();
          }
        }
      }
   }
    function onSwipeDeliveryFirstButton() {
      $('.deliverycont,.deliveryLeft,.deliveeryRight').removeClass('second third fourth fifth sixth seventh').addClass('first');
      $('#d_delivery').toggleClass('boxshadow');
      $('#delivery_attempt, #delivery_clear, #delivery_pickup, #delivery_location, #delivery_manifest').removeClass('boxshadow');
    };
    function onSwipeDeliverySecondButton() {
      $('.deliverycont').removeClass('first third fourth fifth sixth seventh').addClass('second');
      $('#delivery_clear').toggleClass('boxshadow');
      $('#delivery_attempt, #delivery_pickup, #delivery_location, #d_delivery, #delivery_manifest').removeClass('boxshadow');
    };
    /*function onSwipeDeliveryThirdButton() {
      $('.deliverycont').removeClass('first second fourth fifth sixth seventh').addClass('third');
      $('#delivery_pickup').toggleClass('boxshadow');
      $('#delivery_attempt, #delivery_clear, #delivery_location, #d_delivery, #delivery_manifest').removeClass('boxshadow');
    };
    function onSwipeDeliveryFourthButton() {
      $('.deliverycont').removeClass('first second third fifth sixth seventh').addClass('fourth');
      $('#delivery_location').toggleClass('boxshadow');
      $('#delivery_clear, #delivery_pickup, #delivery_attempt, #d_delivery, #delivery_manifest').removeClass('boxshadow');
    };
    function onSwipeDeliveryFifthButton() {
      $('.deliverycont').removeClass('first second third fourth sixth seventh').addClass('fifth');
    };*/
    function onSwipeDeliverySixthButton() {
      $('.deliverycont').removeClass('first second third fifth fourth seventh').addClass('sixth');
      $('#delivery_manifest').toggleClass('boxshadow');
      $('#delivery_clear, #delivery_pickup, #delivery_attempt, #d_delivery, #delivery_location').removeClass('boxshadow');
    };
    function onSwipeDeliverySeventhButton() {
      $('.deliverycont').removeClass('first second third fifth fourth sixth').addClass('seventh');
      $('#delivery_attempt').toggleClass('boxshadow');
      $('#delivery_clear, #delivery_pickup, #delivery_location, #d_delivery, #delivery_manifest').removeClass('boxshadow');
    };

    function onSwipeDeliveryRightFirstButton() { 
      $('.deliverycont').removeClass('first second third fourth fifth sixth').addClass('seventh');
      $('#delivery_attempt').toggleClass('boxshadow');
      $('#delivery_clear, #delivery_pickup, #delivery_location, #d_delivery, #delivery_manifest').removeClass('boxshadow');
    };
    function onSwipeDeliveryLeftFirstButton() { 
      $('.deliverycont').removeClass('first third fourth fifth sixth seventh').addClass('second');
      $('#delivery_clear').toggleClass('boxshadow');
      $('#delivery_attempt, #delivery_pickup, #delivery_location, #d_delivery, #delivery_manifest').removeClass('boxshadow');
    };
    function onSwipeDeliveryRightSecondButton() { 
      $('.deliverycont').removeClass('second third fourth fifth sixth seventh').addClass('first');
      $('#d_delivery').toggleClass('boxshadow');
      $('#delivery_attempt, #delivery_pickup, #delivery_location, #delivery_clear, #delivery_manifest').removeClass('boxshadow');
    };
    function onSwipeDeliveryLeftSecondButton() { 
      $('.deliverycont').removeClass('first second fourth fifth sixth seventh').addClass('sixth');
      $('#delivery_manifest').toggleClass('boxshadow');
      $('#delivery_attempt, #delivery_clear, #delivery_location, #d_delivery, #delivery_pickup').removeClass('boxshadow');
    };
    /*function onSwipeDeliveryRightThirdButton() { 
      $('.deliverycont').removeClass('first third fourth fifth sixth seventh').addClass('second');
      $('#delivery_clear').toggleClass('boxshadow');
      $('#delivery_attempt, #delivery_pickup, #delivery_location, #d_delivery, #delivery_manifest').removeClass('boxshadow');
    };
    function onSwipeDeliveryLeftThirdButton() { 
      $('.deliverycont').removeClass('first second third fifth sixth seventh').addClass('fourth');
      $('#delivery_location').toggleClass('boxshadow');
      $('#delivery_attempt, #delivery_pickup, #delivery_clear, #d_delivery, #delivery_manifest').removeClass('boxshadow');
    };
    function onSwipeDeliveryRightFourthButton() { 
      $('.deliverycont').removeClass('first second fourth fifth seventh').addClass('third');
      $('#delivery_pickup').toggleClass('boxshadow');
      $('#delivery_attempt, #delivery_clear, #delivery_location, #d_delivery, #delivery_manifest').removeClass('boxshadow');
    };
    function onSwipeDeliveryLeftFourthButton() { 
      $('.deliverycont').removeClass('first second third fourth fifth seventh').addClass('sixth');
      $('#delivery_manifest').toggleClass('boxshadow');
      $('#delivery_attempt, #delivery_pickup, #delivery_location, #d_delivery, #delivery_clear').removeClass('boxshadow');
    };*/
    function onSwipeDeliveryRightSixthButton() { 
      $('.deliverycont').removeClass('first second third fourth fifth sixth').addClass('second');
      $('#delivery_clear').toggleClass('boxshadow');
      $('#delivery_attempt, #delivery_pickup, #delivery_location, #d_delivery, #delivery_manifest').removeClass('boxshadow');
    };
    function onSwipeDeliveryLeftSixthButton() { 
      $('.deliverycont').removeClass('first second third fifth sixth seventh').addClass('seventh');
      $('#delivery_attempt').toggleClass('boxshadow');
      $('#delivery_clear, #delivery_pickup, #delivery_location, #d_delivery, #delivery_manifest').removeClass('boxshadow');
    };
    function onSwipeDeliveryRightSeventhButton() { 
      $('.deliverycont').removeClass('first second third fourth fifth seventh').addClass('sixth');
      $('#delivery_manifest').toggleClass('boxshadow');
      $('#delivery_attempt, #delivery_pickup, #delivery_location, #d_delivery, #delivery_clear').removeClass('boxshadow');
    };
    function onSwipeDeliveryLeftSeventhButton() { 
      $('.deliverycont').removeClass('second third fourth fifth sixth seventh').addClass('first');
      $('#d_delivery').toggleClass('boxshadow');
      $('#delivery_attempt, #delivery_pickup, #delivery_location, #delivery_clear, #delivery_manifest').removeClass('boxshadow');
    };

    function deliveryLeft(){
      var classNames = $('.deliverycont').attr('class').split(" ");
      var className = classNames[2];
      switch (className) {
        case 'first':               
        $('.deliverycont.'+className).removeClass(className).addClass('second');
        $('#delivery_clear').toggleClass('boxshadow');
        $('#delivery_attempt, #delivery_pickup, #delivery_location, #d_delivery').removeClass('boxshadow');
        break;
        case 'second':
        $('.deliverycont.'+className).removeClass(className).addClass('sixth');
        $('#delivery_manifest').toggleClass('boxshadow');
        $('#delivery_attempt, #delivery_clear, #delivery_location, #d_delivery').removeClass('boxshadow');
        break;
       /* case 'third':
        $('.deliverycont.'+className).removeClass(className).addClass('fourth');
        $('#delivery_location').toggleClass('boxshadow');
        $('#delivery_clear, #delivery_pickup, #delivery_attempt, #d_delivery').removeClass('boxshadow');
        break;
        case 'fourth':
        $('.deliverycont.'+className).removeClass(className).addClass('sixth');
        $('#delivery_manifest').toggleClass('boxshadow');
        $('#delivery_clear, #delivery_pickup, #delivery_attempt, #d_delivery, #delivery_location').removeClass('boxshadow');
        break;*/
        case 'sixth': 
        $('.deliverycont.'+className).removeClass(className).addClass('seventh');
        $('#delivery_attempt').toggleClass('boxshadow');
        $('#delivery_clear, #delivery_pickup, #delivery_location, #d_delivery').removeClass('boxshadow');
        break;
        case 'seventh': 
        $('.deliverycont.'+className).removeClass(className).addClass('first');
        $('#d_delivery').toggleClass('boxshadow');
        $('#delivery_attempt, #delivery_clear, #delivery_pickup, #delivery_location').removeClass('boxshadow');
        break;
      }
    }
    function deliveryRight(){
      var classNames = $('.deliverycont').attr('class').split(" ");
      var className = classNames[2];
      switch (className) {
        case 'first':               
        $('.deliverycont.'+className).removeClass(className).addClass('seventh');
        $('#delivery_attempt').toggleClass('boxshadow');
        $('#delivery_clear, #delivery_pickup, #delivery_location, #d_delivery').removeClass('boxshadow');
        break;
        case 'second':
        $('.deliverycont.'+className).removeClass(className).addClass('first');
        $('#d_delivery').toggleClass('boxshadow');
        $('#delivery_attempt, #delivery_clear, #delivery_pickup, #delivery_location').removeClass('boxshadow');
        break;
        /*case 'third':
        $('.deliverycont.'+className).removeClass(className).addClass('second');
        $('#delivery_clear').toggleClass('boxshadow');
        $('#delivery_attempt, #delivery_pickup, #delivery_location, #d_delivery').removeClass('boxshadow');
        break;
        case 'fourth':
        $('.deliverycont.'+className).removeClass(className).addClass('third');
        $('#delivery_pickup').toggleClass('boxshadow');
        $('#delivery_attempt, #delivery_clear, #delivery_location, #d_delivery').removeClass('boxshadow');
        break;*/
        case 'sixth': 
        $('.deliverycont.'+className).removeClass(className).addClass('second');
        $('#delivery_clear').toggleClass('boxshadow');
        $('#delivery_location, #delivery_pickup, #delivery_attempt, #d_delivery').removeClass('boxshadow');
        break;
        case 'seventh': 
        $('.deliverycont.'+className).removeClass(className).addClass('sixth');
        $('#delivery_manifest').toggleClass('boxshadow');
        $('#delivery_clear, #delivery_pickup, #delivery_attempt, #d_delivery, #delivery_location').removeClass('boxshadow');
        break;
      }
    }

    function delivery_attempt(){
      $('#delivery_attempt').addClass('boxshadow');
      $('#delivery_clear, #delivery_pickup, #delivery_location, #d_delivery').removeClass('boxshadow');
    }
    function delivery_clear(){
      $('#delivery_clear').addClass('boxshadow');
      $('#delivery_attempt, #delivery_pickup, #delivery_location, #d_delivery').removeClass('boxshadow');
      _deliveryScanItems =  scanFunctionService.clearScan(_deliveryScanItems,vm.clearScanItem);
      _loadScanItems =  scanFunctionService.clearScan(_loadScanItems,vm.clearScanItem);
      _attemptScanItems = scanFunctionService.clearScan(_attemptScanItems,vm.clearScanItem);
      _pickupScanItems = scanFunctionService.clearScan(_pickupScanItems,vm.clearScanItem);
      _locationScanItems = scanFunctionService.clearScan(_locationScanItems,vm.clearScanItem);
      if(vm.scannedItemsLength > 0){
        vm.scannedItemsLength = vm.scannedItemsLength-1;
        vm.scannedItems.splice(-1,1);
        if(totalBarcodesCount != vm.totalBarcodesCountToScan)
          vm.totalBarcodesCountToScan = vm.totalBarcodesCountToScan - 1;
      }
      vm.scannedBarcode.splice(-1,1);
      if($rootScope.BarcodesBelongsToStop.indexOf(vm.lastScanItem) != -1){
        $rootScope.BarcodesBelongsToStop.splice(-1,1);
      }
      if(vm.scannedBarcode.length > 0){
        vm.clearScanItem = vm.scannedBarcode.slice(-1)[0].barcode;
        vm.lastScanItem = vm.scannedBarcode.slice(-1)[0].barcode;

      }else{
        vm.lastScanItem = [];
        vm.clearScanItem = [];
      }
      if($scope.previousURL == 'app.scan' && vm.scannedBarcode.length == 0){
        vm.totalBarcodesCountToScan = 0;
        currentStopIdentifier = "";
      }
    }
    function delivery_pickup(){
      $('#delivery_pickup').addClass('boxshadow');
      $('#delivery_attempt, #delivery_clear, #delivery_location, #d_delivery, #delivery_manifest').removeClass('boxshadow');
    }
    function delivery_location(){
      $('#delivery_location').addClass('boxshadow');
      $('#delivery_clear, #delivery_pickup, #delivery_attempt, #d_delivery, #delivery_manifest').removeClass('boxshadow');
    }

    //attemptScan functionality
    function attemptScanAction(){
      if(angular.element('#attemptAction').hasClass('first') && angular.element('#a_attempt').hasClass('boxshadow')){
        $('#a_attempt').addClass('boxshadow');
        $('#attempt_clear, #attempt_location, #attempt_pickup, #attempt_manifest').removeClass('boxshadow');
      }else{
        scanService.hideScanner();
        if(vm.scannedItems.length == '0'){
          notificationService.alert('','','You have not scanned a barcode.',function(){
            $ionicHistory.nextViewOptions({
               disableBack: true
             });
            var previousURLParams = angular.fromJson(window.localStorage["previousURL"]);
            if($scope.previousURL == 'app.scan.stop'){
              if($window.localStorage["previousURL"] == "{}"){
                $state.go($scope.prevURL);
              }else{
                $state.go($scope.prevURL, { 'stopIdentifier': previousURLParams.stopIdentifier});
              }
            }else{
              if($window.localStorage["previousURL"] == "{}"){
                $state.go($scope.previousURL);
              }else{
                $state.go($scope.previousURL, { 'stopIdentifier': previousURLParams.stopIdentifier});
              }
            }
          })
        }else{
          if(_unassignPickupScanItems.length > 0){
            if(_pickupScanItems.length > 0){
              var deferred = $q.defer();
              var promises = [];
              promises.push(UnassignPickUpBarcodeLoading());
              $q.all(promises).then(function (data) {
                //_unassignPickupScanItems = [];
                saveRootScopes();
                attemptProcess();
              })
            }else{
              saveRootScopes();
              attemptProcess();
            }
          }else{
            attemptProcess();
          }
        }
      }
    }

    function onSwipeAttemptFirstButton() { 
      $('.attemptcont,.attemptLeft,.attemptRight').removeClass('second third fourth fifth sixth seventh').addClass('first');
      $('#a_attempt').toggleClass('boxshadow');
      $('#attempt_clear, #attempt_location, #attempt_pickup, #attempt_manifest').removeClass('boxshadow');
    };
    function onSwipeAttemptSecondButton() {
      $('.attemptcont').removeClass('first third fourth fifth sixth seventh').addClass('second');
      $('#attempt_clear').toggleClass('boxshadow');
      $('#a_attempt, #attempt_location, #attempt_pickup, #attempt_manifest').removeClass('boxshadow');
    };
    /*function onSwipeAttemptThirdButton() {
      $('.attemptcont').removeClass('first second fourth fifth sixth seventh').addClass('third');
      $('#attempt_location').toggleClass('boxshadow');
      $('#a_attempt, #attempt_clear, #attempt_pickup, #attempt_manifest').removeClass('boxshadow');
    };
    function onSwipeAttemptFourthButton() {
      $('.attemptcont').removeClass('first second third fifth sixth seventh').addClass('fourth');
    };
    function onSwipeAttemptFifthButton() {
      $('.attemptcont').removeClass('first second third fourth sixth seventh').addClass('fifth');
    };
    function onSwipeAttemptSixthButton() {
      $('.attemptcont').removeClass('first second third fifth fourth seventh').addClass('sixth');
      $('#attempt_manifest').toggleClass('boxshadow');
      $('#a_attempt, #attempt_clear, #attempt_location, #attempt_pickup').removeClass('boxshadow');
    };*/
    function onSwipeAttemptSeventhButton() {
      $('.attemptcont').removeClass('first second third fifth fourth sixth').addClass('seventh');
      $('#attempt_manifest').toggleClass('boxshadow');
      $('#a_attempt, #attempt_clear, #attempt_location, #attempt_pickup').removeClass('boxshadow');
    };

    function onSwipeAttemptRightFirstButton() { 
      $('.attemptcont').removeClass('first second third fifth fourth sixth').addClass('seventh');
      $('#attempt_manifest').toggleClass('boxshadow');
      $('#a_attempt, #attempt_clear, #attempt_location, #attempt_pickup').removeClass('boxshadow');
    };
    function onSwipeAttemptLeftFirstButton() { 
      $('.attemptcont').removeClass('first third fifth fourth sixth seventh').addClass('second');
      $('#attempt_clear').toggleClass('boxshadow');
      $('#a_attempt, #attempt_pickup, #attempt_location, #attempt_manifest').removeClass('boxshadow');
    };
    function onSwipeAttemptRightSecondButton() { 
      $('.attemptcont').removeClass('second third fifth fourth sixth seventh').addClass('first');
      $('#a_attempt').toggleClass('boxshadow');
      $('#attempt_pickup, #attempt_clear, #attempt_location, #attempt_manifest').removeClass('boxshadow');
    };
    function onSwipeAttemptLeftSecondButton() { 
      $('.attemptcont').removeClass('first second fifth fourth sixth seventh').addClass('seventh');
      $('#attempt_manifest').toggleClass('boxshadow');
      $('#a_attempt, #attempt_clear, #attempt_pickup, #attempt_location').removeClass('boxshadow');
    };
    /*function onSwipeAttemptRightThirdButton() { 
      $('.attemptcont').removeClass('first third fifth fourth sixth seventh').addClass('second');
      $('#attempt_clear').toggleClass('boxshadow');
      $('#a_attempt, #attempt_pickup, #attempt_location, #attempt_manifest').removeClass('boxshadow');
    };
    function onSwipeAttemptLeftThirdButton() { 
      $('.attemptcont').removeClass('first second third fifth fourth seventh').addClass('sixth');
      $('#attempt_manifest').toggleClass('boxshadow');
      $('#a_attempt, #attempt_clear, #attempt_location, #attempt_pickup').removeClass('boxshadow');
    };
    function onSwipeAttemptRightSixthButton() { 
      $('.attemptcont').removeClass('first second fifth fourth sixth seventh').addClass('third');
      $('#attempt_location').toggleClass('boxshadow');
      $('#a_attempt, #attempt_clear, #attempt_pickup, #attempt_manifest').removeClass('boxshadow');
    };
    function onSwipeAttemptLeftSixthButton() { 
      $('.attemptcont').removeClass('first second third fifth fourth sixth').addClass('seventh');
      $('#attempt_pickup').toggleClass('boxshadow');
      $('#a_attempt, #attempt_clear, #attempt_location, #attempt_manifest').removeClass('boxshadow');
    };*/
    function onSwipeAttemptRightSeventhButton() { 
      $('.attemptcont').removeClass('first second third fifth fourth seventh').addClass('second');
      $('#attempt_clear').toggleClass('boxshadow');
      $('#a_attempt, #attempt_manifest, #attempt_location, #attempt_pickup').removeClass('boxshadow');
    };
    function onSwipeAttemptLeftSeventhButton() { 
      $('.attemptcont').removeClass('second third fifth fourth sixth seventh').addClass('first');
      $('#a_attempt').toggleClass('boxshadow');
      $('#attempt_pickup, #attempt_clear, #attempt_location, #attempt_manifest').removeClass('boxshadow');
    };

    function attemptLeft(){
      var classNames = $('.attemptcont').attr('class').split(" ");
      var className = classNames[2];
      switch (className) {
        case 'first':               
        $('.attemptcont.'+className).removeClass(className).addClass('second');
        $('#attempt_clear').toggleClass('boxshadow');
        $('#a_attempt, #attempt_location, #attempt_pickup').removeClass('boxshadow');
        break;
        case 'second':
        $('.attemptcont.'+className).removeClass(className).addClass('seventh');
        $('#attempt_manifest').toggleClass('boxshadow');
        $('#a_attempt, #attempt_clear, #attempt_pickup').removeClass('boxshadow');
        break;
        /*case 'third':
        $('.attemptcont.'+className).removeClass(className).addClass('sixth');
        $('#attempt_manifest').toggleClass('boxshadow');
        $('#a_attempt, #attempt_clear, #attempt_location, #attempt_pickup').removeClass('boxshadow');
        break;
        case 'sixth': 
        $('.attemptcont.'+className).removeClass(className).addClass('seventh');
        $('#attempt_pickup').toggleClass('boxshadow');
        $('#a_attempt, #attempt_clear, #attempt_location').removeClass('boxshadow');
        break;*/
        case 'seventh': 
        $('.attemptcont.'+className).removeClass(className).addClass('first');
        $('#a_attempt').toggleClass('boxshadow');
        $('#attempt_clear, #attempt_location, #attempt_pickup').removeClass('boxshadow');
        break;
      }
    }
    function attemptRight(){
      var classNames = $('.attemptcont').attr('class').split(" ");
      var className = classNames[2];
      switch (className) {
        case 'first':               
        $('.attemptcont.'+className).removeClass(className).addClass('seventh');
        $('#attempt_manifest').toggleClass('boxshadow');
        $('#a_attempt, #attempt_clear, #attempt_location').removeClass('boxshadow');
        break;
        case 'second':
        $('.attemptcont.'+className).removeClass(className).addClass('first');
        $('#a_attempt').toggleClass('boxshadow');
        $('#attempt_clear, #attempt_location, #attempt_pickup').removeClass('boxshadow');
        break;
        /*case 'third':
        $('.attemptcont.'+className).removeClass(className).addClass('second');
        $('#attempt_clear').toggleClass('boxshadow');
        $('#a_attempt, #attempt_location, #attempt_pickup').removeClass('boxshadow');
        break;
        case 'sixth': 
        $('.attemptcont.'+className).removeClass(className).addClass('third');
        $('#attempt_location').toggleClass('boxshadow');
        $('#a_attempt, #attempt_clear, #attempt_pickup').removeClass('boxshadow');
        break;*/
        case 'seventh': 
        $('.attemptcont.'+className).removeClass(className).addClass('second');
        $('#attempt_clear').toggleClass('boxshadow');
        $('#a_attempt, #attempt_manifest, #attempt_location, #attempt_pickup').removeClass('boxshadow');
        break;
      }
    }
    function attempt_clear(){
      $('#attempt_clear').addClass('boxshadow');
      $('#a_attempt, #attempt_location, #attempt_pickup, #attempt_manifest').removeClass('boxshadow');
      _attemptScanItems = scanFunctionService.clearScan(_attemptScanItems,vm.clearScanItem);
      _pickupScanItems = scanFunctionService.clearScan(_pickupScanItems,vm.clearScanItem);
      _locationScanItems = scanFunctionService.clearScan(_locationScanItems,vm.clearScanItem);
      if(vm.scannedItemsLength > 0){
        vm.scannedItemsLength = vm.scannedItemsLength-1;
        vm.totalBarcodesCountToScan = vm.totalBarcodesCountToScan - 1;
        vm.scannedItems.splice(-1,1);
      }
      vm.scannedBarcode.splice(-1,1);
      if($rootScope.BarcodesBelongsToStop.indexOf(vm.lastScanItem) != -1){
        $rootScope.BarcodesBelongsToStop.splice(-1,1);
      }
      if(vm.scannedBarcode.length > 0){
        vm.clearScanItem = vm.scannedBarcode.slice(-1)[0].barcode;
        vm.lastScanItem = vm.scannedBarcode.slice(-1)[0].barcode;

      }else{
        vm.lastScanItem = [];
        vm.clearScanItem = [];
      }
      if($scope.previousURL == 'app.scan' && vm.scannedBarcode.length == 0){
        vm.totalBarcodesCountToScan = 0;
        currentStopIdentifier = "";
      }
    }
    function attempt_pickup(){
      $('#attempt_pickup').addClass('boxshadow');
      $('#a_attempt, #attempt_location, #attempt_clear, #attempt_manifest').removeClass('boxshadow');
    }
    function attempt_location(){
      $('#attempt_location').addClass('boxshadow');
      $('#a_attempt, #attempt_pickup, #attempt_clear, #attempt_manifest').removeClass('boxshadow');
    }


    //locationScan functionality
    function locationScanAction(){
      if(angular.element('#locationAction').hasClass('first') && angular.element('#lc_location').hasClass('boxshadow')){
        $('#lc_location').addClass('boxshadow');
        $('#location_clear, #location_pickup, #location_load, #location_attempt, #location_delivery,#loaction_manifest').removeClass('boxshadow');
      }else{
        scanService.hideScanner();
        if(_unassignPickupScanItems.length > 0){
          if(_pickupScanItems.length > 0){
            var deferred = $q.defer();
            var promises = [];
            promises.push(UnassignPickUpBarcodeLoading());
            $q.all(promises).then(function (data) {
              saveRootScopes();
              locationProcess();
            })
          }else{
            saveRootScopes();
            locationProcess();
          }
        }else{
          locationProcess();
        }
      }
    }
    function onSwipeLocationFirstButton() { 
      $('.locationcont,.locationLeft,.locationRight').removeClass('second third fourth fifth sixth seventh').addClass('first');
      $('#lc_location').toggleClass('boxshadow');
      $('#location_clear, #location_pickup, #location_load, #location_attempt, #location_delivery, #loaction_manifest').removeClass('boxshadow');
    };
    function onSwipeLocationSecondButton() {
      $('.locationcont').removeClass('first third fourth fifth sixth seventh').addClass('second');
      $('#location_clear').toggleClass('boxshadow');
      $('#lc_location, #location_pickup, #location_load, #location_attempt, #location_delivery, #loaction_manifest').removeClass('boxshadow');
    };
    function onSwipeLocationThirdButton() {
      $('.locationcont').removeClass('first second fourth fifth sixth seventh').addClass('third');
      $('#location_pickup').toggleClass('boxshadow');
      $('#lc_location, #location_clear, #location_load, #location_attempt, #location_delivery, #loaction_manifest').removeClass('boxshadow');
    };
    function onSwipeLocationFourthButton() {
      $('.locationcont').removeClass('first second third fifth sixth seventh').addClass('fourth'); 
      $('#loaction_manifest').toggleClass('boxshadow');
      $('#lc_location, #location_pickup, #location_clear, #location_attempt, #location_delivery, #location_load').removeClass('boxshadow'); 
    };
    function onSwipeLocationFifthButton() {
      $('.locationcont').removeClass('first second third fourth sixth seventh').addClass('fifth');
      $('#location_load').toggleClass('boxshadow');
      $('#lc_location, #location_pickup, #location_clear, #location_attempt, #location_delivery, #loaction_manifest').removeClass('boxshadow');
    };
    function onSwipeLocationSixthButton() {
      $('.locationcont').removeClass('first second third fifth fourth seventh').addClass('sixth');
      $('#location_attempt').toggleClass('boxshadow');
      $('#lc_location, #location_pickup, #location_load, #location_clear, #location_delivery, #loaction_manifest').removeClass('boxshadow');
    };
    function onSwipeLocationSeventhButton() {
      $('.locationcont').removeClass('first second third fifth fourth sixth').addClass('seventh');
      $('#location_delivery').toggleClass('boxshadow');
      $('#lc_location, #location_pickup, #location_load, #location_attempt, #location_clear, #loaction_manifest').removeClass('boxshadow');
    };

    function onSwipeLocationRightFirstButton() { 
      $('.locationcont').removeClass('first second third fifth fourth sixth').addClass('seventh');
      $('#location_delivery').toggleClass('boxshadow');
      $('#lc_location, #location_pickup, #location_load, #location_attempt, #location_clear, #loaction_manifest').removeClass('boxshadow');
    };
    function onSwipeLocationLeftFirstButton() { 
      $('.locationcont').removeClass('first third fifth fourth sixth seventh').addClass('second');
      $('#location_clear').toggleClass('boxshadow');
      $('#lc_location, #location_pickup, #location_load, #location_attempt, #location_delivery, #loaction_manifest').removeClass('boxshadow');
    };
    function onSwipeLocationRightSecondButton() { 
      $('.locationcont').removeClass('second third fifth fourth sixth seventh').addClass('first');
      $('#lc_location').toggleClass('boxshadow');
      $('#location_delivery, #location_pickup, #location_load, #location_attempt, #location_clear, #loaction_manifest').removeClass('boxshadow');
    };
    function onSwipeLocationLeftSecondButton() { 
      $('.locationcont').removeClass('first second fifth fourth sixth seventh').addClass('third');
      $('#location_pickup').toggleClass('boxshadow');
      $('#lc_location, #location_delivery, #location_load, #location_attempt, #location_clear, #loaction_manifest').removeClass('boxshadow');
    };
    function onSwipeLocationRightThirdButton() { 
      $('.locationcont').removeClass('first third fifth fourth sixth seventh').addClass('second');
      $('#location_clear').toggleClass('boxshadow');
      $('#lc_location, #location_pickup, #location_load, #location_attempt, #location_delivery, #loaction_manifest').removeClass('boxshadow');
    };
    function onSwipeLocationLeftThirdButton() { 
      $('.locationcont').removeClass('first second third fifth sixth seventh').addClass('fourth');
      $('#loaction_manifest').toggleClass('boxshadow');
      $('#lc_location, #location_pickup, #location_load, #location_attempt, #location_clear, #location_delivery').removeClass('boxshadow');
    };
    function onSwipeLocationRightFourthButton() { 
      $('.locationcont').removeClass('first second fifth fourth sixth seventh').addClass('third');
      $('#location_pickup').toggleClass('boxshadow');
      $('#lc_location, #location_delivery, #location_load, #location_attempt, #location_clear, #loaction_manifest').removeClass('boxshadow');
    };
    function onSwipeLocationLeftFourthButton() { 
      $('.locationcont').removeClass('first second third fourth sixth seventh').addClass('fifth');
      $('#location_load').toggleClass('boxshadow');
      $('#lc_location, #location_pickup, #location_delivery, #location_attempt, #location_clear, #loaction_manifest').removeClass('boxshadow');
    };
    function onSwipeLocationRightFifthButton() { 
      $('.locationcont').removeClass('first second third fifth sixth seventh').addClass('fourth');
      $('#loaction_manifest').toggleClass('boxshadow');
      $('#lc_location, #location_pickup, #location_load, #location_attempt, #location_clear, #location_delivery').removeClass('boxshadow');
    };
    function onSwipeLocationLeftFifthButton() { 
      $('.locationcont').removeClass('first second third fifth fourth seventh').addClass('sixth');
      $('#location_attempt').toggleClass('boxshadow');
      $('#lc_location, #location_pickup, #location_load, #location_delivery, #location_clear, #loaction_manifest').removeClass('boxshadow');
    };
    function onSwipeLocationRightSixthButton() { 
      $('.locationcont').removeClass('first second third fourth sixth seventh').addClass('fifth');
      $('#location_load').toggleClass('boxshadow');
      $('#lc_location, #location_pickup, #location_delivery, #location_attempt, #location_clear, #loaction_manifest').removeClass('boxshadow');
    };
    function onSwipeLocationLeftSixthButton() { 
      $('.locationcont').removeClass('first second third fifth fourth sixth').addClass('seventh');
      $('#location_delivery').toggleClass('boxshadow');
      $('#lc_location, #location_pickup, #location_load, #location_attempt, #location_clear, #loaction_manifest').removeClass('boxshadow');
    };
    function onSwipeLocationRightSeventhButton() { 
      $('.locationcont').removeClass('first second third fifth fourth seventh').addClass('sixth');
      $('#location_attempt').toggleClass('boxshadow');
      $('#lc_location, #location_pickup, #location_load, #location_delivery, #location_clear, #loaction_manifest').removeClass('boxshadow');
    };
    function onSwipeLocationLeftSeventhButton() { 
      $('.locationcont').removeClass('second third fifth fourth sixth seventh').addClass('first');
      $('#lc_location').toggleClass('boxshadow');
      $('#location_delivery, #location_pickup, #location_load, #location_attempt, #location_clear, #loaction_manifest').removeClass('boxshadow');
    };

    function locationLeft(){
      var classNames = $('.locationcont').attr('class').split(" ");
      var className = classNames[2];
      switch (className) {
        case 'first':               
        $('.locationcont.'+className).removeClass(className).addClass('second');
        $('#location_clear').toggleClass('boxshadow');
        $('#lc_location, #location_pickup, #location_load, #location_attempt, #location_delivery').removeClass('boxshadow');
        break;
        case 'second':
        $('.locationcont.'+className).removeClass(className).addClass('third');
        $('#location_pickup').toggleClass('boxshadow');
        $('#lc_location, #location_clear, #location_load, #location_attempt, #location_delivery').removeClass('boxshadow');
        break;
        case 'third':
        $('.locationcont.'+className).removeClass(className).addClass('fourth');
        $('#loaction_manifest').toggleClass('boxshadow');
        $('#lc_location, #location_pickup, #location_clear, #location_attempt, #location_delivery, #location_load').removeClass('boxshadow'); 
        break;
        case 'fourth':
        $('.locationcont.'+className).removeClass(className).addClass('fifth');
        $('#location_load').toggleClass('boxshadow');
        $('#lc_location, #location_pickup, #location_clear, #location_attempt, #location_delivery').removeClass('boxshadow');
        break;
        case 'fifth':
        $('.locationcont.'+className).removeClass(className).addClass('sixth');
        $('#location_attempt').toggleClass('boxshadow');
        $('#lc_location, #location_pickup, #location_load, #location_clear, #location_delivery').removeClass('boxshadow');
        break;
        case 'sixth': 
        $('.locationcont.'+className).removeClass(className).addClass('seventh');
        $('#location_delivery').toggleClass('boxshadow');
        $('#lc_location, #location_pickup, #location_load, #location_attempt, #location_clear').removeClass('boxshadow');
        break;
        case 'seventh': 
        $('.locationcont.'+className).removeClass(className).addClass('first');
        $('#lc_location').toggleClass('boxshadow');
        $('#location_clear, #location_pickup, #location_load, #location_attempt, #location_delivery').removeClass('boxshadow');
        break;
      }
    }
    function locationRight(){
      var classNames = $('.locationcont').attr('class').split(" ");
      var className = classNames[2];
      switch (className) {
        case 'first':               
        $('.locationcont.'+className).removeClass(className).addClass('seventh');
        $('#location_delivery').toggleClass('boxshadow');
        $('#lc_location, #location_pickup, #location_load, #location_attempt, #location_clear').removeClass('boxshadow');
        break;
        case 'second':
        $('.locationcont.'+className).removeClass(className).addClass('first');
        $('#lc_location').toggleClass('boxshadow');
        $('#location_clear, #location_pickup, #location_load, #location_attempt, #location_delivery').removeClass('boxshadow');
        break;
        case 'third':
        $('.locationcont.'+className).removeClass(className).addClass('second');
        $('#location_clear').toggleClass('boxshadow');
        $('#lc_location, #location_pickup, #location_load, #location_attempt, #location_delivery').removeClass('boxshadow');
        break;
        case 'fourth':
        $('.locationcont.'+className).removeClass(className).addClass('third');
        $('#location_pickup').toggleClass('boxshadow');
        $('#lc_location, #location_clear, #location_load, #location_attempt, #location_delivery').removeClass('boxshadow');
        break;
        case 'fifth':
        $('.locationcont.'+className).removeClass(className).addClass('fourth');
        $('#loaction_manifest').toggleClass('boxshadow');
        $('#lc_location, #location_pickup, #location_clear, #location_attempt, #location_delivery, #location_load').removeClass('boxshadow'); 
        break;
        case 'sixth': 
        $('.locationcont.'+className).removeClass(className).addClass('fifth');
        $('#location_load').toggleClass('boxshadow');
        $('#lc_location, #location_pickup, #location_clear, #location_attempt, #location_delivery').removeClass('boxshadow');
        break;
        case 'seventh': 
        $('.locationcont.'+className).removeClass(className).addClass('sixth');
        $('#location_attempt').toggleClass('boxshadow');
        $('#lc_location, #location_pickup, #location_load, #location_clear, #location_delivery').removeClass('boxshadow');
        break;
      }
    }

    function location_clear(){
      $('#location_clear').addClass('boxshadow');
      $('#lc_location, #location_pickup, #location_load, #location_attempt, #location_delivery, #loaction_manifest').removeClass('boxshadow');
      _deliveryScanItems =  scanFunctionService.clearScan(_deliveryScanItems,vm.clearScanItem);
      _loadScanItems =  scanFunctionService.clearScan(_loadScanItems,vm.clearScanItem);
      _attemptScanItems = scanFunctionService.clearScan(_attemptScanItems,vm.clearScanItem);
      _pickupScanItems = scanFunctionService.clearScan(_pickupScanItems,vm.clearScanItem);
      _locationScanItems = scanFunctionService.clearScan(_locationScanItems,vm.clearScanItem);
      _unassignPickupScanItems = scanFunctionService.clearScan(_unassignPickupScanItems,vm.lastScanItem);
      $rootScope.addUnassignpickupItems = scanFunctionService.clearScan($rootScope.addUnassignpickupItems,vm.lastScanItem);
      if(vm.scannedItemsLength > 0){
        vm.scannedItemsLength = vm.scannedItemsLength-1;
        vm.totalBarcodesCountToScan = vm.totalBarcodesCountToScan - 1;
        vm.scannedItems.splice(-1,1);
      }
      vm.scannedBarcode.splice(-1,1);
      if(vm.scannedBarcode.length > 0){
        vm.clearScanItem = vm.scannedBarcode.slice(-1)[0].barcode;
        vm.lastScanItem = vm.scannedBarcode.slice(-1)[0].barcode;

      }else{
        vm.lastScanItem = [];
        vm.clearScanItem = [];
      }
   }
   function location_pickup(){
     $('#location_pickup').addClass('boxshadow');
     $('#lc_location, #location_clear, #location_load, #location_attempt, #location_delivery, #loaction_manifest').removeClass('boxshadow');
   }
   function location_load(){
     $('#location_load').addClass('boxshadow');
     $('#lc_location, #location_clear, #location_pickup, #location_attempt, #location_delivery, #loaction_manifest').removeClass('boxshadow');
   }
   function location_attempt(){
     $('#location_attempt').addClass('boxshadow');
     $('#lc_location, #location_clear, #location_pickup, #location_load, #location_delivery, #loaction_manifest').removeClass('boxshadow');
   }
   function location_delivery(){
     $('#location_delivery').addClass('boxshadow');
     $('#lc_location, #location_clear, #location_pickup, #location_attempt, #location_load, #loaction_manifest').removeClass('boxshadow');
   }
    //pickupScan functionality
    function pickupScanAction() {
      if(angular.element('#pickupAction').hasClass('first') && angular.element('#p_pickup').hasClass('boxshadow')){
        $('#p_pickup').addClass('boxshadow');
        $('#pickup_clear, #pickup_location, #pickup_manifest').removeClass('boxshadow');
      }else{
        scanService.hideScanner();
        if(_pickupScanItems.length == 0 && _unassignPickupScanItems.length == 0 && $rootScope.addUnassignpickupItems.length == 0){
          notificationService.alert('','','You have not scanned a barcode.',function(){
            if($rootScope.flag == 'locationpickup'){
             eventsService.locationScanBarcodesEventUpdate(_locationScanItems,'END').then(function(data){
                  _locationScanItems = [];
                  $rootScope.flag = "";
                  locationScan();
                  eventsService.sendEvents();
                  scanFunctionService.clearRootScope();
                  rootScopeService.clearLocalStorage();
                $ionicHistory.nextViewOptions({
                   disableBack: true
                  });
                  var previousURLParams = angular.fromJson(window.localStorage["previousURL"]);
                  if($window.localStorage["previousURL"] == "{}"){
                    $state.go($scope.previousURL);
                  }else{
                    $state.go($scope.previousURL, { 'stopIdentifier': previousURLParams.stopIdentifier, 'type': previousURLParams.type});
                  }
             },function(err){
                  $log.info("Error in adding location event",err);
             })
            }else{
              $ionicHistory.nextViewOptions({
              disableBack: true
              });
              if($scope.previousURL == 'app.pickupDetail' || $scope.previousURL == 'app.unassignPickupDetail'){
                if($scope.prePreviousURL == 'app.detail'){
                  $state.go($rootScope.prePreviousURL, { 'stopIdentifier': currentStopIdentifier, 'type': 'pickup'});
                }else{
                  $state.go($rootScope.prePreviousURL);
                }
              }else{
                var previousURLParams = angular.fromJson(window.localStorage["previousURL"]);
                if($scope.previousURL == 'app.scan.stop'){
                  if($window.localStorage["previousURL"] == "{}"){
                    $state.go($scope.prevURL);
                  }else{
                    $state.go($scope.prevURL, { 'stopIdentifier': previousURLParams.stopIdentifier, 'type': previousURLParams.type});
                  }
                }else{
                  if($window.localStorage["previousURL"] == "{}"){
                    $state.go($scope.previousURL);
                  }else{
                    $state.go($scope.previousURL, { 'stopIdentifier': previousURLParams.stopIdentifier, 'type': previousURLParams.type});
                  }
                }
              }
            }
            
          })
        }else{
          $ionicHistory.nextViewOptions({
            disableBack: true
          });
          if(_pickupScanItems.length > 0){
            var deferred = $q.defer();
            var promises = [];
            promises.push(UnassignPickUpBarcodeLoading());
            $q.all(promises).then(function (data) {
              saveRootScopes();
              $state.go('app.pickupDetail');
            })
          }else{
            saveRootScopes();
            $state.go('app.unassignPickupDetail');
          }        
        }
      }
    }
    function onSwipePickupFirstButton() { 
      $('.pickupcont,.pickupLeft,.pickupRight').removeClass('second third fourth fifth sixth seventh').addClass('first');
      $('#p_pickup').toggleClass('boxshadow');
      $('#pickup_clear, #pickup_manifest').removeClass('boxshadow');
    };
    function onSwipePickupSecondButton() {
      $('.pickupcont').removeClass('first third fourth fifth sixth seventh').addClass('second');
      $('#pickup_clear').toggleClass('boxshadow');
      $('#p_pickup, #pickup_manifest').removeClass('boxshadow');
    };
    function onSwipePickupSeventhButton() {
      $('.pickupcont').removeClass('first second third fifth fourth sixth').addClass('seventh');
      $('#pickup_manifest').toggleClass('boxshadow');
      $('#pickup_clear, #p_pickup').removeClass('boxshadow');
    };

    function onSwipePickupRightFirstButton() { 
      $('.pickupcont').removeClass('first second third fifth fourth sixth').addClass('seventh');
      $('#pickup_manifest').toggleClass('boxshadow');
      $('#pickup_clear, #p_pickup').removeClass('boxshadow');
    };
    function onSwipePickupLeftFirstButton() { 
      $('.pickupcont').removeClass('first third fifth fourth sixth seventh').addClass('second');
      $('#pickup_clear').toggleClass('boxshadow');
      $('#pickup_manifest, #p_pickup').removeClass('boxshadow');
    };
    function onSwipePickupRightSecondButton() {
      $('.pickupcont').removeClass('second third fifth fourth sixth seventh').addClass('first');
      $('#p_pickup').toggleClass('boxshadow');
      $('#pickup_clear, #pickup_manifest').removeClass('boxshadow'); 
    };
    function onSwipePickupLeftSecondButton() { 
      $('.pickupcont').removeClass('first second fifth fourth sixth seventh').addClass('seventh');
      $('#pickup_manifest').toggleClass('boxshadow');
      $('#pickup_clear, #p_pickup').removeClass('boxshadow');
    };
    function onSwipePickupRightSeventhButton() { 
      $('.pickupcont').removeClass('first second fifth fourth sixth seventh').addClass('second');
      $('#pickup_clear').toggleClass('boxshadow');
      $('#pickup_manifest, #p_pickup').removeClass('boxshadow');
    };
    function onSwipePickupLeftSeventhButton() { 
      $('.pickupcont').removeClass('second third fifth fourth sixth seventh').addClass('first');
      $('#p_pickup').toggleClass('boxshadow');
      $('#pickup_clear, #pickup_manifest').removeClass('boxshadow'); 
    };

    function pickupLeft(){
      var classNames = $('.pickupcont').attr('class').split(" ");
      var className = classNames[2];
      switch (className) {
        case 'first':               
        $('.pickupcont.'+className).removeClass(className).addClass('second');
        $('#pickup_clear').toggleClass('boxshadow');
        $('#p_pickup, #pickup_manifest').removeClass('boxshadow');
        break;
        case 'second':
        $('.pickupcont.'+className).removeClass(className).addClass('seventh');
        $('#pickup_manifest').toggleClass('boxshadow');
        $('#pickup_clear, #p_pickup').removeClass('boxshadow');
        break;
        case 'seventh': 
        $('.pickupcont.'+className).removeClass(className).addClass('first');
        $('#p_pickup').toggleClass('boxshadow');
        $('#pickup_clear, #pickup_manifest').removeClass('boxshadow');
        break;
      }
    }

    function pickupRight(){
      var classNames = $('.pickupcont').attr('class').split(" ");
      var className = classNames[2];
      switch (className) {
        case 'first':               
        $('.pickupcont.'+className).removeClass(className).addClass('seventh');
        $('#pickup_manifest').toggleClass('boxshadow');
        $('#pickup_clear, #p_pickup').removeClass('boxshadow');
        break;
        case 'second':
        $('.pickupcont.'+className).removeClass(className).addClass('first');
        $('#p_pickup').toggleClass('boxshadow');
        $('#pickup_clear, #pickup_manifest').removeClass('boxshadow');
        break;
        case 'seventh': 
        $('.pickupcont.'+className).removeClass(className).addClass('second');
        $('#pickup_clear').toggleClass('boxshadow');
        $('#p_pickup, #pickup_manifest').removeClass('boxshadow');
        break;
      }
    }

    function pickup_clear(){
      $('#pickup_clear').addClass('boxshadow');
      $('#p_pickup, #pickup_manifest').removeClass('boxshadow');
      _pickupScanItems = scanFunctionService.clearScan(_pickupScanItems,vm.clearScanItem);
      _locationScanItems = scanFunctionService.clearScan(_locationScanItems,vm.clearScanItem);
      _unassignPickupScanItems = scanFunctionService.clearScan(_unassignPickupScanItems,vm.lastScanItem);
      $rootScope.addUnassignpickupItems = scanFunctionService.clearScan($rootScope.addUnassignpickupItems,vm.lastScanItem);
      if(vm.scannedItemsLength > 0){
        vm.scannedItemsLength = vm.scannedItemsLength-1;
        vm.scannedItems.splice(-1,1);
        if(totalBarcodesCount != vm.totalBarcodesCountToScan)
          vm.totalBarcodesCountToScan = vm.totalBarcodesCountToScan - 1;
      }
      vm.scannedBarcode.splice(-1,1);
      if(vm.scannedBarcode.length > 0){
        vm.clearScanItem = vm.scannedBarcode.slice(-1)[0].barcode;
        vm.lastScanItem = vm.scannedBarcode.slice(-1)[0].barcode;

      }else{
        vm.lastScanItem = [];
        vm.clearScanItem = [];
      }
    }
    //doorScan functionality
    function doorScanAction() {
      scanService.hideScanner();
      if(_doorScanItems.length == 0){
          vm.doortag = '';
        $ionicPopup.show({
          template: '<input type = "text" ng-model ="vm.doortag" autofocus="autofocus" id="drtgSkb">',
          title: 'Please enter door tag#', 
          scope: $scope,             
          buttons: [
          {
            text: 'Done',
            type: 'button-positive',
            onTap: function(e) {
            $log.info('door tag lists');
            _doorScanItemBarcode = vm.doortag;
            if(_doorScanItemBarcode != ''){
              if($rootScope.deliveryEvent == false){
                packageStatusService.updateAttemptPackageStatus(_attemptScanItems).then(function(data){
                  if(_pickupScanItems.length > 0){
                  saveRootScopes();
                  $ionicHistory.nextViewOptions({
                    disableBack: true
                  });
                  $state.go('app.pickupDetail');
                  }else if(_unassignPickupScanItems.length > 0 || $rootScope.addUnassignpickupItems.length > 0){
                  saveRootScopes();
                  $ionicHistory.nextViewOptions({
                    disableBack: true
                  });
                  $state.go('app.unassignPickupDetail');
                  }else{
                  $ionicHistory.nextViewOptions({
                    disableBack: true
                  });
                  if(_deliveryScanItems.length > 0){
                    _attemptScanItems = [];
                    saveRootScopes();
                    if($window.localStorage["process"] == 'L2T' || $window.localStorage["process"] == 'T2M'){
                      var count = 0;
                      for (var i=0; i<_deliveryScanItems.length; i+=1) {      //chunk delivery events
                        var _chunkDeliveryEvents = _deliveryScanItems.slice(i,i+1);
                        count+=_chunkDeliveryEvents.length;
                        eventsService.deliveredEventUpdate(_chunkDeliveryEvents, "", "", "","","").then(function(data){
                          if(_deliveryScanItems.length == count ){
                           scanFunctionService.clearRootScope();
                            rootScopeService.clearLocalStorage();
                            rootScopeService.clearDeliveryLocalVariables();
                            $ionicHistory.nextViewOptions({
                              disableBack: true
                            });
                            $state.go('app.manifest');
                          }
                        }, function (err) {
                          $log.warn('Error in updating delivered event',err);
                        })
                      }
                    }else{
                      $state.go('app.signature', {'stopIdentifier': currentStopIdentifier});
                    }

                  }else{
                    $state.go('app.manifest');
                  }
                  }
                },function(err){
                  $log.info('Error updateing package status',err);
                });

                if(_doorScanItems.length> 0 && _attemptScanItems.length >0){
                  if(_deliveryScanItems.length > 0){
                  for(var i in _attemptScanItems){
                    for(var j in _deliveryScanItems){
                    if(_attemptScanItems[i].barcode == _deliveryScanItems[j].barcode){
                      _deliveryScanItems.splice(j,1);
                      saveRootScopes();
                    }
                    }
                  }
                  }
                }
                if(_locationScanItems.length>0){
                  var count = 0;
                  for (var i=0; i<_attemptScanItems.length; i+=1) {      //chunk attempt events
                    var _chunkAttemptEvents = _attemptScanItems.slice(i,i+1);
                    count+=_chunkAttemptEvents.length;
                    eventsService.attemptBarcodesEventUpdate(_chunkAttemptEvents,_doorScanItemBarcode,$rootScope.Reasonselected,_locationScanItems[0].barcode).then(function(data){
                      if(_attemptScanItems.length == count ){
                        _attemptScanItems = [];
                        _doorScanItems =[];
                        if(_pickupScanItems.length == 0 && _unassignPickupScanItems.length == 0 && $rootScope.addUnassignpickupItems.length == 0){
                          eventsService.locationScanBarcodesEventUpdate(_locationScanItems,'').then(function(data){
                          if(deviceService.getConnectionStatus()){
                            eventsService.sendEvents();
                          }
                          //scanFunctionService.clearRootScope();
                          }, function (err) {
                          $log.warn('Error location event update',err);
                          })
                        }
                      }
                    },function(err){
                    $log.warn("Error attempt event update",err);
                    });
                  }
                }else{
                  var count = 0;
                  for (var i=0; i<_attemptScanItems.length; i+=1) {      //chunk attempt events
                    var _chunkAttemptEvents = _attemptScanItems.slice(i,i+1);
                    count+=_chunkAttemptEvents.length;
                    eventsService.attemptBarcodesEventUpdate(_chunkAttemptEvents,"",$rootScope.Reasonselected,"").then(function (data) {
                      if(_attemptScanItems.length == count ){
                        if(_pickupScanItems.length == 0 && _unassignPickupScanItems.length == 0 && $rootScope.addUnassignpickupItems.length == 0){
                          if(deviceService.getConnectionStatus()){
                            eventsService.sendEvents();
                          }
                          //scanFunctionService.clearRootScope();
                        }
                      }
                    }, function (err) {
                    $log.info('Error in package updating status',err);
                    })
                  }
                }
              }else{
                 $rootScope.deliveryEvent = false;
                 saveRootScopes();
                 $ionicHistory.nextViewOptions({
                    disableBack: true
                  });
                 var previousURLParams = angular.fromJson(window.localStorage["previousURL"]);
                  if($window.localStorage["previousURL"] == "{}"){
                    $state.go($scope.previousURL);
                  }else{
                    $state.go($scope.previousURL, { 'stopIdentifier': previousURLParams.stopIdentifier});
                  }
              } 
            }else{
              e.preventDefault();
              notificationService.alert('Alert','', "Please enter Barcode", null);
            }
            }
          }
          ]
        });
      }else{
        if($rootScope.deliveryEvent == false){        
          if(_doorScanItems.length> 0 && _attemptScanItems.length >0){
            if(_deliveryScanItems.length > 0){
              for(var i in _attemptScanItems){
                for(var j in _deliveryScanItems){
                  if(_attemptScanItems[i].barcode == _deliveryScanItems[j].barcode){
                    _deliveryScanItems.splice(j,1);
                    saveRootScopes();
                  }
                }
              }
            }
            if(_locationScanItems.length>0){
              var count = 0;
              for (var i=0; i<_attemptScanItems.length; i+=1) {      //chunk attempt events
                var _chunkAttemptEvents = _attemptScanItems.slice(i,i+1);
                count+=_chunkAttemptEvents.length;
                eventsService.attemptBarcodesEventUpdate(_chunkAttemptEvents,_doorScanItems[0].barcode,$rootScope.Reasonselected,_locationScanItems[0].barcode).then(function(data){
                  if(_attemptScanItems.length == count ){
                    _attemptScanItems = [];
                    _doorScanItems =[];
                    if(_pickupScanItems.length == 0 && _unassignPickupScanItems.length == 0 && $rootScope.addUnassignpickupItems.length == 0){
                      eventsService.locationScanBarcodesEventUpdate(_locationScanItems,'').then(function(data){
                      if(deviceService.getConnectionStatus()){
                        eventsService.sendEvents();
                      }
                      //scanFunctionService.clearRootScope();
                      }, function (err) {
                      $log.warn('Error in updating location event',err);
                      })
                    }
                  }
                },function(err){
                $log.warn("Error in updating attempt event",err);
                });
              }
            }else{
              var count = 0;
              for (var i=0; i<_attemptScanItems.length; i+=1) {      //chunk attempt events
                var _chunkAttemptEvents = _attemptScanItems.slice(i,i+1);
                count+=_chunkAttemptEvents.length;
                eventsService.attemptBarcodesEventUpdate(_chunkAttemptEvents,_doorScanItems[0].barcode,$rootScope.Reasonselected,"").then(function(data){
                  if(_attemptScanItems.length == count ){
                    _attemptScanItems = [];
                    _doorScanItems =[];
                    if(_pickupScanItems.length == 0 && _unassignPickupScanItems.length == 0 && $rootScope.addUnassignpickupItems.length == 0){
                      if(deviceService.getConnectionStatus()){
                        eventsService.sendEvents();
                      }
                      //scanFunctionService.clearRootScope();
                    }
                  }
                },function(err){
                $log.warn("Error in updating attempt event",err);
                });
              }
            }

          }
          packageStatusService.updateAttemptPackageStatus(_attemptScanItems).then(function(data){
            saveRootScopes();
            if(_pickupScanItems.length > 0){
              saveRootScopes();
              $ionicHistory.nextViewOptions({
                disableBack: true
              });
              $state.go('app.pickupDetail');
            }else if(_unassignPickupScanItems.length > 0 || $rootScope.addUnassignpickupItems.length > 0){
              saveRootScopes();
              $ionicHistory.nextViewOptions({
                disableBack: true
              });
              $state.go('app.unassignPickupDetail');
            }else{
              $ionicHistory.nextViewOptions({
                disableBack: true
              });
              if(_deliveryScanItems.length > 0){
                _attemptScanItems = [];
                saveRootScopes();
               if($window.localStorage["process"] == 'L2T' || $window.localStorage["process"] == 'T2M'){
                  var count = 0;
                  for (var i=0; i<_deliveryScanItems.length; i+=1) {      //chunk delivery events
                    var _chunkDeliveryEvents = _deliveryScanItems.slice(i,i+1);
                    count+=_chunkDeliveryEvents.length;
                    eventsService.deliveredEventUpdate(_chunkDeliveryEvents, "", "", "","","").then(function(data){
                      if(_deliveryScanItems.length == count ){
                       scanFunctionService.clearRootScope();
                        rootScopeService.clearLocalStorage();
                        rootScopeService.clearDeliveryLocalVariables();
                        $ionicHistory.nextViewOptions({
                          disableBack: true
                        });
                        $state.go('app.manifest');
                      }
                    }, function (err) {
                      $log.warn('Error in updating delivered event',err);
                    })
                  }
                }else{
                  $state.go('app.signature', {'stopIdentifier': currentStopIdentifier});
                }
              }else{
                $state.go('app.manifest');
              }
            }
          },function(err){
            $log.info('Error in updating package status',err);
          });
        }else{
          $rootScope.deliveryEvent = true;
          saveRootScopes();
          $ionicHistory.nextViewOptions({
            disableBack: true
          });
         var previousURLParams = angular.fromJson(window.localStorage["previousURL"]);
          if($window.localStorage["previousURL"] == "{}"){
            $state.go($scope.previousURL);
          }else{
            $state.go($scope.previousURL, { 'stopIdentifier': previousURLParams.stopIdentifier});
          }
        }
      }
    }    
    function onSwipeDoorFirstButton() { 
      $('.doorcont').removeClass('second third fourth fifth sixth seventh').addClass('first');
      $('#doortag').toggleClass('boxshadow');
      $('#door_clear, #door_manifest').removeClass('boxshadow');

    };
    function onSwipeDoorSecondButton() {
      $('.doorcont').removeClass('first third fourth fifth sixth seventh').addClass('second');
      $('#door_clear').toggleClass('boxshadow');
      $('#doortag, #door_manifest').removeClass('boxshadow');
    };
    function onSwipeDoorSeventhButton() {
      $('.doorcont').removeClass('first second third fifth fourth sixth').addClass('seventh');
      $('#door_manifest').toggleClass('boxshadow');
      $('#door_clear, #doortag').removeClass('boxshadow');
    };  

    function door_clear(){
      $('#door_clear').addClass('boxshadow');
      $('#doortag, #door_manifest').removeClass('boxshadow');
      _doorScanItems = scanFunctionService.clearScan(_doorScanItems,vm.clearScanItem);
      if(vm.scannedItemsLength > 0){
        vm.scannedItemsLength = vm.scannedItemsLength-1;
        vm.totalBarcodesCountToScan = vm.totalBarcodesCountToScan - 1;
        vm.scannedItems.splice(-1,1);
      }
      vm.scannedBarcode.splice(-1,1);
      if(vm.scannedBarcode.length > 0){
        vm.clearScanItem = vm.scannedBarcode.slice(-1)[0].barcode;
        vm.lastScanItem = vm.scannedBarcode.slice(-1)[0].barcode;

      }else{
        vm.lastScanItem = [];
        vm.clearScanItem = [];
      }
    }

    function onSelectFirstButton() { 
      $('.swipeSelectCont,.selectRightClick,.selectLeftClick').removeClass('second third sixth seventh').addClass('first');
    }
    function onSelectSecondButton() {
      $('.swipeSelectCont').removeClass('first third sixth seventh').addClass('second');
    }
    function onSelectThirdButton() {
      $('.swipeSelectCont').removeClass('first second sixth seventh').addClass('third');
    }
    function onSelectSixthButton() {
      $('.swipeSelectCont').removeClass('first second third seventh').addClass('sixth');
    }
    function onSelectSeventhButton() {
      $('.swipeSelectCont').removeClass('first second third sixth').addClass('seventh');
    }

    function onSelectRightFirstButton() { 
      $('.swipeSelectCont').removeClass('first second third sixth').addClass('seventh');
    }
    function onSelectLeftFirstButton() { 
      $('.swipeSelectCont').removeClass('first third sixth seventh').addClass('second');
    }
    function onSelectRightSecondButton() { 
      $('.swipeSelectCont').removeClass('second third sixth seventh').addClass('first');
    }
    function onSelectLeftSecondButton() { 
      $('.swipeSelectCont').removeClass('first second sixth seventh').addClass('third');
    }
    function onSelectRightThirdButton() { 
      $('.swipeSelectCont').removeClass('first third sixth seventh').addClass('second');
    }
    function onSelectLeftThirdButton() { 
      $('.swipeSelectCont').removeClass('first second third sixth').addClass('seventh');
    }
    function onSelectRightSixthButton() { 
      $('.swipeSelectCont').removeClass('first second sixth seventh').addClass('third');
    }
    function onSelectLeftSixthButton() { 
      $('.swipeSelectCont').removeClass('first second third sixth').addClass('seventh');
    }
    function onSelectRightSeventhButton() { 
      $('.swipeSelectCont').removeClass('first second third seventh').addClass('sixth');
    }
    function onSelectLeftSeventhButton() { 
      $('.swipeSelectCont').removeClass('second third sixth seventh').addClass('first');
    }

    function selectLeftClick(){
      var classNames = $('.swipeSelectCont').attr('class').split(" ");
      var className = classNames[2];
      switch (className) {
        case 'first':               
        $('.swipeSelectCont.'+className).removeClass(className).addClass('second');
        break;
        case 'second':
        $('.swipeSelectCont.'+className).removeClass(className).addClass('third');
        break;
        case 'third':
        $('.swipeSelectCont.'+className).removeClass(className).addClass('sixth');
        break;
        case 'sixth': 
        $('.swipeSelectCont.'+className).removeClass(className).addClass('seventh');
        break;
        case 'seventh': 
        $('.swipeSelectCont.'+className).removeClass(className).addClass('first');
        break;
      }
    }
    function selectRightClick(){
      var classNames = $('.swipeSelectCont').attr('class').split(" ");
      var className = classNames[2];
      switch (className) {
        case 'first':               
        $('.swipeSelectCont.'+className).removeClass(className).addClass('seventh');
        break;
        case 'second':
        $('.swipeSelectCont.'+className).removeClass(className).addClass('first');
        break;
        case 'third':
        $('.swipeSelectCont.'+className).removeClass(className).addClass('second');
        break;
        case 'sixth': 
        $('.swipeSelectCont.'+className).removeClass(className).addClass('third');
        break;
        case 'seventh': 
        $('.swipeSelectCont.'+className).removeClass(className).addClass('sixth');
        break;
      }
    }

    function gotoSelectScan(scantype){
      var currentScanClick = new Date();
      currentScanClick = currentScanClick.getTime();
      if((currentScanClick - lastScanClick) <= 500){
        lastScanClick = currentScanClick;
      }else{
        lastScanClick = currentScanClick;
        if(scantype == 'load')
          loadSelect();
        else if(scantype == 'delivery')
          deliverySelect();
        else if(scantype == 'attempt')
          attemptSelect();
        else if(scantype == 'pickup')
          pickupSelect();
      }    
    }

    function attemptSelect(){
      $scope.prevURL = $scope.previousURL;
      manifestService.getManifestDetailByStopIdentifier(currentStopIdentifier).then(
        function(data){
          barcodesForCurrentStop = [];
          vm.packagesForLocation = data; 
          vm.totalBarcodesCountToScan = vm.packagesForLocation.length;
          totalBarcodesCount = vm.packagesForLocation.length;  
          angular.forEach(data, function (piece, index) {
            barcodesForCurrentStop.push(piece.json.Barcode);
          });   
          $scope.$apply();
        },function(error){
          $log.info('error in getting packages by stopIdentifier',error);
        })
      scanService.showScanner(successScan,errorScan);
      $ionicHistory.nextViewOptions({
        disableBack: true
      });
      $timeout(function(){
        $state.go('app.scan.attempt', { 'stopIdentifier': currentStopIdentifier });
      },250);
    }
    function deliverySelect(){
      $scope.prevURL = $scope.previousURL;
      manifestService.getManifestDetailByStopIdentifier(currentStopIdentifier).then(
        function(data){
          barcodesForCurrentStop = [];
          vm.packagesForLocation = data; 
          vm.totalBarcodesCountToScan = vm.packagesForLocation.length;
          totalBarcodesCount = vm.packagesForLocation.length;  
          angular.forEach(data, function (piece, index) {
            barcodesForCurrentStop.push(piece.json.Barcode);
          });   
          $scope.$apply();
        },function(error){
          $log.info('error in getting packages by stopIdentifier',error);
        })
      scanService.showScanner(successScan,errorScan);
      $ionicHistory.nextViewOptions({
        disableBack: true
      });
      $timeout(function(){
        $state.go('app.scan.delivery', { 'stopIdentifier': currentStopIdentifier });
      },250);
    }
    function pickupSelect(){
      $scope.prevURL = $scope.previousURL;
      manifestService.getPickUpDetailByStopIdentifier(currentStopIdentifier).then(
        function(data){
          barcodesForStop = [];
          vm.packagesForLocation = data; 
          vm.totalBarcodesCountToScan = vm.packagesForLocation.length;
          totalBarcodesCount = vm.packagesForLocation.length;   
          angular.forEach(data, function (piece, index) {
            barcodesForStop.push(piece.json.Barcode);
          });
          $scope.$apply();             
        },function(error){
          $log.info('error in getting packages by stopIdentifier',error);
        })
      scanService.showScanner(successScan,errorScan);
      $ionicHistory.nextViewOptions({
        disableBack: true
      });
      $timeout(function(){
        $state.go('app.scan.pickup', { 'stopIdentifier': currentStopIdentifier });
      },250);
    }
    function loadSelect(){
        var count = 1;
        if(_scannedLoadItems.length >0){
          angular.forEach(_scannedLoadItems, function (piece, index) {
            vm.scannedBarcode.push({
                barcode : piece.barcode,
                scantype : 'load'
            });
            vm.scannedItemsLength = count++;
            vm.scannedItems.push(piece.barcode);
          });
        }
        if(_unassignLoadItems.length >0){
          angular.forEach(_unassignLoadItems, function (piece, index) {
            vm.scannedBarcode.push({
                barcode : piece.barcode,
                scantype : 'load'
            });
            vm.scannedItemsLength = count++;
            vm.scannedItems.push(piece.barcode);
          });
        }
        if(vm.scannedBarcode.length > 0){
          vm.lastScanItem = vm.scannedBarcode.slice(-1)[0].barcode;
          vm.clearScanItem = vm.scannedBarcode.slice(-1)[0].barcode;
        }
        manifestService.getManifestDeliveryBarcode().then(
          function (data) {
            packagesCountInManifest = data.length;
            vm.totalBarcodesCountToScan = packagesCountInManifest; 
            $scope.$apply();       
          },
          function (err) {
            $log.error('Error getting PackagesInCustody count', err);
          }
        );
      $scope.prevURL = $scope.previousURL;
      scanService.showScanner(successScan,errorScan);
      $ionicHistory.nextViewOptions({
        disableBack: true
      });
      $timeout(function(){
        $state.go('app.scan.load',{'stopIdentifier': ''});
      },250);
    }

    function attemptEventTrigger(){
      if((CustomerID.length == 0) || (vm.CustomerConfig.length == 0)) {
      vm.CustomerConfig = [
            {
              Code: "BCLD",
              Label: "Business Closed"
            },
           {
              Code: "CTRF",
              Label: "Recipient refused delivery"
            },
           {
              Code: "RFDM",
              Label: "Damaged"
            },
           {
              Code: "NDMI",
              Label: "Need More Information"
            },
            {
              Code: "NTHM",
              Label: "No one at delivery location"
            },
           {
              Code: "INAD",
              Label: "Incorrect Address"
            },
           {
              Code: "ACSS",
              Label: "Unable to gain access to deliver"
            },
           {
              Code: "MSPK",
              Label: "Carton Placed on Wrong Truck"
            },
           {
              Code: "OTBD",
              Label: "Mechanical breakdown, may impact delivery"
            },
           {
              Code: "ROOT",
              Label: "Unable to Complete before 5pm"
            },
           {
              Code: "WRDL",
              Label: "Delay due to weather or natural disaster"
            },
           {
              Code: "USFR",
              Label: "Unsafe for Release"
            },
           {
              Code: "RRDV",
              Label: "Customer requested re-delivery"
            },
           {
              Code: "UTLV",
              Label: "Unable to Leave"
            },
           {
              Code: "PUIA",
              Label: "Could not locate Pickup. Incorrect Address"
            },
           {
              Code: "PUKT",
              Label: "Pickup refused. Customer kept piece"
            },
           {
              Code: "PURU",
              Label: "Return unavailable for Pickup"
            }
        ]
      }
      var myPopup = $ionicPopup.show({
        'template': '<div class="list"><label class="item item-input item-select brdr_1px"><div class="input-label"></div><select class="maxwidth_100p" ng-model="vm.selectedName" ng-value="data.Label" placeholder = ""><option value="" disabled selected>Please Select an Reason</option><option ng-repeat="data in vm.CustomerConfig">{{data.Label}}</option></select></label></div>',        
        title: 'Attempt Reasons',
        scope: $scope,
        'buttons': [
        {
          'text': 'Ok',
          'type': 'button-positive',
          'onTap': function(event) {
            if(vm.selectedName != undefined){
              angular.forEach(vm.CustomerConfig, function (piece, index) { 
                if(piece.Label == vm.selectedName){
                  $rootScope.Reasonselected = piece.Code;
                }
              })
              $scope.selectedReason();
            }else{
              notificationService.alert('Alert','', "Please Select the Attempt Reason", null);
              event.preventDefault();
            }
          }
        },
        {
          'text': 'Cancel',
          'type': 'button-positive',
          'onTap': function(event) {
            $('#a_attempt').addClass('boxshadow');
          }
        }
        ]
      });
      /*$scope.showselected = function(selectedName) {
        $rootScope.Reasonselected = selectedName;
      } */    
      $scope.selectedReason = function() {
        myPopup.close();
        $ionicPopup.show({
          title: 'Door Tag',
          template: 'Do you want to scan a doortag?',
          scope: $scope,      
          'buttons': [
          {
            'text': 'No',
            'type': 'button-positive',
            'onTap': function() {
              vm.doortag = '';
              var doorTag = vm.doortag;
              /*if(doorTag != ''){*/
                packageStatusService.updateAttemptPackageStatus(_attemptScanItems).then(function(data){
                  if(_deliveryScanItems.length > 0){
                    for(var i in _attemptScanItems){
                      for(var j in _deliveryScanItems){
                        if((_attemptScanItems[i].barcode) == (_deliveryScanItems[j].barcode)){
                          _deliveryScanItems.splice(j,1);
                          saveRootScopes();
                        }
                      }
                    }
                  }
                  $ionicHistory.nextViewOptions({
                    disableBack: true
                  });
                  if(_deliveryScanItems.length > 0){
                    _attemptScanItems = [];
                    saveRootScopes();
                    if($window.localStorage["process"] == 'L2T' || $window.localStorage["process"] == 'T2M'){
                      var count = 0;
                      for (var i=0; i<_deliveryScanItems.length; i+=1) {      //chunk delivery events
                        var _chunkDeliveryEvents = _deliveryScanItems.slice(i,i+1);
                        count+=_chunkDeliveryEvents.length;
                        eventsService.deliveredEventUpdate(_chunkDeliveryEvents, "", "", "","","").then(function(data){
                          if(_deliveryScanItems.length == count ){
                           scanFunctionService.clearRootScope();
                            rootScopeService.clearLocalStorage();
                            rootScopeService.clearDeliveryLocalVariables();
                            $ionicHistory.nextViewOptions({
                              disableBack: true
                            });
                            $state.go('app.manifest');
                          }
                        }, function (err) {
                          $log.warn('Error in updating delivered event',err);
                        })
                      }
                    }else{
                      $state.go('app.signature', {'stopIdentifier': currentStopIdentifier});
                    }
                  }else if(_pickupScanItems.length == 0 && _unassignPickupScanItems.length == 0){
                    $state.go('app.manifest');
                  }
                },function(err){
                  $log.info('Error in updating package status',err);
                });

                if(_locationScanItems.length>0){
                  var count = 0;
                  for (var i=0; i<_attemptScanItems.length; i+=1) {      //chunk attempt events
                    var _chunkAttemptEvents = _attemptScanItems.slice(i,i+1);
                    count+=_chunkAttemptEvents.length;
                    eventsService.attemptBarcodesEventUpdate(_chunkAttemptEvents,doorTag,$rootScope.Reasonselected,_locationScanItems[0].barcode).then(function(data){
                      if(_attemptScanItems.length == count ){
                        if(_pickupScanItems.length > 0){
                          saveRootScopes();
                          $ionicHistory.nextViewOptions({
                            disableBack: true
                          });
                          $state.go('app.pickupDetail');
                        }else if(_unassignPickupScanItems.length > 0 || $rootScope.addUnassignpickupItems.length > 0){
                          saveRootScopes();
                          $ionicHistory.nextViewOptions({
                            disableBack: true
                          });
                          $state.go('app.unassignPickupDetail');
                        }else{
                          if(deviceService.getConnectionStatus()){
                            eventsService.sendEvents();
                          }
                          //scanFunctionService.clearRootScope();
                        }
                      }
                    },function(err){
                      $log.error("Error in updating attempt event",err);
                    })
                  }
                } else{
                  var count = 0;
                  for (var i=0; i<_attemptScanItems.length; i+=1) {      //chunk attempt events
                    var _chunkAttemptEvents = _attemptScanItems.slice(i,i+1);
                    count+=_chunkAttemptEvents.length;
                    eventsService.attemptBarcodesEventUpdate(_chunkAttemptEvents,doorTag,$rootScope.Reasonselected,"").then(function(data){
                      if(_attemptScanItems.length == count ){
                        if(_pickupScanItems.length > 0){
                          saveRootScopes();
                          $ionicHistory.nextViewOptions({
                            disableBack: true
                          });
                          $state.go('app.pickupDetail');
                        }else if(_unassignPickupScanItems.length > 0 || $rootScope.addUnassignpickupItems.length > 0){
                          saveRootScopes();
                          $ionicHistory.nextViewOptions({
                            disableBack: true
                          });
                          $state.go('app.unassignPickupDetail');
                        }else{
                          if(deviceService.getConnectionStatus()){
                            eventsService.sendEvents();
                          }
                          //scanFunctionService.clearRootScope();
                        }
                      }
                    },function(err){
                      $log.warn("Error in updating attempt event",err);
                    })
                  }
                }
            }
          },
          {
            'text': 'Yes',
            'type': 'button-positive',
            'onTap': function() {
              if(vm.scannedItemsLength > 0){
                vm.scannedItemsLength = 0;
                vm.totalBarcodesCountToScan = 0;
                vm.scannedItems = [];
                vm.scannedBarcode= [];
                vm.lastScanItem = [];
              }
              saveRootScopes();
              $state.go('app.scan.door',{'stopIdentifier': ''});
              scanService.showScanner(successScan,errorScan);
            }
          }
          ]
        });
      }
    }

    function attemptReasons(){
        angular.forEach(_attemptScanItems, function (piece, index) {
        var deferred = $q.defer();
          promiseColl.push(             
          manifestService.getManifestDetailByBarcode(piece.barcode).then(
            function (manifestData) {
              angular.forEach(manifestData, function (piece, index) {
                CustomerID.push(piece.json.CustomerID);
              })
            },function(error){
              $log.info("Error in updating manifest detail ",error);
            }
          ))
      })
      $q.all(promiseColl).then(function (data) {
        CustomerID = jQuery.unique(CustomerID);
        customerService.getCustomerAttemptByID(CustomerID).then(function(data){
          angular.forEach(data[0], function (piece, index) { 
            var events = $.grep(vm.CustomerConfig, function (e) {
                return piece.Code === e.Code && piece.Sort === e.Sort;
            });
            if (events.length === 0) {
              vm.CustomerConfig.push(piece);
            }            
          })
        },function(err){
          $log.info("Error in getting Customer info",err);
        })
      }, function (err) {
        $log.info("Error in promise call",err);
        deferred.reject(err);
      });
    }

    var paused;
    $scope.$watch(function(){
     return angular.element(document.querySelector('#delivery_clear, #attempt_clear, #location_clear, #pickup_clear, #door_clear')).attr('class');
    },function(classNames,oldClassNames){
      var loc = $location.path().split('/');
      if(("/"+loc[1]+"/"+loc[2]+"/"+loc[3] == '/app/scan/delivery') || ("/"+loc[1]+"/"+loc[2]+"/"+loc[3] == '/app/scan/attempt') || ("/"+loc[1]+"/"+loc[2]+"/"+loc[3] == '/app/scan/pickup') || ("/"+loc[1]+"/"+loc[2]+"/"+loc[3] == '/app/scan/location') || ("/"+loc[1]+"/"+loc[2]+"/"+loc[3] == '/app/scan/door')) {
        var className = classNames.split(' ');
        if(className[4] == 'boxshadow'){
         paused=scanService.pauseScanner();
          $('.dsbleIp').prop("disabled",true).css('background','#f8f8f8');
          $('.dsbleBtn').prop("disabled",true);
        }else{
          if(paused == true){
            scanService.resumeScanner();
            $('.dsbleIp').prop("disabled",false).css('background','#fff');
            $('.dsbleBtn').prop("disabled",false);
          }
        }
      }
   });

    function detailPage(detailBarcode){
      if(detailBarcode.length>0){
        if(detailBarcode != undefined){
          manifestService.getManifestDetailByBarcode(detailBarcode).then(function(barcodeDetail){
            if(barcodeDetail.length>0){
              if(barcodeDetail[0].json.Destination != undefined){
               $ionicHistory.nextViewOptions({
                 disableBack: true
               });
               $state.go('app.detail', { 'stopIdentifier':  barcodeDetail[0].json.Destination.StopIdentifier, 'type': 'delivery'});
             }
             if(barcodeDetail[0].json.Origin != undefined){
               $ionicHistory.nextViewOptions({
                 disableBack: true
               });
               $state.go('app.detail', { 'stopIdentifier': barcodeDetail[0].json.Origin.StopIdentifier, 'type': 'pickup'});
             }
            
           }else{
             $log.info("manifest detail not available for",detailBarcode);
           }
         },function(error){
           $log.info("Error in getting barcode detail",error);
         })
        }
      }
   }
   function buttonChangeEvent(){
     vm.lastScanItem = [];
     vm.showAddButton = true;
     
   }
    function adddetail(barcode){
      if(barcode.length>0){
        var barcode = barcode.replace(/[^a-z0-9$ \/\-]/gi,'');
        barcode = $filter('uppercase')(barcode);
        if((vm.scannedItems.indexOf(barcode) == -1) || ((barcode) == -1 && $scope.previousURL == 'app.signature') || ((barcode) != -1 && $scope.previousURL == 'app.scan.attempt')){
          vm.showAddButton = false;
          vm.scannedItems.push(barcode);
          vm.scannedItemsLength = vm.scannedItems.length;
          scanType = scanFunctionService.scanType();
          vm.lastScanItem = barcode;
          vm.clearScanItem = barcode;
          vm.scannedBarcode.push({
            barcode : barcode,
            scantype : scanType
          });
          var scannedTime = new Date();
          var loc = $location.path().split('/');
          if(scanType == 'location'){
            vm.totalBarcodesCountToScan += 1;
            _locationScanItems.push({
              barcode:barcode,
              scannedTime:scannedTime
            });
            if(("/"+loc[1]+"/"+loc[2]+"/"+loc[3] == '/app/scan/location')){
              locationDDU();
            } 
          }else if(scanType == 'door'){
            vm.totalBarcodesCountToScan += 1;
            _doorScanItems.push({
              barcode:barcode,
              scannedTime:scannedTime
            });
            doorScanAction();
          }else if(scanType == 'load'){
            if($window.localStorage.loadBarcodes != undefined && (($window.localStorage.loadBarcodes).indexOf(barcode) != -1)){
              loadedBarcode.push({
                barcode:barcode,
                scannedTime:scannedTime
              })
              _loadScanItems.push({
                barcode:barcode,
                scannedTime:scannedTime
              });
              _scannedLoadItems.push({
                barcode:barcode,
                scannedTime:scannedTime
              });
              eventsService.loadEventUpdate(loadedBarcode,"", "Loaded").then(function(data){
                loadedBarcode = [];
              },function(err){
                  $log.warn("Error in load eventupdate",err);
              });
              loadedItems.add(loadedBarcode);
              loadedItems.findAll().then(function (data) {
                  rootScopeService.saveScannedLoadScanItems(data);
              }, function (err) {
                  $log.warn("Failed to save scanned items",err);
              });
            }else if($window.localStorage.pickupBarcodes != undefined && (($window.localStorage.pickupBarcodes).indexOf(barcode) != -1)){
              if(vm.scannedItemsLength > 0){
                vm.scannedItemsLength = vm.scannedItemsLength - 1;
                vm.scannedItems.splice(-1,1);
                vm.scannedBarcode.splice(-1,1);
              }
              if(vm.scannedBarcode.length > 0){
                vm.lastScanItem = vm.scannedBarcode.slice(-1)[0].barcode;
              }else{
                vm.lastScanItem = vm.scannedBarcode.slice(-1);
              }
            }else{
              vm.totalBarcodesCountToScan += 1;
              loadedBarcode.push({
                barcode:barcode,
                scannedTime:scannedTime
              })
              _scannedLoadItems.push({
                barcode:barcode,
                scannedTime:scannedTime
              });
              _unknownLoadItems.push({
                barcode:barcode,
                scannedTime:scannedTime
              })
              _unassignLoadScanItems.push({
                barcode:barcode,
                scannedTime:scannedTime
              })
              unassignLoadedItems.add(loadedBarcode);
              unassignLoadedItems.findAll().then(function (data) {
                  rootScopeService.unassignLoadItems(data);
              }, function (err) {
                  $log.warn("Failed to save scanned items",err);
              });
              rootScopeService.saveUnknownLoadItems(_unknownLoadItems);
              eventsService.loadEventUpdate(loadedBarcode,"", "Loaded").then(function(data){
                loadedBarcode = [];
              },function(err){
                  $log.warn("Error in load eventupdate",err);
              });
            }
          }else if(scanType == 'pickup'){  
            if($window.localStorage.pickupBarcodes != undefined && (($window.localStorage.pickupBarcodes).indexOf(barcode) != -1)){
              if(barcodesForStop.indexOf(barcode) != -1){
                _pickupScanItems.push({
                  barcode:barcode,
                  scannedTime:scannedTime
                });
              }else{
                if(currentStopIdentifier == "" || currentStopIdentifier == undefined){
                  manifestService.getManifestDetailByBarcode(barcode).then(
                    function(data){
                      if(data.length > 0){
                        currentStopIdentifier = data[0].json.Origin.StopIdentifier;
                        pauseIfScnig();
                        notificationService.confirm('','Are you at'+' '+data[0].json.Origin.Address+'?', 'Yes', 'No',function(event){
                          $(event.toElement).attr('disabled', true);
                          $log.info('At the current address');
                          rsmeIfPausd();
                        },function(event){
                          $(event.toElement).attr('disabled', true);
                          $log.info('Not at the current address');
                          rsmeIfPausd();
                        })
                      }
                    },function(error){
                      $log.info("Error in adding manifest detail",error);
                    });
                }
                _pickupScanItems.push({
                  barcode:barcode,
                  scannedTime:scannedTime
                });
                vm.totalBarcodesCountToScan += 1;
              }
            }else{ 
              _unassignPickupScanItems.push({
                barcode:barcode,
                scannedTime:scannedTime
              });
              vm.totalBarcodesCountToScan += 1;
            }
          }else{
            manifestService.getManifestDetailByBarcode(barcode).then(function(data){
              if(data.length>0){
                if(scanType == 'delivery'){
                  packageStatusService.getPackageByBarcode(barcode).then(function(packagedata){
                    if(packagedata.length > 0 && packagedata[0].json.Delivered == true){
                      pauseIfScnig();
                      notificationService.alert('','','This piece is already delivered',function(){
                        rsmeIfPausd();
                        if(vm.scannedItemsLength > 0){
                          vm.scannedItemsLength = vm.scannedItemsLength - 1;
                          vm.scannedItems.splice(-1,1);
                          vm.scannedBarcode.splice(-1,1);
                        }
                        if(vm.scannedBarcode.length > 0){
                          vm.lastScanItem = vm.scannedBarcode.slice(-1)[0].barcode;
                        }else{
                          vm.lastScanItem = vm.scannedBarcode.slice(-1);
                        }
                      });
                    }else{
                      if(data[0].json.Destination != undefined && (currentStopIdentifier == "" || currentStopIdentifier == undefined)){
                        currentStopIdentifier = data[0].json.Destination.StopIdentifier;
                        locationInfo = "addressPrompt";
                        manifestService.getManifestDetailByStopIdentifier(currentStopIdentifier).then(
                          function(data){
                            barcodesForCurrentStop = [];
                            vm.packagesForLocation = data; 
                            vm.totalBarcodesCountToScan = vm.packagesForLocation.length;
                            totalBarcodesCount = vm.packagesForLocation.length;
                            angular.forEach(data, function (piece, index) {
                                barcodesForCurrentStop.push(piece.json.Barcode);
                            }); 
                            $scope.$apply();  
                          },function(error){
                            $log.info('error in getting packages by stopIdentifier',error);
                          })
                      }
                      if(data[0].json.Destination != undefined && data[0].json.Destination.Instruction != ""){
                        pauseIfScnig();
                        notificationService.alert('','scanpopup',data[0].json.Destination.Instruction,function(){
                          if(locationInfo == "addressPrompt"){
                            notificationService.confirm('','Are you at'+' '+data[0].json.Destination.Address+'?', 'Yes', 'No',function(event){
                              $(event.toElement).attr('disabled', true);
                              $log.info('At the current address');
                              locationInfo = "";
                               rsmeIfPausd();
                            },function(event){
                              $(event.toElement).attr('disabled', true);
                              $log.info('Not at the current address');
                              locationInfo = "";
                               rsmeIfPausd();
                            })
                          }
                        });
                      }else if(locationInfo == "addressPrompt"){
                        pauseIfScnig();
                        notificationService.confirm('','Are you at'+' '+data[0].json.Destination.Address+'?', 'Yes', 'No',function(event){
                          $(event.toElement).attr('disabled', true);
                          $log.info('At the current address');
                          locationInfo = "";
                          rsmeIfPausd();
                        },function(event){
                          $(event.toElement).attr('disabled', true);
                          $log.info('Not at the current address');
                          locationInfo = "";
                          rsmeIfPausd();
                        })
                      }
                     if(data[0].json.Destination != undefined && data[0].json.Destination.StopIdentifier == currentStopIdentifier){
                      _deliveryScanItems.push({
                        barcode:barcode,
                        scannedTime:scannedTime
                      });
                       $rootScope.BarcodesBelongsToStop.push(barcode);
                     }else if(data[0].json.Origin != undefined){
                      pauseIfScnig();
                      notificationService.alert('','','You’ve scanned an item that is not for delivery',function(){
                        rsmeIfPausd();
                      });
                      if(vm.scannedItemsLength > 0){
                        vm.scannedItemsLength = vm.scannedItemsLength - 1;
                        vm.scannedItems.splice(-1,1);
                        vm.scannedBarcode.splice(-1,1);
                      }
                     if(vm.scannedBarcode.length > 0){
                        vm.lastScanItem = vm.scannedBarcode.slice(-1)[0].barcode;
                      }else{
                        vm.lastScanItem = vm.scannedBarcode.slice(-1);
                      }
                    }else if(data[0].json.Destination != undefined && data[0].json.Destination.StopIdentifier != currentStopIdentifier && currentStopIdentifier != undefined && currentStopIdentifier != ""){
                    // current stop Identifer != scanned item stop Identifer
                      pauseIfScnig();
                      notificationService.confirm('', 'You’ve scanned a piece that’s not at this stop. Continue?', 'Yes', 'No', 
                        function (event) {
                          $(event.toElement).attr('disabled', true);
                          rsmeIfPausd();
                          _deliveryScanItems.push({
                            barcode:barcode,
                            scannedTime:scannedTime
                          });
                          vm.totalBarcodesCountToScan += 1;
                        }, function (event) {
                          $(event.toElement).attr('disabled', true);
                          rsmeIfPausd();
                          if(vm.scannedItemsLength > 0){
                            vm.scannedItemsLength = vm.scannedItemsLength - 1;
                            vm.scannedItems.splice(-1,1);
                            vm.scannedBarcode.splice(-1,1);
                          }
                          if(vm.scannedBarcode.length > 0){
                            vm.lastScanItem = vm.scannedBarcode.slice(-1)[0].barcode;
                          }else{
                            vm.lastScanItem = vm.scannedBarcode.slice(-1);
                          }
                        });

                      }else{
                        _deliveryScanItems.push({
                          barcode:barcode,
                          scannedTime:scannedTime
                        });
                        vm.totalBarcodesCountToScan += 1;
                      }
                    }
                  },function(error){
                    $log.error("Error in getting package status",error);
                  })
                }  
                if(scanType == 'attempt'){  
                  if(data[0].json.Destination != undefined && (currentStopIdentifier == "" || currentStopIdentifier == undefined)){
                    currentStopIdentifier = data[0].json.Destination.StopIdentifier;
                    locationInfo = "addressPrompt";
                    manifestService.getManifestDetailByStopIdentifier(currentStopIdentifier).then(
                      function(data){
                        barcodesForCurrentStop = [];
                        vm.packagesForLocation = data; 
                        vm.totalBarcodesCountToScan = vm.packagesForLocation.length;
                        totalBarcodesCount = vm.packagesForLocation.length;
                        angular.forEach(data, function (piece, index) {
                            barcodesForCurrentStop.push(piece.json.Barcode);
                        }); 
                        $scope.$apply();  
                      },function(error){
                        $log.info('error in getting packages by stopIdentifier',error);
                      })
                  }
                  if(data[0].json.Destination != undefined && data[0].json.Destination.Instruction != ""){
                    pauseIfScnig();
                    notificationService.alert('','scanpopup',data[0].json.Destination.Instruction,function(){
                      if(locationInfo == "addressPrompt"){
                        notificationService.confirm('','Are you at'+' '+data[0].json.Destination.Address+'?', 'Yes', 'No',function(event){
                          $(event.toElement).attr('disabled', true);
                          $log.info('At the current address');
                          locationInfo = "";
                           rsmeIfPausd();
                        },function(event){
                          $(event.toElement).attr('disabled', true);
                          $log.info('Not at the current address');
                          locationInfo = "";
                           rsmeIfPausd();
                        })
                      }
                    });
                  }else if(locationInfo == "addressPrompt"){
                    pauseIfScnig();
                    notificationService.confirm('','Are you at'+' '+data[0].json.Destination.Address+'?', 'Yes', 'No',function(event){
                      $(event.toElement).attr('disabled', true);
                      $log.info('At the current address');
                      locationInfo = "";
                      rsmeIfPausd();
                    },function(event){
                      $(event.toElement).attr('disabled', true);
                      $log.info('Not at the current address');
                      locationInfo = "";
                      rsmeIfPausd();
                    })
                  }
                 if(data[0].json.Destination != undefined && data[0].json.Destination.StopIdentifier == currentStopIdentifier){
                    _attemptScanItems.push({
                      barcode:barcode,
                      scannedTime:scannedTime
                    });
                     $rootScope.BarcodesBelongsToStop.push(barcode);
                   }else if(data[0].json.Origin != undefined){
                    pauseIfScnig();
                    notificationService.alert('','','You’ve scanned an item that is not for delivery',function(){
                      rsmeIfPausd();
                    });
                    if(vm.scannedItemsLength > 0){
                      vm.scannedItemsLength = vm.scannedItemsLength - 1;
                      vm.scannedItems.splice(-1,1);
                    }
                   if(vm.scannedBarcode.length > 0){
                      vm.lastScanItem = vm.scannedBarcode.slice(-1)[0].barcode;
                    }else{
                      vm.lastScanItem = vm.scannedBarcode.slice(-1);
                    }
                  }else if(data[0].json.Destination != undefined && data[0].json.Destination.StopIdentifier != currentStopIdentifier && currentStopIdentifier != undefined && currentStopIdentifier != ""){
                  // current stop Identifer != scanned item stop Identifer
                    pauseIfScnig();
                    notificationService.confirm('', 'You’ve scanned a piece that’s not at this stop. Continue?', 'Yes', 'No', 
                      function (event) {
                        $(event.toElement).attr('disabled', true);
                        rsmeIfPausd();
                        _attemptScanItems.push({
                          barcode:barcode,
                          scannedTime:scannedTime
                        });
                        vm.totalBarcodesCountToScan += 1;
                      }, function (event) {
                        $(event.toElement).attr('disabled', true);
                        rsmeIfPausd();
                        if(vm.scannedItemsLength > 0){
                          vm.scannedItemsLength = vm.scannedItemsLength - 1;
                          vm.scannedItems.splice(-1,1);
                          vm.scannedBarcode.splice(-1,1);
                        }
                        if(vm.scannedBarcode.length > 0){
                          vm.lastScanItem = vm.scannedBarcode.slice(-1)[0].barcode;
                        }else{
                          vm.lastScanItem = vm.scannedBarcode.slice(-1);
                        }
                      });

                    }else{
                      _attemptScanItems.push({
                        barcode:barcode,
                        scannedTime:scannedTime
                      });
                      vm.totalBarcodesCountToScan += 1;
                      $scope.$apply();
                    }
                }
              }else{
                if(scanType != 'load' && scanType != 'pickup'){
                  pauseIfScnig();
                  notificationService.confirm('', 'Piece not in manifest. Add it?', 'Yes', 'No', 
                    function (event) {
                      $(event.toElement).attr('disabled', true);
                      vm.totalBarcodesCountToScan += 1;
                      rsmeIfPausd();
                      if(scanType == 'delivery')  
                        _deliveryScanItems.push({
                          barcode:barcode,
                          scannedTime:scannedTime
                        });
                      if(scanType == 'attempt')  
                        _attemptScanItems.push({
                          barcode:barcode,
                          scannedTime:scannedTime
                        });
                      
                      if(scanType != 'pickup'){
                        manifestDetailService.getManifestDetail(barcode).then(function(data){
                         if(data.Pieces != undefined && data.Pieces.length > 0 && data.Pieces[0].Destination != undefined){
                             manifestService.addManifestData(data,barcode).then(function(data){
                             },function(err){
                              $log.error("Error in getting manifest detail",err);
                             })
                           }else{
                            manifestService.addManifestDetail(barcode,"Not Available","","").then(function(data){
                            },function(error){
                              $log.error("Error in adding manifest detail",error);
                            });
                           }
                         },function(error){
                            $log.warn("Error in getting data from manifest adapter", error);
                            manifestService.addManifestDetail(barcode,"Not Available","","").then(function(data){
                            },function(error){
                              $log.warn("Error in adding manifest detail",error);
                            });

                         })
                      } 

                    },function (event) {
                      $(event.toElement).attr('disabled', true);
                      rsmeIfPausd();
                      if(vm.scannedItemsLength > 0){
                        vm.scannedItemsLength = vm.scannedItemsLength - 1;
                        vm.scannedItems.splice(-1,1);
                        vm.scannedBarcode.splice(-1,1);
                      }
                      if(vm.scannedBarcode.length > 0){
                        vm.lastScanItem = vm.scannedBarcode.slice(-1)[0].barcode;
                      }else{
                        vm.lastScanItem = vm.scannedBarcode.slice(-1);
                      }
                  }); 
                }
              }        
            },function(error){
                vm.totalBarcodesCountToScan += 1;
                if(scanType != 'pickup' && scanType != 'load' && scanType != 'door'){
                  manifestService.addManifestDetail(barcode,"Not Available","","").then(function(data){
                  },function(error){
                    $log.info("Error in adding manifest detail",error);
                  });
                }
                if(scanType == 'pickup'){
                  $rootScope.unassignedItemAvailable = 'true';
                  saveRootScopes();
                  $ionicHistory.nextViewOptions({
                   disableBack: true
                  });
                  $state.go('app.unassignPickupDetail');
                }
             })  
          }
        }else{
          var loc = $location.path().split('/');      
          if(("/"+loc[1]+"/"+loc[2]+"/"+loc[3] != '/app/scan/load') && ("/"+loc[1]+"/"+loc[2]+"/"+loc[3] != '/app/scan/pickup')){
            pauseIfScnig();
            notificationService.alert('','','This item already scanned',function(){
              rsmeIfPausd();
            });
          }
        }
      }
    }

    $(':input, #drtgSkb').on("keydown",function(event){
       if (event.keyCode == 13) {
           //if clicked  "GO" Btn hide softkeyboard
            cordova.plugins.Keyboard.close();
       }
    });
    $('#hide_skb').keyup(function(e){
         if (this.value != this.value.replace(/[^0-9A-Za-z$ \/\-]/g,'')){
              this.value = this.value.replace(/[^0-9A-Za-z$ \/\-]/g,''); 
         }
    });
      function pauseIfScnig(){
      if(scanService.pauseScanner()==false)
      {
        scanService.pauseScanner();
      }
     }

     function rsmeIfPausd(){
       if(scanService.pauseScanner()==true)
      {
        scanService.resumeScanner();
      }
     }

     function UnassignPickUpBarcodeLoading(){
      angular.forEach(_unassignPickupScanItems, function (piece, index) {
        _pickupScanItems.push(piece);
      })
     }

     $scope.location = $location;
      $scope.$watch( 'location.url()', function( url ) {
        var loc = $location.path().split('/'); 
        if(($location.path() == '/app/scan')){
          $('.dsbleIp').prop("disabled",true).css('background','#f8f8f8');
          $('.dsbleBtn').prop("disabled",true);
          vm.scannedItems = [];
          vm.scannedBarcode = [];
          vm.lastScanItem = [];
          vm.scannedItemsLength = 0;
          vm.totalBarcodesCountToScan = 0;
          currentStopIdentifier = "";
          _loadScanItems = [];
          _unassignLoadScanItems = [];
          _deliveryScanItems = [];
          _attemptScanItems = [];
          _pickupScanItems = [];
          _locationScanItems = [];
          _unassignPickupScanItems = [];
          _doorScanItems = [];
          _doorScanItemBarcode =[];
          scanFunctionService.clearRootScope();
          rootScopeService.clearLocalStorage();
          rootScopeService.clearDeliveryLocalVariables();
        }else if(("/"+loc[1]+"/"+loc[2]+"/"+loc[3] == '/app/scan/selectScan')){
          $('.dsbleIp').prop("disabled",true).css('background','#f8f8f8');
          $('.dsbleBtn').prop("disabled",true);
        }else{
          $('.dsbleIp').prop("disabled",false).css('background','#fff');
          $('.dsbleBtn').prop("disabled",false);
        }   
     });

      function loadScanRedirect(){
        if(vm.scannedItems.length == '0' && _unknownLoadItems.length == 0 ){
          notificationService.alert('','','You have not scanned a barcode.',function(){
            $ionicHistory.nextViewOptions({
              disableBack: true
            });
            if($scope.previousURL == 'app.loadreport'){
              if($scope.prePreviousURL == 'app.detail'){
                $state.go($rootScope.prePreviousURL, { 'stopIdentifier': currentStopIdentifier, 'type': 'delivery'});
              }else{
                $state.go($rootScope.prePreviousURL);
              }
            }else{
              var previousURLParams = angular.fromJson(window.localStorage["previousURL"]);
              if($scope.previousURL == 'app.scan.stop'){
                if($window.localStorage["previousURL"] == "{}"){
                  $state.go($scope.prevURL);
                }else{
                  $state.go($scope.prevURL, { 'stopIdentifier': previousURLParams.stopIdentifier, 'type': previousURLParams.type});
                }
              }else{
                if($window.localStorage["previousURL"] == "{}"){
                  $state.go($scope.previousURL);
                }else{
                  $state.go($scope.previousURL, { 'stopIdentifier': previousURLParams.stopIdentifier, 'type': previousURLParams.type});
                }
              }
            }            
          })     
        }else{
            if(_unknownLoadItems.length > 0){
              var def = $q.defer();
              var promiseColl = [];
              notificationService.showTextSpinner('Processing Load');
              angular.forEach(_unknownLoadItems, function (piece, index) { 
                var barcode = piece.barcode;
                promiseColl.push( 
                  manifestService.addManifestDetail(barcode,"Not Available","","").then(function(data){
                    for(var i in _unknownLoadItems){
                      if(_unknownLoadItems[i].barcode == barcode){
                        _unknownLoadItems.splice(i,1);
                        rootScopeService.saveUnknownLoadItems(_unknownLoadItems);
                      }
                    }
                  },function(error){
                    $log.warn("Error in adding manifest detail",error);
                  })
                )
              });
              $q.all(promiseColl).then(function(barcodeDetail){
                notificationService.hideLoad();
                saveRootScopes();
                if(deviceService.getConnectionStatus()){
                  eventsService.sendEvents();
                }
                $ionicHistory.nextViewOptions({
                  disableBack: true
                });
                vm.totalBarcodesCountToScan = 0;
                $state.go('app.loadreport');
              },function(error){
                $log.info("Error in adding manifest detail",error);
              });
            }else{
              saveRootScopes();
              if(deviceService.getConnectionStatus()){
                eventsService.sendEvents();
              }
              $ionicHistory.nextViewOptions({
                disableBack: true
              });
              vm.totalBarcodesCountToScan = 0;
              $state.go('app.loadreport');
            }
          }
      }

      function locationDDU(){
        pauseIfScnig();
        eventsService.locationScanBarcodesEventUpdate(_locationScanItems,'').then(function(data){
          eventsService.sendEvents();
          notificationService.confirm('', 'Are there any pickup items?', 'Yes', 'No', 
            function () {
              $q.all([ 
                locationStartEvent(),
              ]).then(function(data){
                eventsService.locationScanBarcodesEventUpdate(startLocationEvents,'START').then(function(data){
                  eventsService.sendEvents();
                  $rootScope.flag = 'locationpickup';
                  rsmeIfPausd();
                  $ionicHistory.nextViewOptions({
                    disableBack: true
                  });
                  $state.go('app.scan.pickup', { 'stopIdentifier': '' });
                },function(error){
                  $log.warn("Error in updating location start event",error);
                });
              },function(error){
                $log.warn("Error in updating location start event",error);
              })

            },function(){
              rsmeIfPausd();
              //$rootScope.locationScanItems=[];
              $('#lc_location').toggleClass('boxshadow');
              locationScanAction();
            })

        },function(error){
              $log.info("Error in updating location blank event",error);
        });
      }

      function arrayContainsAnotherArray(actualBarcode, scannedBarcodes){
        for(var i = 0; i < actualBarcode.length; i++){
          if(scannedBarcodes.indexOf(actualBarcode[i]) === -1)
             return false;
        }
        return true;
      }
    function locationStartEvent(){
      var startTime = new Date();
      angular.forEach(_locationScanItems, function (piece, index) {
        startLocationEvents.push({
          barcode:piece.barcode,
          scannedTime:startTime
        })
      });
    }

    function saveRootScopes(){
        rootScopeService.saveLoadItems(_loadScanItems);
        rootScopeService.saveUnasignLoadItems(_unassignLoadScanItems);
        rootScopeService.saveDeliveryScanItems(_deliveryScanItems);
        rootScopeService.saveAttemptScanItems(_attemptScanItems);
        rootScopeService.savePickupScanItems(_pickupScanItems);
        rootScopeService.saveLocationScanItems(_locationScanItems);
        rootScopeService.saveUnassignPickupScanItems(_unassignPickupScanItems);
        rootScopeService.saveDoorScanItems(_doorScanItems);
        rootScopeService.saveDoorScanItemBarcode(_doorScanItemBarcode);
    }

    function deliveryProcess(){
      if(!(arrayContainsAnotherArray($rootScope.BarcodesBelongsToStop,barcodesForCurrentStop) &&($rootScope.BarcodesBelongsToStop.length == barcodesForCurrentStop.length)) && (currentStopIdentifier !== '')){
       notificationService.confirm('Confirmation', 'Not all piece(s) have been scanned for this stop', 'Cancel', 'Continue', 
         function () {
            $('#d_delivery').addClass('boxshadow');
            scanService.showScanner(successScan,errorScan);
         }, function () {
           $ionicHistory.nextViewOptions({
             disableBack: true
           });          
            if(_deliveryScanItems.length > 0){
              saveRootScopes();
              if($window.localStorage["process"] == 'L2T' || $window.localStorage["process"] == 'T2M'){
                var count = 0;
                for (var i=0; i<_deliveryScanItems.length; i+=1) {      //chunk delivery events
                  var _chunkDeliveryEvents = _deliveryScanItems.slice(i,i+1);
                  count+=_chunkDeliveryEvents.length;
                  eventsService.deliveredEventUpdate(_chunkDeliveryEvents, "", "", "","","").then(function(data){
                    if(_deliveryScanItems.length == count ){
                     scanFunctionService.clearRootScope();
                      rootScopeService.clearLocalStorage();
                      rootScopeService.clearDeliveryLocalVariables();
                      $ionicHistory.nextViewOptions({
                        disableBack: true
                      });
                      $state.go('app.manifest');
                    }
                  }, function (err) {
                    $log.warn('Error in updating delivered event',err);
                  })
                }
              }else{
                $state.go('app.signature', {'stopIdentifier': currentStopIdentifier});
              }
              //$state.params.stopIdentifier = "";
            }else if(_attemptScanItems.length > 0){
              attemptEventTrigger();
            }else if(_pickupScanItems.length > 0){
              saveRootScopes();
              $state.go('app.pickupDetail');
            }else if(_unassignPickupScanItems.length > 0){
              $state.go('app.unassignPickupDetail');
              saveRootScopes();
            }else{
              $state.go('app.manifest');
            }
         });
     }else{
        if(_deliveryScanItems.length > 0){
          saveRootScopes();
          if($window.localStorage["process"] == 'L2T' || $window.localStorage["process"] == 'T2M'){
            var count = 0;
            for (var i=0; i<_deliveryScanItems.length; i+=1) {      //chunk delivery events
              var _chunkDeliveryEvents = _deliveryScanItems.slice(i,i+1);
              count+=_chunkDeliveryEvents.length;
              eventsService.deliveredEventUpdate(_chunkDeliveryEvents, "", "", "","","").then(function(data){
                if(_deliveryScanItems.length == count ){
                 scanFunctionService.clearRootScope();
                  rootScopeService.clearLocalStorage();
                  rootScopeService.clearDeliveryLocalVariables();
                  $ionicHistory.nextViewOptions({
                    disableBack: true
                  });
                  $state.go('app.manifest');
                }
              }, function (err) {
                $log.warn('Error in updating delivered event',err);
              })
            }
          }else{
            $state.go('app.signature', {'stopIdentifier': currentStopIdentifier});
          }
          //$state.params.stopIdentifier = "";
        }else if(_attemptScanItems.length > 0){
          attemptEventTrigger();
        }else if(_pickupScanItems.length > 0){
          saveRootScopes();
          $state.go('app.pickupDetail');
        }else if(_unassignPickupScanItems.length > 0){
          saveRootScopes();
          $state.go('app.unassignPickupDetail');
        }else{
          $state.go('app.manifest');
        }
      }
    }

    function attemptProcess(){
      if(!(arrayContainsAnotherArray($rootScope.BarcodesBelongsToStop,barcodesForCurrentStop) &&($rootScope.BarcodesBelongsToStop.length == barcodesForCurrentStop.length)) && (currentStopIdentifier !== '')){
        notificationService.confirm('Confirmation', 'Not all piece(s) have been scanned for this stop', 'Cancel', 'Continue', 
         function () {
            $('#a_attempt').addClass('boxshadow');
            scanService.showScanner(successScan,errorScan);
         }, function () {
            if(_attemptScanItems.length > 0){
              attemptEventTrigger();
            }else if(_pickupScanItems.length > 0){
              saveRootScopes();
              $ionicHistory.nextViewOptions({
                disableBack: true
              });
              $state.go('app.pickupDetail');
            }else if(_unassignPickupScanItems.length > 0 || $rootScope.addUnassignpickupItems.length > 0){
              saveRootScopes();
              $ionicHistory.nextViewOptions({
                disableBack: true
              });
              $state.go('app.unassignPickupDetail');
            }else if(_locationScanItems.length > 0){
              eventsService.locationScanBarcodesEventUpdate(_locationScanItems,'').then(function(data){
                eventsService.sendEvents();
                scanFunctionService.clearRootScope();
                rootScopeService.clearLocalStorage();
              }, function (err) {
                $log.info('Error in add location event',err);
              })
            }
         })
      }else{
        if(_attemptScanItems.length > 0){
          attemptEventTrigger();
        }else if(_pickupScanItems.length > 0){
          saveRootScopes();
          $ionicHistory.nextViewOptions({
            disableBack: true
          });
          $state.go('app.pickupDetail');
        }else if(_unassignPickupScanItems.length > 0 || $rootScope.addUnassignpickupItems.length > 0){
          saveRootScopes();
          $ionicHistory.nextViewOptions({
            disableBack: true
          });
          $state.go('app.unassignPickupDetail');
        }else if(_locationScanItems.length > 0){
          eventsService.locationScanBarcodesEventUpdate(_locationScanItems,'').then(function(data){
            eventsService.sendEvents();
            scanFunctionService.clearRootScope();
            rootScopeService.clearLocalStorage();
          }, function (err) {
            $log.info('Error in add location event',err);
          })
        }
      }
    }

    function locationProcess(){
      if(_loadScanItems.length > 0){
        packageStatusService.loadPackageInfo(_loadScanItems);
        if(_locationScanItems.length > 0){
          eventsService.loadEventUpdate(_loadScanItems,_locationScanItems[0].barcode).then(function(data){
            if(_deliveryScanItems.length > 0){
              saveRootScopes();
              $ionicHistory.nextViewOptions({
                disableBack: true
              });
             if($window.localStorage["process"] == 'L2T' || $window.localStorage["process"] == 'T2M'){
                var count = 0;
                for (var i=0; i<_deliveryScanItems.length; i+=1) {      //chunk delivery events
                  var _chunkDeliveryEvents = _deliveryScanItems.slice(i,i+1);
                  count+=_chunkDeliveryEvents.length;
                  eventsService.deliveredEventUpdate(_chunkDeliveryEvents, "", "", "","","").then(function(data){
                    if(_deliveryScanItems.length == count ){
                     scanFunctionService.clearRootScope();
                      rootScopeService.clearLocalStorage();
                      rootScopeService.clearDeliveryLocalVariables();
                      $ionicHistory.nextViewOptions({
                        disableBack: true
                      });
                      $state.go('app.manifest');
                    }
                  }, function (err) {
                    $log.warn('Error in updating delivered event',err);
                  })
                }
              }else{
                $state.go('app.signature', {'stopIdentifier': currentStopIdentifier});
              }
            }else if(_attemptScanItems.length > 0){
              attemptEventTrigger();
            }else if(_pickupScanItems.length > 0){
              saveRootScopes();
              $ionicHistory.nextViewOptions({
                disableBack: true
              });
              $state.go('app.pickupDetail');
            }else if(_unassignPickupScanItems.length > 0 || $rootScope.addUnassignpickupItems.length > 0){
              saveRootScopes();
              $ionicHistory.nextViewOptions({
                disableBack: true
              });
              $state.go('app.unassignPickupDetail');
            }else{
              eventsService.locationScanBarcodesEventUpdate(_locationScanItems,'').then(function(data){
                var previousURLParams = angular.fromJson(window.localStorage["previousURL"]);
                if($window.localStorage["previousURL"] == "{}"){
                  $state.go($scope.previousURL);
                }else{
                  $state.go($scope.previousURL, { 'stopIdentifier': previousURLParams.stopIdentifier});
                }
                eventsService.sendEvents();
                scanFunctionService.clearRootScope();
                rootScopeService.clearLocalStorage();
              },function(err){
                $log.warn("Error in adding location event",err);
              })
            }
          },function(err){
            $log.warn("Error in adding location event",err);
          })
        }else{
          eventsService.loadEventUpdate(_loadScanItems,"").then(function(data){
            if(_deliveryScanItems.length > 0){
              saveRootScopes();
              $ionicHistory.nextViewOptions({
                disableBack: true
              });
             if($window.localStorage["process"] == 'L2T' || $window.localStorage["process"] == 'T2M'){
                var count = 0;
                for (var i=0; i<_deliveryScanItems.length; i+=1) {      //chunk delivery events
                  var _chunkDeliveryEvents = _deliveryScanItems.slice(i,i+1);
                  count+=_chunkDeliveryEvents.length;
                  eventsService.deliveredEventUpdate(_chunkDeliveryEvents, "", "", "","","").then(function(data){
                    if(_deliveryScanItems.length == count ){
                     scanFunctionService.clearRootScope();
                      rootScopeService.clearLocalStorage();
                      rootScopeService.clearDeliveryLocalVariables();
                      $ionicHistory.nextViewOptions({
                        disableBack: true
                      });
                      $state.go('app.manifest');
                    }
                  }, function (err) {
                    $log.warn('Error in updating delivered event',err);
                  })
                }
              }else{
                $state.go('app.signature', {'stopIdentifier': currentStopIdentifier});
              }
            }else if(_attemptScanItems.length > 0){
              attemptEventTrigger();
            }else if(_pickupScanItems.length > 0){
              saveRootScopes();
              $ionicHistory.nextViewOptions({
                disableBack: true
              });
              $state.go('app.pickupDetail');
            }else if(_unassignPickupScanItems.length > 0 || $rootScope.addUnassignpickupItems.length > 0){
              saveRootScopes();
              $ionicHistory.nextViewOptions({
                disableBack: true
              });
              $state.go('app.unassignPickupDetail');
            }else{
               var previousURLParams = angular.fromJson(window.localStorage["previousURL"]);
                if($window.localStorage["previousURL"] == "{}"){
                  $state.go($scope.previousURL);
                }else{
                  $state.go($scope.previousURL, { 'stopIdentifier': previousURLParams.stopIdentifier});
                }
              eventsService.sendEvents();
              scanFunctionService.clearRootScope();
              rootScopeService.clearLocalStorage();
            }
          },function(err){
            $log.warn("Error in adding location event",err);
          })
        }
        
      }else if(_deliveryScanItems.length > 0){
        saveRootScopes();
        $ionicHistory.nextViewOptions({
            disableBack: true
          });
         if($window.localStorage["process"] == 'L2T' || $window.localStorage["process"] == 'T2M'){
            var count = 0;
            for (var i=0; i<_deliveryScanItems.length; i+=1) {      //chunk delivery events
              var _chunkDeliveryEvents = _deliveryScanItems.slice(i,i+1);
              count+=_chunkDeliveryEvents.length;
              eventsService.deliveredEventUpdate(_chunkDeliveryEvents, "", "", "","","").then(function(data){
                if(_deliveryScanItems.length == count ){
                 scanFunctionService.clearRootScope();
                  rootScopeService.clearLocalStorage();
                  rootScopeService.clearDeliveryLocalVariables();
                  $ionicHistory.nextViewOptions({
                    disableBack: true
                  });
                  $state.go('app.manifest');
                }
              }, function (err) {
                $log.warn('Error in updating delivered event',err);
              })
            }
          }else{
            $state.go('app.signature', {'stopIdentifier': currentStopIdentifier});
          }
        }else if(_attemptScanItems.length > 0){
          attemptEventTrigger();
        }else if(_pickupScanItems.length > 0){
          saveRootScopes();
          $ionicHistory.nextViewOptions({
            disableBack: true
          });
          $state.go('app.pickupDetail');
        }else if(_unassignPickupScanItems.length > 0 || $rootScope.addUnassignpickupItems.length > 0){
          saveRootScopes();
          $ionicHistory.nextViewOptions({
            disableBack: true
          });
          $state.go('app.unassignPickupDetail');
        }else if(_locationScanItems.length > 0){
            $ionicHistory.nextViewOptions({
              disableBack: true
            });
            $state.go('app.manifest');
        }else{
          notificationService.alert('','','You have not scanned a barcode.',function(){
            $ionicHistory.nextViewOptions({
              disableBack: true
            });
            var previousURLParams = angular.fromJson(window.localStorage["previousURL"]);
            if($window.localStorage["previousURL"] == "{}"){
              $state.go($scope.previousURL);
            }else{
              $state.go($scope.previousURL, { 'stopIdentifier': previousURLParams.stopIdentifier});
            }
          })
        }     
    }
  }
})();
