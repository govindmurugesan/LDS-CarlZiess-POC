angular
  .module('beam')
  .service('notificationService', notificationService);

notificationService.$inject = ['$ionicPopup', '$state','$ionicLoading']

function notificationService($ionicPopup, $state,$ionicLoading) {
  var vm = this;
  vm.confirm = confirm;
  vm.showLoad = showLoad;
  vm.hideLoad = hideLoad;
  vm.alert = alert;
  vm.showSpinner = showSpinner;
  vm.showTextSpinner = showTextSpinner;

  function confirm(title, alertMsg, cancelText, confirmText, cancelAction, confirmAction) {
    var confirmPopup = $ionicPopup.confirm({
      title: title,
      template: alertMsg,
      buttons: [{
        text: cancelText,
        type: 'button-positive',
        onTap: cancelAction

      }, {
          text: confirmText,
          type: 'button-positive',
          onTap: confirmAction
        }]
    });
  } 
  function showLoad(msg){
      $ionicLoading.show({
      template: msg? msg : 'Loading...'
    });
  }
  function showSpinner(){
      $ionicLoading.show({
      template: '<ion-spinner icon="spiral"></ion-spinner>'
    });
  }
  function hideLoad(){
    $ionicLoading.hide();
  }
  function alert(title,className,alertMsg,okAction){
    $ionicPopup.alert({
     title: title,
     cssClass: className,
     template: alertMsg
   }).then(okAction);
  }
  function showTextSpinner(text){
    $ionicLoading.show({
      template: '<ion-spinner icon="bubbles"></ion-spinner><p>'+ text +'</p>'
    });
  }

}