(function () {
    'use strict';

    angular
        .module('beam')
        .factory('AuthAdapterService', AuthAdapterService);

    AuthAdapterService.$inject = ['$window', '$http', 'deviceService', '$q', 'driverService', 'jsonStoreService','AppVersion', '$log', 'APIUrl'];
    function AuthAdapterService($window, $http, deviceService, $q, driverService, jsonStoreService,AppVersion, $log, APIUrl) {
        var service = {
            login: login,
            loginStatus: status,
            logOut: logOut,
        };
        return service;

        function login(driverId, password) {
            if (status()) {
                driverService.clearDriverInfo();
            }
            var def = $q.defer();
            var _date = new Date();
            var guidRequest = {
                'eventDate': _date,
                'eventType': 'InfoRequest',
                'eventModifier': 'LOGN',
                'appversion': AppVersion,
                'driverId': driverId,//'06243',
                'FacilityCode':driverId
            };
            var md = deviceService.getSecurityGuid(guidRequest);
            $q.all([deviceService.getLocation(),
                deviceService.getIpAddress(),
                deviceService.getPhoneNum(),
                deviceService.getUUID()])
                .then(function (data) {
                    var _pos = data[0];
                    var _ip = data[1];
                    var _phoneNum = data[2];
                    var uuid = data[3];
                    var req = {
                        "DriverInfo": {
                            "DriverID": guidRequest.driverId,
                            "FacilityID": guidRequest.FacilityCode,
                            "Password": password//"19857977"
                        },
                        "DeviceInfo": {
                            "DeviceID": uuid,
                            "PhoneNum": _phoneNum,
                            "PhoneIp": _ip,
                            "AppVersion": guidRequest.appversion
                        },
                        "LocationInfo": {
                            "Longitude": _pos.long,
                            "Latitude": _pos.lat,
                            "GPSPrecision": "1",
                            "GPSFixDateTime": _pos.timeStamp.toMyISOString()
                        },
                        "SecurityGuid": md,
                        "EventISODateTime": guidRequest.eventDate.toMyISOString()
                    };
                    console.log('requestttt', req);
                   var request = {
                     method: 'POST',
                     url: APIUrl + 'login',
                     headers: {
                       'Content-Type': 'application/x-www-form-urlencoded'
                     },
                     data: req
                    }

                    $http(request).then(function(response){
                        console.log('response',response);
                        if (response.status == 200 && response.data.responseJSON.isSuccessful && response.data.responseJSON.statusCode != 404) {
                            if (response.data.responseJSON.ErrorMessage && response.data.responseJSON.Error)
                                def.reject(response.responseJSON.ErrorMessage);
                            else {
                                response.data.responseJSON.currentDriverId = driverId;
                                driverService.saveDriverInfo(response.data.responseJSON);
                                def.resolve(response.data.responseJSON);
                            }
                        }
                        else {
                            def.reject('Login Failed. Try Again');
                        }
                    }, function(error){
                        def.reject(error);
                    });
                   
                }, function (err) {
                    console.log('Error acquiring device infos', err);
                    def.reject(err);
                })
            return def.promise
        }

        function logOut() {
            var def = $q.defer();
            var _date = new Date();
            var driverInfo = driverService.getDriverInfo();
            var guidRequest = {
                'eventDate': _date,
                'eventType': 'InfoRequest',
                'eventModifier': 'LOGT',
                'appversion': AppVersion,
                'driverId': driverInfo.driverId,
                'facilityId': driverInfo.FacilityCode,
            }
            var md = deviceService.getSecurityGuid(guidRequest);
            $q.all([deviceService.getLocation(),
                deviceService.getIpAddress(),
                deviceService.getPhoneNum(),
                deviceService.getUUID()])
                .then(function (data) {
                    var _pos = data[0];
                    var _ip = data[1];
                    var _phoneNum = data[2];
                    var uuid = data[3];
                   
                    var req = {
                        "DriverInfo": {
                            "DriverID": driverInfo.driverId,
                            "FacilityID": driverInfo.FacilityCode,
                            "Password": driverInfo.password
                        },
                        "DeviceInfo": {
                            "DeviceID": uuid,
                            "PhoneNum": _phoneNum,
                            "PhoneIp": _ip,
                            "AppVersion": guidRequest.appversion
                        },
                        "LocationInfo": {
                            "Longitude": _pos.long,
                            "Latitude": _pos.lat,
                            "GPSPrecision": "1",
                            "GPSFixDateTime": _pos.timeStamp.toMyISOString()
                        },
                        "SecurityGuid": md,
                        "EventISODateTime": guidRequest.eventDate.toMyISOString()
                    };
                    var request = {
                     method: 'POST',
                     url: APIUrl + 'logout',
                     headers: {
                       'Content-Type': 'application/x-www-form-urlencoded'
                     },
                     data: req
                    }

                    $http(request).then(function(response){
                         if (response.status == 200 && response.data.responseJSON.isSuccessful && response.data.responseJSON.errors.length == 0) // Logout Successful else throw error
                            {
                                def.resolve(response.data.responseJSON);
                                jsonStoreService.closeStore();
                            }
                            else
                                def.reject(response.data.responseJSON);
                    }, function(error){
                        def.reject(error);
                    });
                    
                }, function (err) {
                    $log.error('Error acquiring device infos', err);
                    def.reject(err);
                });
            return def.promise;
        }

        function status() {
            return $window.localStorage['beamCredentials'] !== undefined;
        }
    }

})();
