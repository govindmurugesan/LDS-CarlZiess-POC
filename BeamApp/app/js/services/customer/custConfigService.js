(function () {
    'use strict';

    angular
        .module('beam')
        .factory('custConfigService', custConfigService);

    custConfigService.$inject = ['$window', 'deviceService', '$q', 'driverService','$log','AppVersion','APIUrl','$http'];
    function custConfigService($window, deviceService, $q ,driverService, $log,AppVersion, APIUrl,$http) {
        var service = {
            getCustConfig: getCustConfig
        };

        return service;

        function getCustConfig(custId) {
            var driverInfo = driverService.getDriverInfo();
            var def = $q.defer();
            var _date = new Date();
            var guidRequest = {
                'eventDate': _date,
                'eventType': 'InfoRequest',
                'eventModifier': 'CRCF',
                'appversion': AppVersion,
                'driverId': driverInfo.driverId,
                'facilityId': driverInfo.FacilityCode,
            }
            var md = deviceService.getSecurityGuid(guidRequest);
            $q.all([deviceService.getLocation(),
                deviceService.getIpAddress(),
                deviceService.getPhoneNum(),
                deviceService.getUUID()])
                .then(function (data) {
                    var _pos = data[0];
                    var _ip = data[1];
                    var _phoneNum = data[2];
                    var uuid = data[3];
                    var req = {
                        "DriverInfo": {
                            "DriverID": driverInfo.driverId,
                            "FacilityID": driverInfo.FacilityCode,
                        },
                        "DeviceInfo": {
                            "DeviceID": uuid,
                            "PhoneNum": _phoneNum,
                            "PhoneIp": _ip,
                            "AppVersion": guidRequest.appversion
                        },
                        "LocationInfo": {
                            "Longitude": _pos.long,
                            "Latitude": _pos.lat,
                            "GPSPrecision": "1",
                            "GPSFixDateTime": _pos.timeStamp.toMyISOString()
                        },
                        "SecurityGuid": md,
                        "EventISODateTime": guidRequest.eventDate.toMyISOString(),
                        "CustomerId": custId
                    };
                   
                    var request = {
                     method: 'POST',
                     url: APIUrl + 'customerconfig',
                     headers: {
                       'Content-Type': 'application/x-www-form-urlencoded'
                     },
                     data: req
                    }
                    $http(request).then(function(response){                  
                        if (response.status == 200 && response.data.responseJSON.isSuccessful)
                        {
                            def.resolve(response.data.responseJSON.adapterResponse);
                        }else 
                            def.reject(response.data.responseJSON);
                    }, function(error){
                        $log.error('Error acquiring device infos', error);
                        def.reject(error);
                    });
                    
                }, function (err) {
                    $log.error('Error acquiring device infos', err);
                    def.reject(err);
                });
            return def.promise;
        }
    }
})();