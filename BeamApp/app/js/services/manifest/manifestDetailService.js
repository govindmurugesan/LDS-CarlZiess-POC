(function () {
    'use strict';

    angular
        .module('beam')
        .factory('manifestDetailService', manifestDetailService);

    manifestDetailService.$inject = ['$window', 'deviceService', '$q', 'driverService', '$log', 'AppVersion', '$cordovaNetwork', 'APIUrl', '$http'];
    function manifestDetailService($window, deviceService, $q, driverService, $log, AppVersion, $cordovaNetwork, APIUrl, $http) {
        var service = {
            getManifestDetail: getManifestDetail
        };

        return service;

        function getManifestDetail(barcode) {
            var def = $q.defer();
            if(deviceService.getConnectionStatus()){
            var driverInfo = driverService.getDriverInfo();
            var _date = new Date();
            var guidRequest = {
                'eventDate': _date,
                'eventType': 'InfoRequest',
                'eventModifier': 'PCDL',
                'appversion': AppVersion,
                'driverId': driverInfo.driverId,
                'facilityId': driverInfo.FacilityCode,
            }
            var md = deviceService.getSecurityGuid(guidRequest);
            $q.all([deviceService.getLocation(),
                deviceService.getIpAddress(),
                deviceService.getPhoneNum(),
                deviceService.getUUID()])
                .then(function (data) {
                    var _pos = data[0];
                    var _ip = data[1];
                    var _phoneNum = data[2];
                    var uuid = data[3];

                    var req = {
                        "DriverInfo": {
                             "DriverID": driverInfo.driverId,
                            "FacilityID": driverInfo.FacilityCode,
                        },
                        "DeviceInfo": {
                            "DeviceID": uuid,
                            "PhoneNum": _phoneNum,
                            "PhoneIp": _ip,
                            "AppVersion": guidRequest.appversion
                        },
                        "LocationInfo": {
                            "Longitude": _pos.long,
                            "Latitude": _pos.lat,
                            "GPSPrecision": "1",
                            "GPSFixDateTime": _pos.timeStamp.toMyISOString()
                        },
                        "SecurityGuid": md,
                        "EventISODateTime": guidRequest.eventDate.toMyISOString(),
                        "BarCode": encodeURIComponent(barcode.trim())
                    };

                     var request = {
                     method: 'POST',
                     url: APIUrl + 'manifestdetails',
                     headers: {
                       'Content-Type': 'application/x-www-form-urlencoded'
                     },
                     data: req
                    }

                    $http(request).then(function(response){                        
                        if (response.status == 200 && response.data.responseJSON.isSuccessful) 
                        {
                            if (response.data.responseJSON.Pieces && response.data.responseJSON.Pieces.length > 0)
                                def.resolve(response.data.responseJSON);
                            else
                                def.reject('No manifestdetails records found');
                        }
                        else
                            def.reject(response.data.responseJSON);
                        
                    }, function(error){                        
                        def.reject(error);
                    });
                   
                }, function (err) {
                    $log.error('Error acquiring device infos', err);
                    def.reject(err);
                });
            }else{
                def.resolve("");
            }
            return def.promise;
        }
    }
})();